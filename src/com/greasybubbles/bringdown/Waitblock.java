package com.greasybubbles.bringdown;

import java.util.HashMap;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.scene.Scene;
import org.andengine.util.modifier.IModifier;

import android.util.Log;

/* This class deserves an explanation. Basically I created it to 'solve' the problem of implementing
 * a linear sequence of events including 'waits' or delays between statements, in an environment where the
 * execution is not linear but is based on a loop that gets called over and over, such as is the case in a
 * video game (usually). It is all achievable by using a more normal state machine and creating one state
 * for each group of statements in between waits, but such an approach can get seriously messy and
 * 'spaghetti'-like to follow if there are several stages and several delays to observe or 'waitpoints'.
 * My solution is based on the concept of Entity modifiers in AndEngine and their ability to call a function
 * within a 'listener' (defined as an interface) when the modifier ends. This is a useful way of generating a
 * delay but normally it does not block execution while the modifier is running. By enclosing the section of
 * code that needs this pseudo-linear behaviour inside a switch statement that gets called over and over
 * via the main loop, and using this class to hide the dirty work, some rather readable code results.
 * There is an example appended to this class file that should make it clearer.
 */

public class Waitblock implements IEntityModifierListener {

	//Constants
	public static final int START = 0;
	private static final int NONE = -65536;

	
	// STATIC COMPONENTS
	
	private static HashMap<String,Waitblock> instances = new HashMap<String,Waitblock>();
	
	//if the specified Id already exists, then returns the pre-existing instance,
	//otherwise create a new one and keep track of it.
	public static Waitblock getInstance(String instanceId) {
		if (instances.containsKey(instanceId)) {
			return instances.get(instanceId);
		} else {
			Waitblock newInstance = new Waitblock(instanceId);
			instances.put(instanceId, newInstance);
			return newInstance;
		}
	}
	
	// NONSTATIC COMPONENTS
	
	private String id;	
	private boolean waiting;
	private int currentWaitpoint;	
	private Entity dummyEntity; //its sole purpose is to have delay modifiers attached to it!
	
	// Constructor. (private! the user accesses objects via getInstance().
	private Waitblock(String instanceId) {
		id = instanceId;
		dummyEntity = new Entity();
		currentWaitpoint = START;
		waiting = false;
	}

	//
	// Methods
	//
	public int state() {
		if (this.ready())
			return currentWaitpoint;
		else
			return (-currentWaitpoint);
	}
	
	//this command is called by the user upon setting up their delay and reaching the end of
	//one of the blocks in the switch statement. See conceptual explanation at the end...
	public void YieldAt(int waitpointId) {
		currentWaitpoint = waitpointId;
		waiting = true;
	}

	public boolean ready() {
		return (!waiting);
	}

	//go to the specified point without waiting for no-one!
	public void goTo(int waitpointId) {
		currentWaitpoint = waitpointId;
		waiting = false;
	}
	
	public void wait(float time, Scene scene) { // TODO the need to have a SCENE passed is because
			//if I attach the entity modifier to the dummyentity it doesn't get called! i need
			// something more 'real' to attach it to... but WHY?
		//this is an utility function really, it doesn't do anything that one couldn't do in
		//the main program by attaching the delaymodifier to some object there. But this allows for
		//more readability.
		waiting = true;
		scene.registerEntityModifier(new DelayModifier(time, this));
	}
	
	public void end() {
		waiting = false;
		currentWaitpoint = NONE;
		if (instances.containsKey(this.id))
			instances.remove(this.id);
	}
	
	@Override
	public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
		//nothing to do
	}

	@Override
	public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
		//It works like this: the user in the main programs sets up an Entity modifier
		//to implement a delay. They then give a Waitblock object as the entity modifier listener.
		//When the modifier finishes, it calls this function and the Waitblock object knows the wait is over.
		waiting = false;
	}
}


/* THIS IS HOW IT WORKS CONCEPTUALLY in the user's program:

waitblock = Waitblock.Instance("Example");

switch (waitblock.stage) {
	[some program statements...];
	//wait X
	waitModifier(X, waitblock);
	waitblock.yieldAt(A);	
case A:
	if (waitblock.ready()) break;
	[some program statements...];
	//wait Y
	waitModifier(Y, waitblock);
	waitblock.yieldAt(B);
case B:
	if (waitblock.ready()) break;
	[some program statements...];
	timeblock.end();
}

*/