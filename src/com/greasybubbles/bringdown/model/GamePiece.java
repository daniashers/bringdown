package com.greasybubbles.bringdown.model;

public class GamePiece {

	// enums used to define the size shape and material of a game piece

	public enum Shape {
		HorzDouble,
		VertDouble,
		Square,
		HorzHalf,
		VertHalf,
		DiagLeftBottom,
		DiagLeftTop,
		DiagRightBottom,
		DiagRightTop
	}
	public enum Material { //the convention MSD (movable, splittable, destroyable is used)
		// most common
		Fixed,				//MSD 000: totally immovable and immutable, doesn't even fall down if unsupported 
		Indestructible,		//MSD 100: can be moved but not altered or destroyed
		Versatile, 			//MSD 111: can do everything with it
		Glass,				//MSD 101+Shatter: Like concrete, but will shatter if explosion next to it
		//less common
		Indivisible,		//MSD 101: can be moved and destroyed but not split. 
		SplittableOnly,		//MSD 110: can be split and moved but not destroyed altogether
		Special,			//MSD 100: a special user block. Similar properties to movable but immutable
		// least common
		Sculptable,			//MSD 001: Immovable but can be blasted
		SculptablePlus, 	//MSD 011: Immovable but can be blasted and split
		SuperHeavy, 		//this piece can fall, but cannot be blasted laterally.
	};	
	
	static private final Material[] MOVABLE_MATERIALS = { Material.Indestructible, Material.Indivisible, Material.SplittableOnly,
			Material.Glass, Material.Versatile, Material.Special };
	static private final Material[] SPLITTABLE_MATERIALS = { Material.SplittableOnly, Material.SculptablePlus,
			Material.Versatile };
	static private final Material[] DESTROYABLE_MATERIALS = { Material.Glass, Material.Sculptable,
			Material.SculptablePlus, Material.Versatile, Material.Indivisible };
	static private final Material[] SHATTERABLE_MATERIALS = { Material.Glass };
	static private final Shape[] SPLITTABLE_SHAPES = { Shape.Square,
			Shape.HorzDouble, Shape.VertDouble };

	public static enum PhysicsStatus {
		Undefined,
		Stable,
		Forced,
		Moving
	}
	
	public Shape shape;
	public Material material;
	public int posX;
	public int posY;
	
	PhysicsStatus fallingStatus;
	PhysicsStatus leftSlopeStatus;
	PhysicsStatus rightSlopeStatus;
	PhysicsStatus LeftPushStatus;
	PhysicsStatus RightPushStatus;
	
	public float offsetX; //expressed in fractions of a cell
	public float offsetY; //expressed in fractions of a cell
	//(the above Offset values are useful for smooth animations)

	//a class that defines an immovable block useful for representing the ground to the engine
	public static final GamePiece GROUND = new GroundPiece();
	
	
	// Constructors
	//
	
	public GamePiece(Shape initShape, Material initMaterial, int x, int y) {
		this.shape = initShape;
		this.material = initMaterial;
		this.posX = x;
		this.posY = y;
		this.offsetX = 0.0f;
		this.offsetY = 0.0f;
		//TODO error checking on x and y
	}

	public GamePiece(GamePiece p) { //copy constructor
		this(p.shape, p.material, p.posX, p.posY);
	}
	
	// Methods
	//
	
	public boolean isMovable() {
		for (Material m : MOVABLE_MATERIALS) { if (m == this.material) return true; }
		return false;
	}
	public boolean isSplittable() {
		for (Material m : SPLITTABLE_MATERIALS) { if (m == this.material) return true; }
		return false;
	}
	public boolean isDestroyable() {
		for (Material m : DESTROYABLE_MATERIALS) { if (m == this.material) return true; }
		return false;
	}
	public boolean isShatterable() {
		for (Material m : SHATTERABLE_MATERIALS) { if (m == this.material) return true; }
		return false;
	}
	public boolean isFixed() {
		if (this.material == Material.Fixed) return true; else return false;
	}

	public boolean isSplittableShape() {
		for (Shape s : SPLITTABLE_SHAPES) { if (s == this.shape) return true; }
		return false;
	}
	
	// properties method
	public boolean isDiagonal() {
		switch(this.shape) {
		case DiagLeftBottom:
		case DiagLeftTop:
		case DiagRightBottom:
		case DiagRightTop:
			return true;
		default:
			return false;
		}
	}
	
	public boolean isValidSplit(int x1, int y1, int x2, int y2)
	//returns whether the line traced is a valid split point for the piece in question
	//e.g. a straight vertical line in the middle for a horizontal double...
	//in other words can the piece be split in that location?
	{
		switch (this.shape) {
		case HorzHalf:
		case VertHalf:
		case DiagLeftBottom:
		case DiagLeftTop:
		case DiagRightBottom:
		case DiagRightTop:
			//none of these pieces can be split at all, so return false
			return false;
		case HorzDouble:
			if (x1 == x2 && x1 == this.posX+2) {
				if (Math.min(y1, y2) == this.posY && Math.max(y1, y2) == (this.posY+2)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		case VertDouble:
			if (y1 == y2 && y1 == this.posY+2) {
				if (Math.min(x1, x2) == this.posX && Math.max(x1, x2) == (this.posX+2)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}	
		case Square:
			//swaps the points if necessary so that point 1 is the lower one.
			int tx1, ty1, tx2, ty2;
			if (y1 <= y2) { tx1 = x1; ty1 = y1; tx2 = x2; ty2 = y2; }
			else { tx1 = x2; ty1 = y2; tx2 = x1; ty2 = y1; }
				//if (ty2 - ty1) > 2 return false; //if the 2 coordinates are further apart than 2
			//if (Math.abs(tx2- tx1) > 2) return false; //then it can't be.
				//test for horizontal split
			if (ty1 == ty2 && ty1 == this.posY+1) {
				if (Math.min(tx1, tx2) == this.posX && Math.max(tx1,tx2) == (this.posX+2)) {
					return true;
				} else {
					return false;
				}
			}
			//test for diag "slash" split
			if (ty1 == this.posY && tx1 == this.posX) {
				if (ty2 == this.posY + 2 && tx2 == this.posX + 2) {
					return true;
				} else {
					return false;
				}
			}
			//test for diag "backslash" split
			if (ty1 == this.posY && tx1 == this.posX + 2) {
				if (ty2 == this.posY + 2 && tx2 == this.posX) {
					return true;
				} else {
					return false;
				}
			}
			//test for vertical split
			if (tx1 == tx2 && tx1 == this.posX + 1) {
				if (ty1 == this.posY && ty2 == this.posY + 2) {
					return true;
				} else {
					return false;
				}
			}
			break;
		default:
			return false;
		}
		return false;
	}
	
	public int getWeight() {
		switch (this.shape) {
		case HorzDouble:
		case VertDouble:
			return 4;
		case Square:
			return 2;
		case HorzHalf:
		case VertHalf:
		case DiagLeftBottom:
		case DiagLeftTop:
		case DiagRightBottom:
		case DiagRightTop:
			return 1;
		default:
			return 0;		
		}
	}
	
	public int getBottomY() {
		return posY;
	}
	
	public int getTopY() {
		switch (this.shape) {
		case VertDouble:
			return (posY + 4);
		case HorzDouble:
		case Square:
		case VertHalf:
		case DiagLeftBottom:
		case DiagLeftTop:
		case DiagRightBottom:
		case DiagRightTop:
			return 2;
		case HorzHalf:
			return (posY + 1);		
		default:
			return posY;		
		}
	}
	
	public int getWidth() {
		switch (this.shape) {
		case VertDouble:
		case Square:
		case HorzHalf:
		case DiagLeftBottom:
		case DiagLeftTop:
		case DiagRightBottom:
		case DiagRightTop:
			return 2;
		case HorzDouble:
			return 4;
		case VertHalf:
			return 1;		
		default:
			return 0;		
		}
	}
	
	public int getHeight() {
		switch (this.shape) {
		case VertDouble:
			return 4;
		case HorzDouble:
		case Square:
		case VertHalf:
		case DiagLeftBottom:
		case DiagLeftTop:
		case DiagRightBottom:
		case DiagRightTop:
			return 2;
		case HorzHalf:
			return 1;		
		default:
			return 0;		
		}
	}	

	public iLine getLeftEdge() {
		switch (this.shape) {
		case VertDouble:
		case HorzDouble:
		case Square:
		case VertHalf:
		case HorzHalf:
		case DiagLeftBottom:
		case DiagLeftTop:
			return iLine.createLine(this.posX, this.posY, this.posX, this.posY + this.getHeight());
		case DiagRightBottom:
			return iLine.createLine(this.posX, this.posY, this.posX+2,  this.posY+2);
		case DiagRightTop:
			return iLine.createLine(this.posX+2, this.posY, this.posX,  this.posY+2);
		default:
			return null;
		}
	}

	public iLine getRightEdge() {
		switch (this.shape) {
		case VertDouble:
		case HorzDouble:
		case Square:
		case VertHalf:
		case HorzHalf:
		case DiagRightBottom:
		case DiagRightTop:
			return iLine.createLine(this.posX + this.getWidth(), this.posY,
								    this.posX + this.getWidth(), this.posY + this.getHeight());
		case DiagLeftBottom:
			return iLine.createLine(this.posX+2, this.posY, this.posX,  this.posY+2);
		case DiagLeftTop:
			return iLine.createLine(this.posX, this.posY, this.posX+2,  this.posY+2);
		default:
			return null;
		}
	}

	public iLine[] getSplitPoints() {
		if (!this.isSplittable())
			return new iLine[0];
		iLine[] returnArray;
		switch (this.shape) {
		case DiagLeftBottom:
		case DiagLeftTop:
		case DiagRightBottom:
		case DiagRightTop:
		case VertHalf:
		case HorzHalf:
		case VertDouble:
			returnArray = new iLine[1];
			returnArray[0] = iLine.createLine(this.posX, this.posY+2, this.posX+2, this.posY+2);
			return returnArray;
		case HorzDouble:
			returnArray = new iLine[1];
			returnArray[0] = iLine.createLine(this.posX+2, this.posY, this.posX+2, this.posY+2);
			return returnArray;
		case Square:
			returnArray = new iLine[4];
			returnArray[0] = iLine.createLine(this.posX+1, this.posY, this.posX+1, this.posY+2); //vert split
			returnArray[1] = iLine.createLine(this.posX, this.posY+1, this.posX+2, this.posY+1); //horz split
			returnArray[2] = iLine.createLine(this.posX, this.posY, this.posX+2, this.posY+2); //slash split
			returnArray[3] = iLine.createLine(this.posX, this.posY+2, this.posX+2, this.posY);
			return returnArray;
		default:
			return null;
		}
	}

	
	//a class that defines an immovable block useful for representing the ground to the engine
	static class GroundPiece extends GamePiece {
		
		public GroundPiece(Shape initShape, Material initMaterial, int x, int y) {
			super(initShape, initMaterial, x, y);
			fallingStatus = PhysicsStatus.Stable;
			leftSlopeStatus = PhysicsStatus.Stable;
			rightSlopeStatus = PhysicsStatus.Stable;
			LeftPushStatus = PhysicsStatus.Stable;
			RightPushStatus = PhysicsStatus.Stable;	
		}
		
		public GroundPiece() {
			this(null, null, 0, 0);
		}
	}
}
