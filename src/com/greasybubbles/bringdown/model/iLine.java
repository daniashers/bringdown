package com.greasybubbles.bringdown.model;

import java.util.ArrayList;

// a simple Line definition with pooling
public class iLine {

	private static final int MAX_POOL_SIZE = 200;
	
	public int x1, y1, x2, y2;

	//pool of instances
	private static ArrayList<iLine> pool = new ArrayList<iLine>();
	
	//private constructor
	private iLine() {
		//do nothing
	}

	//pool operations
	private static iLine getFromPool() {
		iLine inst;
		if (pool.size() > 0) {
			inst = pool.get(pool.size()-1);
			pool.remove(inst);
		} else {
			inst = new iLine();
		}
		return inst;
	}
	private static void putInPool(iLine inst) {
		if (pool.size() < MAX_POOL_SIZE) {
			pool.add(inst);
		}
	}
	
	//public factory methods
	public static iLine createLine(int x1, int y1, int x2, int y2) {
		iLine inst = getFromPool();
		inst.x1 = x1;
		inst.y1 = y1;
		inst.x2 = x2;
		inst.y2 = y2;
		return inst;
	}

	public void recycle() {
		putInPool(this);
	}
	
	//equals method
	public boolean CompareTo(iLine otherLine) {
		if (this.x1 == otherLine.x1 && this.y1 == otherLine.y1 &&
		    this.x2 == otherLine.x2 && this.y2 == otherLine.y2)
			return true;
		if (this.x1 == otherLine.x2 && this.y1 == otherLine.y2 &&
		    this.x2 == otherLine.x1 && this.y2 == otherLine.y1)
			return true;
		return false;
	}

	//other methods
	private boolean isHorzVertOrDiag() {
		if (x1 == x2) return true; //vert
		if (y1 == y2) return true; //horz
		if (Math.abs(x1-x2) == Math.abs(y1-y2)) return true; //diag
		return false;
	}
	private int blockLen() { //please note that diagonal length is considered same as vertical...
		if (!isHorzVertOrDiag()) return 0;
		return Math.max( Math.abs(x1-x2) , Math.abs(y1-y2));
	}
	private iLine subSegment(int start, int end) {
		//TODO horrendously inefficiently. Rewrite.
		int len = this.blockLen();
		if (start < 0 || start > len || end < 0 || end > len || start > end) return null;
		
		//xvers keeps track of whether going to x1 to x2 the x goes up or down or constant
		int xvers;
		if (x1 < x2) xvers = 1;
		else if (x1 > x2) xvers = -1;
		else xvers = 0;

		//xvers keeps track of whether going to y1 to y2 the y goes up or down or constant
		int yvers; 
		if (y1 < y2) yvers = 1;
		else if (y1 > y2) yvers = -1;
		else yvers = 0;
				
		int newx1 = x1 + start*xvers;
		int newy1 = y1 + start*yvers;
		int newx2 = x1 + end*xvers;
		int newy2 = y1 + end*yvers;

		return iLine.createLine(newx1, newy1, newx2, newy2);
	}

	//to understand the thing below: imagine a line FOUR blocks long: ----
	//we will be returning all the possible sublines, namely:
	//=---, -=--, --=-, ---=, ==--, -==-, --==, ===-, -===, ====.
	public iLine[] GetSubsegments() {
		int len = this.blockLen();
		int noOfSubsegments = (len*len + len) / 2; //you can convince yourself of this by thinking!
		iLine[] segms = new iLine[noOfSubsegments];
		int j=0;
		for (int subLen = len; subLen >= 1; subLen--) {
			for (int start = 0; start <= (len-subLen); start++) {
				segms[j++] = this.subSegment(start, start+subLen);
			}
		}
		assert (j == noOfSubsegments);
		return segms;
	}
	
	public int topY() {
		if (y1 > y2) return y1;
		else return y2;
	}
	
	public int bottomY() {
		if (y1 < y2) return y1;
		else return y2;
	}
	
	public int leftmostX() {
		if (x1 < x2) return x1;
		else return x2;
	}

	public int rightmostX() {
		if (x1 > x2) return x1;
		else return x2;
	}
	
	public float centreX() {
		return 0.5f*(x1 + x2);
	}
	
	public float centreY() {
		return 0.5f*(y1 + y2);
	}

	
	@Override
	public String toString() {
		return ("(" + this.x1 + "," + this.y1 + ")-(" + this.x2 + "," + this.y2 + ")");
	}
}
