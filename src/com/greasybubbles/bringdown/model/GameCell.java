package com.greasybubbles.bringdown.model;

import com.greasybubbles.bringdown.PieceList;
import com.greasybubbles.bringdown.model.GamePiece.Shape;

public class GameCell {
	
	// Variables
	//
	
	private GamePiece sectorA;
	private GamePiece sectorB;
	private GamePiece sectorC;
	private GamePiece sectorD;
	
	//imagining the cell divided by two diagonal lines, the sectors are as follows:
	//  _____________
	// |\           /|
	// |  \   A   /  |
	// |    \   /    |
	// | B    -    C |
	// |    /   \    |
	// |  /   D   \  |
	// |/___________\|
	
	// Constructors
	//
	
	GameCell() {
		this.clear();
	}
	
	// Methods
	//
	
	void clear() {
		sectorA = null;
		sectorB = null;
		sectorC = null;
		sectorD = null;
	}
	
	boolean isEmpty() {
		return ((sectorA == null) &&
				(sectorB == null) &&
				(sectorC == null) &&
				(sectorD == null));
	}
	
	boolean isSolid() {
		return ((sectorA == sectorB) && (sectorB == sectorC) && (sectorC == sectorD));
	}
	
	public GamePiece getContentTop() {
		return sectorA;
	}
	
	public GamePiece getContentLeft() {
		return sectorB;
	}
	
	public GamePiece getContentRight() {
		return sectorC;
	}
	
	public GamePiece getContentBottom() {
		return sectorD;
	}

	public PieceList getAllContents() {
		PieceList contents = new PieceList();
		if (sectorA != null)
			contents.add(sectorA);
		if (sectorB != null && !contents.contains(sectorB))
			contents.add(sectorB);
		if (sectorC != null && !contents.contains(sectorC))
			contents.add(sectorC);
		if (sectorD != null && !contents.contains(sectorD))
			contents.add(sectorD);
		return contents;
	}
	
	boolean ContainsShape(Shape s) {
		if (isEmpty()) return false;
		
		if (sectorA != null) {
			if (sectorA.shape == s) {
				return true;
			}
		}		
		if (sectorB != null) {
			if (sectorB.shape == s) {
				return true;
			}
		}
		if (sectorC != null) {
			if (sectorC.shape == s) {
				return true;
			}
		}
		if (sectorD != null) {
			if (sectorD.shape == s) {
				return true;
			}
		}		
		return false;
	}
	
	GamePiece GetPieceWithShape(Shape s) {
		if (isEmpty()) return null;
		
		if (sectorA != null) {
			if (sectorA.shape == s) {
				return sectorA;
			}
		}
		if (sectorB != null) {
			if (sectorB.shape == s) {
				return sectorB;
			}
		}
		if (sectorC != null) {
			if (sectorC.shape == s) {
				return sectorC;
			}
		}
		if (sectorD != null) {
			if (sectorD.shape == s) {
				return sectorD;
			}
		}

		
		return null;
	}
	

	void SetWholeCellTo(GamePiece p) {
		if (this.isEmpty()) {
			sectorA = p;
			sectorB = p;
			sectorC = p;
			sectorD = p;			
		} else {
			throw new RuntimeException();
		}
	}

	void SetHalfCellTo(GamePiece p) {
		switch (p.shape) {
			case DiagLeftBottom:
				if (sectorB == null) sectorB = p; else throw new RuntimeException();
				if (sectorD == null) sectorD = p; else throw new RuntimeException();
				break;
			case DiagLeftTop:
				if (sectorA == null) sectorA = p; else throw new RuntimeException();
				if (sectorB == null) sectorB = p; else throw new RuntimeException();
				break;
			case DiagRightBottom:
				if (sectorC == null) sectorC = p; else throw new RuntimeException();
				if (sectorD == null) sectorD = p; else throw new RuntimeException();
				break;
			case DiagRightTop:
				if (sectorA == null) sectorA = p; else throw new RuntimeException();
				if (sectorC == null) sectorC = p; else throw new RuntimeException();
				break;
			default:
				throw new RuntimeException();
			}
		return;
	}

}
