package com.greasybubbles.bringdown.model;

import com.greasybubbles.bringdown.PieceList;
import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.bringdown.model.GamePiece.PhysicsStatus;
import com.greasybubbles.bringdown.model.GamePiece.Shape;

import static com.greasybubbles.bringdown.Constants.*;

public class GridMath {

	static public boolean anyPiecesFalling;
	static public boolean anyPiecesSliding;	
	
	private static PieceList GetPiecesToTheLeftOf(GamePiece p, GameGrid grid) {
	
		PieceList returnedList = new PieceList();
				
		switch (p.shape) {
		case HorzDouble:
		case Square:
		case DiagLeftBottom:
		case DiagLeftTop:
		case VertHalf:
			//all these pieces have a flat left side 2 cells high.
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY+1).getAllContents());
			break;
		case VertDouble:
			//this piece has a flat left side 4 cells high.
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY+1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY+2).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY+3).getAllContents());
			break;
		case HorzHalf:
			//this piece has a flat left side 1 cell high.
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY  ).getAllContents());
			break;
		case DiagRightBottom:
			//this piece is treated specially
			for (GamePiece gp : grid.getCell(p.posX  , p.posY+1).getAllContents())
				if (gp.shape != Shape.DiagLeftTop) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX-1, p.posY  ).getAllContents())
				if (gp.shape != Shape.DiagLeftTop) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX  , p.posY  ).getAllContents())
				if (gp.shape == Shape.DiagLeftTop) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY+1).getAllContents())
				if (gp.shape == Shape.DiagLeftTop) returnedList.addUnique(gp);
			break;
		case DiagRightTop:
			//this piece is treated specially
			for (GamePiece gp : grid.getCell(p.posX  , p.posY  ).getAllContents())
				if (gp.shape != Shape.DiagLeftBottom) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX-1, p.posY+1).getAllContents())
				if (gp.shape != Shape.DiagLeftBottom) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY  ).getAllContents())
				if (gp.shape == Shape.DiagLeftBottom) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX  , p.posY+1).getAllContents())
				if (gp.shape == Shape.DiagLeftBottom) returnedList.addUnique(gp);
			break;
		}
		
		return returnedList;
	}

	private static PieceList GetPiecesToTheRightOf(GamePiece p, GameGrid grid) {
		
		PieceList returnedList = new PieceList();
		
		switch (p.shape) {
		case HorzDouble:
		case Square:
		case DiagRightBottom:
		case DiagRightTop:
		case VertHalf:
			//all these pieces have a flat right side 2 cells high.
			returnedList.addAllUnique(grid.getCell(p.posX+p.getWidth(), p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+p.getWidth(), p.posY+1).getAllContents());
			break;
		case VertDouble:
			//this piece has a flat right side 4 cells high.
			returnedList.addAllUnique(grid.getCell(p.posX+p.getWidth(), p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+p.getWidth(), p.posY+1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+p.getWidth(), p.posY+2).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+p.getWidth(), p.posY+3).getAllContents());
			break;
		case HorzHalf:
			//this piece has a flat right side 1 cell high.
			returnedList.addAllUnique(grid.getCell(p.posX+p.getWidth(), p.posY  ).getAllContents());
			break;
		case DiagLeftBottom:
			//this piece is treated specially
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY+1).getAllContents())
				if (gp.shape != Shape.DiagRightTop) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX+2, p.posY  ).getAllContents())
				if (gp.shape != Shape.DiagRightTop) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY  ).getAllContents())
				if (gp.shape == Shape.DiagRightTop) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX  , p.posY+1).getAllContents())
				if (gp.shape == Shape.DiagRightTop) returnedList.addUnique(gp);
			break;
		case DiagLeftTop:
			//this piece is treated specially
			for (GamePiece gp : grid.getCell(p.posX+2, p.posY+1).getAllContents())
				if (gp.shape != Shape.DiagRightBottom) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY  ).getAllContents())
				if (gp.shape != Shape.DiagRightBottom) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX  , p.posY  ).getAllContents())
				if (gp.shape == Shape.DiagRightBottom) returnedList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY+1).getAllContents())
				if (gp.shape == Shape.DiagRightBottom) returnedList.addUnique(gp);
			break;
		}

		return returnedList;
	}

	private static PieceList GetPiecesInLeftSlidePath(GameGrid grid, GamePiece p) {
		
		PieceList returnedList = new PieceList();
		
		switch (p.shape) {
		case HorzDouble:
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX  , p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+1, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX+3, p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX+3, p.posY-1).getContentLeft());			
			break;
		case VertDouble:
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+3).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+3).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY+2).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY+1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX  , p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentLeft());
			break;
		case Square:
		case DiagLeftBottom:
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX  , p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentLeft());
			break;
		case VertHalf:
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentLeft());
			break;
		case HorzHalf:
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY  ).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY  ).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX  , p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentLeft());
			break;
		case DiagLeftTop:
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX-1, p.posY  ).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY-1).getContentLeft());
			break;
		case DiagRightBottom:
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY-1).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY-1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX  , p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentLeft());
			break;
		case DiagRightTop:
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY+1).getContentBottom());
			
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY+1).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY+1).getContentBottom());
			
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY  ).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX-1, p.posY  ).getContentRight());
						
			returnedList.addAllUnique(grid.getCell(p.posX  , p.posY  ).getAllContents());
			
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY  ).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY  ).getContentBottom());
			
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentRight());
	
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentLeft());
			break;
		}

		return returnedList;
	}

	private static PieceList GetPiecesInRightSlidePath(GameGrid grid, GamePiece p) {
		
		PieceList returnedList = new PieceList();
		
		switch (p.shape) {
		case HorzDouble:
			returnedList.addUnique   (grid.getCell(p.posX+4, p.posY+1).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX+4, p.posY+1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX+4, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+4, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+3, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+1, p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentRight());			
			break;
		case VertDouble:
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY+3).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY+3).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY+2).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY+1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+1, p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentRight());
			break;
		case Square:
		case DiagRightBottom:
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY+1).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY+1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+1, p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentRight());
			break;
		case VertHalf:
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY+1).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY+1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX+1, p.posY  ).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+1, p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentRight());
			break;
		case HorzHalf:
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY  ).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY  ).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY-1).getAllContents());
			returnedList.addAllUnique(grid.getCell(p.posX+1, p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentRight());
			break;
		case DiagRightTop:
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY+1).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY+1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX+2, p.posY  ).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY-1).getContentRight());
			break;
		case DiagLeftBottom:
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY-1).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY-1).getContentBottom());
			returnedList.addAllUnique(grid.getCell(p.posX+1, p.posY-1).getAllContents());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentRight());
			break;
		case DiagLeftTop:
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY+1).getContentLeft());
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY+1).getContentBottom());
			
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY+1).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY+1).getContentBottom());
			
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY  ).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX+2, p.posY  ).getContentLeft());
						
			returnedList.addAllUnique(grid.getCell(p.posX+1, p.posY  ).getAllContents());
			
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY  ).getContentRight());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY  ).getContentBottom());
			
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX+1, p.posY-1).getContentLeft());
	
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentTop());
			returnedList.addUnique   (grid.getCell(p.posX  , p.posY-1).getContentRight());
			break;
		}

		return returnedList;
	}

	private static PieceList getUnsupportedPieces(GameGrid grid) {
		PieceList unsupList = new PieceList();

		grid.ResetPieceDynamics();
		
		//sorts the game pieces from bottom to top so we can
		//calculate the system with a minimum of recursion.
		grid.SortBottomToTop();
		
		for (GamePiece p : grid.getLivePieceList()) {
			if (!CheckIfSupported(grid, p))
				unsupList.addUnique(p);
		}		
		return unsupList;
	}

	private static PieceList getLeftSlidingPieces(GameGrid grid) {
		PieceList lSlideList = new PieceList();

		grid.ResetPieceDynamics();
		
		//sorts the game pieces from bottom to top so we can
		//calculate the system with a minimum of recursion.
		grid.SortBottomToTop();
		
		for (GamePiece p : grid.getLivePieceList()) {
			if (!CheckIfLeftSlopeStable(grid, p))
				lSlideList.addUnique(p);
		}		
		return lSlideList;
	}
	
	private static PieceList getRightSlidingPieces(GameGrid grid) {
		PieceList rSlideList = new PieceList();

		grid.ResetPieceDynamics();
		
		//sorts the game pieces from bottom to top so we can
		//calculate the system with a minimum of recursion.
		grid.SortBottomToTop();
		
		for (GamePiece p : grid.getLivePieceList()) {
			if (!CheckIfRightSlopeStable(grid, p))
				rSlideList.addUnique(p);
		}		
		return rSlideList;
	}
	
	private static PieceList GetPiecesBelow(GameGrid grid, GamePiece p) {

		PieceList pList = new PieceList();
		
		switch (p.shape) {
		case VertDouble:
		case Square:
		case HorzHalf:
		case DiagLeftBottom:
		case DiagRightBottom:
			//all these pieces have a flat bottom 2 cells wide.
			pList.addAllUnique(grid.getCell(p.posX  , p.posY-1).getAllContents());
			pList.addAllUnique(grid.getCell(p.posX+1, p.posY-1).getAllContents());
			break;
		case HorzDouble:
			//this piece has a flat bottom 4 cells wide.
			pList.addAllUnique(grid.getCell(p.posX  , p.posY-1).getAllContents());
			pList.addAllUnique(grid.getCell(p.posX+1, p.posY-1).getAllContents());
			pList.addAllUnique(grid.getCell(p.posX+2, p.posY-1).getAllContents());
			pList.addAllUnique(grid.getCell(p.posX+3, p.posY-1).getAllContents());
			break;
		case VertHalf:
			//this piece has a flat bottom 1 cell wide.
			pList.addAllUnique(grid.getCell(p.posX  , p.posY-1).getAllContents());
			break;
		case DiagLeftTop:
			//this piece is treated specially
			for (GamePiece gp : grid.getCell(p.posX  , p.posY-1).getAllContents())
				if (gp.shape != Shape.DiagRightBottom) pList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY  ).getAllContents())
				if (gp.shape != Shape.DiagRightBottom) pList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX  , p.posY  ).getAllContents())
				if (gp.shape == Shape.DiagRightBottom) pList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY+1).getAllContents())
				if (gp.shape == Shape.DiagRightBottom) pList.addUnique(gp);
			break;
		case DiagRightTop:
			//this piece is treated specially
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY-1).getAllContents())
				if (gp.shape != Shape.DiagLeftBottom) pList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX  , p.posY  ).getAllContents())
				if (gp.shape != Shape.DiagLeftBottom) pList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX+1, p.posY  ).getAllContents())
				if (gp.shape == Shape.DiagLeftBottom) pList.addUnique(gp);
			for (GamePiece gp : grid.getCell(p.posX  , p.posY+1).getAllContents())
				if (gp.shape == Shape.DiagLeftBottom) pList.addUnique(gp);
			break;
		}
		return pList;
	}

	public static LineType WhatLineType(int x1, int y1, int x2, int y2) {
		//This function attempts to determine whether the line specified matches one of the
		//"approved" types of lines that we will deal with in the game. Any other type or length
		//of line will be returned as "not valid".
		
		//swaps the two points so that point1 is always the lower one (lower y)
		if (y1 > y2) {
			int temp;
			temp = x1; x1 = x2; x2 = temp;
			temp = y1; y1 = y2; y2 = temp;
		}
		
		//if the line is a null line
		if (x1 == x2 && y1 == y2)
			return LineType.NotValid;
		
		switch (y2-y1) {
		case 0:
			if (Math.abs(x2-x1) == 2) return LineType.HorzSingle;
			else return LineType.NotValid;
		case 1:
			if ((x2-x1) == 0) return LineType.VertHalf;
			else  return LineType.NotValid;
		case 2:
			if ((x2-x1) == -2) return LineType.DiagBackslash;
			else if ((x2-x1) == 0) return LineType.VertSingle;
			else if ((x2-x1) == +2) return LineType.DiagSlash;
			else return LineType.NotValid;
		case 4:
			if ((x2-x1) == 0) return LineType.VertDouble;
			else  return LineType.NotValid;
		default:
			return LineType.NotValid;
		}
	}
	
	public static boolean MatchVerticalOrDiagonalEdge(GameGrid grid, int x1, int y1, int x2, int y2) {
	//returns whether the given pair of points (in grid coordinates)
	//represent a vertical or diagonal edge of any piece on the board.

		LineType lType = WhatLineType(x1,y1,x2,y2);

		//we are looking only for vertical or diagonal edges. So if the line is horizontal it's no good.
		if (lType == LineType.NotValid || lType == LineType.HorzSingle)
			return false;
				
		//swaps the two points if necessary so that y1 < y2 (point 1 is lower than point 2)
		//(for convenience of calculation later)
		if (y1 > y2) {
			int tempx = x1;
			int tempy = y1;
			x1 = x2;
			y1 = y2;
			x2 = tempx;
			y2 = tempy;
		}

		//here we go through all the pieces present and try to see if any one have coordinates
		//that match those of our line.
		for (GamePiece p : grid.getLivePieceList()) {
			//if the lower Y is not the same as the piece origin then it can't be an edge:
			//proceed to the next piece
			if (y1 != p.posY) continue;

			switch (p.shape) {
			case HorzDouble:
				if (lType == LineType.VertSingle && (x1 == p.posX || x1 == p.posX + 4)) return true;
				else break;
			case VertDouble:
				if (lType == LineType.VertDouble && (x1 == p.posX || x1 == p.posX + 2)) return true;
				else break;
			case Square:
				if (lType == LineType.VertSingle && (x1 == p.posX || x1 == p.posX + 2)) return true;
				else break;
			case HorzHalf:
				if (lType == LineType.VertHalf && (x1 == p.posX || x1 == p.posX + 2)) return true;
				else break;
			case VertHalf:
				if (lType == LineType.VertSingle && (x1 == p.posX || x1 == p.posX + 1)) return true;
				else break;
			case DiagLeftBottom:
				if ((lType == LineType.VertSingle && x1 == p.posX)||
						(lType == LineType.DiagBackslash && x1 == p.posX+2))  return true;
				else break;
			case DiagLeftTop:
				if ((lType == LineType.VertSingle && x1 == p.posX)||
						(lType == LineType.DiagSlash && x1 == p.posX))  return true;
				else break;
			case DiagRightBottom:
				if ((lType == LineType.VertSingle && x1 == p.posX+2)||
						(lType == LineType.DiagSlash && x1 == p.posX))  return true;
				else break;
			case DiagRightTop:
				if ((lType == LineType.VertSingle && x1 == p.posX+2)||
						(lType == LineType.DiagBackslash && x1 == p.posX+2))  return true;
				else break;
			default:
				return false;
			}
		}
		//if we arrived here it means that our line doesn't match any edges.
		return false;
	}

	private static PieceList GetPiecesToLeftOfLine(GameGrid grid, iLine line) {
		return GetPiecesToLeftOfLine(grid, line.x1, line.y1, line.x2, line.y2);
	}

	private static PieceList GetPiecesToRightOfLine(GameGrid grid, iLine line) {
		return GetPiecesToRightOfLine(grid, line.x1, line.y1, line.x2, line.y2);
	}

	
	private static PieceList GetPiecesToLeftOfLine(GameGrid grid, int x1, int y1, int x2, int y2) {
		PieceList pieces = new PieceList();
		
		if (x1 == x2) { //case of VERTICAL EXPLOSION
			if (x1 == MIN_GRID_X) //if line is at the leftmost of the field, nothing to do.
				return pieces;
			for ( int j = Math.min(y1, y2); j < Math.max(y1, y2); j++) {
				pieces.addUnique(grid.getCell(x1-1, j).getContentRight());
			} 
		} else if ((x2-x1 == y2-y1) && Math.abs(x2-x1) == 2) { //diag slash explosion
			pieces.addUnique(grid.getCell(Math.min(x1,x2), Math.min(y1,y2)).getContentLeft());
			pieces.addUnique(grid.getCell(Math.min(x1,x2)+1, Math.min(y1,y2)+1).getContentLeft());
		} else if ((x2-x1 == -(y2-y1)) && Math.abs(x2-x1) == 2) { //diag backslash explosion
			pieces.addUnique(grid.getCell(Math.max(x1,x2)-1, Math.min(y1,y2)).getContentLeft());
			pieces.addUnique(grid.getCell(Math.max(x1,x2)-2, Math.min(y1,y2)+1).getContentLeft());
		}
		return pieces;
	}

	private static PieceList GetPiecesToRightOfLine(GameGrid grid, int x1, int y1, int x2, int y2) {
		PieceList pieces = new PieceList();
		
		if (x1 == x2) { //case of VERTICAL EXPLOSION
			if (x1 == MAX_GRID_X) //if line is at the leftmost of the field, nothing to do.
				return pieces;
			for ( int j = Math.min(y1, y2); j < Math.max(y1, y2); j++) {
				pieces.addUnique(grid.getCell(x1, j).getContentLeft());
			}
		} else if ((x2-x1 == y2-y1) && Math.abs(x2-x1) == 2) { //diag slash explosion
			pieces.addUnique(grid.getCell(Math.min(x1,x2), Math.min(y1,y2)).getContentRight());
			pieces.addUnique(grid.getCell(Math.min(x1,x2)+1, Math.min(y1,y2)+1).getContentRight());
		} else if ((x2-x1 == -(y2-y1)) && Math.abs(x2-x1) == 2) { //diag backslash explosion
			pieces.addUnique(grid.getCell(Math.max(x1,x2)-1, Math.min(y1,y2)).getContentRight());
			pieces.addUnique(grid.getCell(Math.max(x1,x2)-2, Math.min(y1,y2)+1).getContentRight());
		}
		return pieces;
		//TODO: range checking when diagonal. I may end up checking a nonexistent cell. Please check
		//(also in the complementary LEFT version of this.
	}
		
	
	private static PieceList GetPiecesAffectedByBlast_Left(GameGrid grid, int x1, int y1, int x2, int y2) {

		PieceList totalPiecesAffected = new PieceList();
		PieceList newPiecesAffected = new PieceList();
		
		//STEP 1: calculate the VERY FIRST PIECES directly touched by the explosive.		
		newPiecesAffected = GetPiecesToLeftOfLine(grid, x1, y1, x2, y2);
		
		//STEP 2: propagate the forces to any other pieces touching laterally the ones pushed.
		do {
			// before adding any pieces to the final list of affected pieces,
			// remove any FIXED pieces from the initial set:
			// they won't move and won't transmit the force to other pieces.
			for (int i = newPiecesAffected.size() - 1; i >= 0; i--) {
				if (!newPiecesAffected.get(i).isMovable())
					newPiecesAffected.remove(i);
			}
			
			//add the new pieces to the total piece list.
			for (GamePiece p : newPiecesAffected) {
					totalPiecesAffected.addUnique(p);
			}
					
			//clone a copy of the new pieces array
			PieceList tempList = new PieceList(newPiecesAffected);
			newPiecesAffected.clear();
					
			//find out what "new new" pieces are present to the left of the "new pieces"
			for (GamePiece alreadyAffectedPiece : tempList) {
				for (GamePiece p : GridMath.GetPiecesToTheLeftOf(alreadyAffectedPiece, grid)) {
						newPiecesAffected.addUnique(p);
				}				
			}
		} while (!newPiecesAffected.isEmpty());
		
		return totalPiecesAffected;
	}
		
	private static PieceList GetPiecesAffectedByBlast_Right(GameGrid grid, int x1, int y1, int x2, int y2) {
		
		PieceList totalPiecesAffected = new PieceList();
		PieceList newPiecesAffected = new PieceList();
		
		//STEP 1: calculate the VERY FIRST PIECES directly touched by the explosive.
		newPiecesAffected = GetPiecesToRightOfLine(grid, x1, y1, x2, y2);
		
		//STEP 2: propagate the forces to any other pieces touching laterally the ones pushed.
		do {
			// before adding any pieces to the final list of affected pieces,
			// remove any FIXED pieces from the initial set:
			// they won't move and won't transmit the force to other pieces.
			for (int i = newPiecesAffected.size() - 1; i >= 0; i--) {
				if (!newPiecesAffected.get(i).isMovable())
					newPiecesAffected.remove(i);
			}
			
			//add the new pieces to the total piece list.
			for (GamePiece p : newPiecesAffected) {
					totalPiecesAffected.addUnique(p);
			}
					
			//clone a copy of the new pieces array
			PieceList tempList = new PieceList(newPiecesAffected);
			newPiecesAffected.clear();
					
			//find out what "new new" pieces are present to the left of the "new pieces"
			for (GamePiece alreadyAffectedPiece : tempList) {
				for (GamePiece p : GridMath.GetPiecesToTheRightOf(alreadyAffectedPiece, grid)) {
						newPiecesAffected.addUnique(p);
				}				
			}
		} while (!newPiecesAffected.isEmpty());			
		return totalPiecesAffected;
	}

	public static PieceList GetShatteredPieces(GameGrid grid, iLine explLine) {
		return GetShatteredPieces(grid, explLine.x1, explLine.y1, explLine.x2, explLine.y2);
	}
	
	public static PieceList GetShatteredPieces(GameGrid grid, int x1, int y1, int x2, int y2) {
		//finds any glass panes directly in contact with the explosion, they must be shattered:
		//between processing the pushes resulting from the blast!
		PieceList toBeShattered = new PieceList();
		for (GamePiece p :GetPiecesToLeftOfLine(grid, x1, y1, x2, y2)) {
			if (p.isShatterable()) {
				toBeShattered.addUnique(p);
			}
		}
		for (GamePiece p :GetPiecesToRightOfLine(grid, x1, y1, x2, y2)) {
			if (p.isShatterable()) {
				toBeShattered.addUnique(p);
			}
		}		
		return toBeShattered;
	}
	
	public static void CalculateLateralPushes(GameGrid grid, iLine explLine) {
		CalculateLateralPushes(grid, explLine.x1, explLine.y1, explLine.x2, explLine.y2);
	}
	
	public static void CalculateLateralPushes(GameGrid grid, int x1, int y1, int x2, int y2) {
		PieceList piecesL = new PieceList();
		PieceList piecesR = new PieceList();
		
		if (GetPiecesToLeftOfLine(grid, x1, y1, x2, y2).isEmpty() ||
				GetPiecesToRightOfLine(grid, x1, y1, x2, y2).isEmpty()) {
			//the concept here is that the force has to develop BETWEEN pieces.
			//if to one side of the explosion there is nothing, then the force does not really push
			//in the other direction but disperses through the gap. In other words if there aren't both
			//some pieces going left AND some pieces going right, then nothing will move.
			piecesL.clear();
			piecesR.clear();
		} else {
			//if there is 'something' on each side of the explosion, then we may proceed with
			//the proper calculations then.
			piecesL = GetPiecesAffectedByBlast_Left (grid, x1, y1, x2, y2);
			piecesR = GetPiecesAffectedByBlast_Right(grid, x1, y1, x2, y2);			
		}
		CalculateLateralSlidesLeft (grid, piecesL);
		CalculateLateralSlidesRight(grid, piecesR);		
	}
	
	public static void CalculateLateralSlidesLeft(GameGrid grid, PieceList piecesPushed) {

		PieceList movingPieces = piecesPushed;
		
		int totalWeightOfPiecesPushed = 0;
		for (GamePiece p : movingPieces)
			totalWeightOfPiecesPushed += p.getWeight();
		
		int cellsToGo;
		
		cellsToGo = TileMapper.CalculateDisplacementForWeight(totalWeightOfPiecesPushed);
		
		while (!movingPieces.isEmpty() && cellsToGo > 0) {
			movingPieces = getPiecesThatCanGoLeft(grid, new PieceList(movingPieces));
			for (GamePiece p : movingPieces) {
				p.posX -= 1;
				p.offsetX += 1.0f;
			}
			cellsToGo -- ;
			grid.PopulateGrid();
		}		
	}
		
	public static void CalculateLateralSlidesRight(GameGrid grid, PieceList piecesPushed) {

		PieceList movingPieces = new PieceList(piecesPushed);
		
		int totalWeightOfPiecesPushed = 0;
		for (GamePiece p : piecesPushed)
			totalWeightOfPiecesPushed += p.getWeight();
		
		int cellsToGo;
		
		cellsToGo = TileMapper.CalculateDisplacementForWeight(totalWeightOfPiecesPushed);
		
		
		while (!movingPieces.isEmpty() && cellsToGo > 0) {
			movingPieces = getPiecesThatCanGoRight(grid, new PieceList(movingPieces));
			for (GamePiece p : movingPieces) {
				p.posX += 1;
				p.offsetX -= 1.0f;
			}
			cellsToGo -- ;
			grid.PopulateGrid();
		}		
	}

	public static void CalculateDiagonalSlides(GameGrid grid) {
		
		//UNLIKE THE FALLING ANIMATIONS, THIS ROUTINE ONLY SLIDES THE PIECE ONE STEP RATHER
		//THAN SLIDING THEM ALL THE WAY UNTIL THEY STOP MOVING. THIS IS BECAUSE AFTER EACH
		//STEP WE WANT TO CHECK IF ANY PIECES ARE NOW ABLE TO FALL VERTICALLY.
		PieceList lSlidingPieces;
		PieceList rSlidingPieces;
	
		anyPiecesSliding = false;
		
		if (!(lSlidingPieces = GridMath.getLeftSlidingPieces(grid)).isEmpty()) {
			//TODO: add right slides in the same iteration
			for (GamePiece p : lSlidingPieces) {
				p.posY --;
				p.posX --;
				p.offsetY += 1.0f;
				p.offsetX += 1.0f;
			}
			grid.PopulateGrid();
			anyPiecesSliding = true;
		}
		
		if (!(rSlidingPieces = GridMath.getRightSlidingPieces(grid)).isEmpty()) {
			//TODO: add right slides in the same iteration
			for (GamePiece p : rSlidingPieces) {
				p.posY --;
				p.posX ++;
				p.offsetY += 1.0f;
				p.offsetX -= 1.0f;
			}
			grid.PopulateGrid();
			anyPiecesSliding = true;
		}
		
		//TODO: refine code to avoid mess and certain crash if some pieces are concurrently
		//sliding left and right!! (e.g. the first left slide frees up a right slide)
	}
	
	public static void CalculateFalls(GameGrid grid) {
		PieceList fallingPieces;
		
		while (!(fallingPieces = GridMath.getUnsupportedPieces(grid)).isEmpty()) {
			for (GamePiece p : fallingPieces) {
				p.posY --;
				p.offsetY += 1.0f;
			}
			grid.PopulateGrid();
		}
	}

	private static PieceList getPiecesThatCanGoLeft(GameGrid grid, PieceList piecesAlreadyMoving) {
		PieceList piecesMoving = new PieceList(piecesAlreadyMoving);

		//sorts the game pieces from bottom to top so we can
		//calculate the system properly.
		grid.SortLeftToRight(piecesMoving);
		
		PieceList markedForRemoval = new PieceList();
		
		//remove the pieces that are already at the limit of the screen.
		for (GamePiece p : piecesMoving) {
			if (p.posX == MIN_GRID_X)
				markedForRemoval.add(p);
		}
		for (GamePiece rp : markedForRemoval)
			piecesMoving.remove(rp);
		markedForRemoval.clear();
		
		//now do the proper calculations.
		for (GamePiece p : piecesMoving) {
			for (GamePiece gp : GridMath.GetPiecesToTheLeftOf(p, grid)) {
				if (!piecesMoving.contains(gp) || markedForRemoval.contains(gp))
					markedForRemoval.add(p);
			}
		}	
		for (GamePiece rp : markedForRemoval)
			piecesMoving.remove(rp);
		return piecesMoving;
	}

	private static PieceList getPiecesThatCanGoRight(GameGrid grid, PieceList piecesAlreadyMoving) {
		PieceList piecesMoving = new PieceList(piecesAlreadyMoving);

		//sorts the game pieces from bottom to top so we can
		//calculate the system properly.
		grid.SortRightToLeft(piecesMoving);
		
		PieceList markedForRemoval = new PieceList();
		
		//remove the pieces that are already at the limit of the screen.
		for (GamePiece p : piecesMoving) {
			if (p.posX + p.getWidth() >= MAX_GRID_X + 1)
				markedForRemoval.add(p);
		}
		for (GamePiece rp : markedForRemoval)
			piecesMoving.remove(rp);
		markedForRemoval.clear();
		
		//now do the proper calculations.
		for (GamePiece p : piecesMoving) {
			for (GamePiece gp : GridMath.GetPiecesToTheRightOf(p, grid)) {
				if (!piecesMoving.contains(gp) || markedForRemoval.contains(gp))
					markedForRemoval.add(p);
			}
		}	
		for (GamePiece rp : markedForRemoval)
			piecesMoving.remove(rp);
		return piecesMoving;
	}

	private static boolean CheckIfSupported(GameGrid grid, GamePiece piece)
	//RECURSIVE VERSION.
	{
		//if the piece is fixed, it's stable for sure.
		if (piece.isFixed()) {
			piece.fallingStatus  = PhysicsStatus.Stable;
			return true;
		}
		//if the piece had been already calculated, return the result.
		if (piece.fallingStatus == PhysicsStatus.Stable)
			return true;
		if (piece.fallingStatus == PhysicsStatus.Moving)
			return false;

		//otherwise calculate from scratch.

		assert (piece.fallingStatus  == PhysicsStatus.Undefined);

		boolean tempSupported = false;

		for (GamePiece p : GetPiecesBelow(grid, piece)) {
			if (CheckIfSupported(grid, p)) {
				tempSupported = true;
				break;
			}
		}
		
		//if after doing all these tests the value is still false,
		//then the piece is not supported. Return false.
		if (tempSupported == true) {
			piece.fallingStatus  = PhysicsStatus.Stable;
			return true;
		} else {
			piece.fallingStatus  = PhysicsStatus.Moving;
			return false;
		}
	}
	
	private static boolean CheckIfLeftSlopeStable(GameGrid grid, GamePiece piece)
	//RECURSIVE VERSION.
	//This assumes that any vertical falls have already been processed!!!
	{
		//if the piece is fixed, it's stable for sure.
		if (piece.isFixed()) {
			piece.leftSlopeStatus  = PhysicsStatus.Stable;
			return true;
		}
		//if the piece had been already calculated, return the result.
		if (piece.leftSlopeStatus == PhysicsStatus.Stable)
			return true;
		//if it is not stable, and it is not undefined, then it is sloping in some sort of way
		if (piece.leftSlopeStatus == PhysicsStatus.Moving)
			return false;

		//otherwise calculate from scratch.

		assert (piece.leftSlopeStatus == PhysicsStatus.Undefined);
		
		boolean tempSlopeStable = false;

		for (GamePiece p : GetPiecesInLeftSlidePath(grid, piece)) {
			if (CheckIfLeftSlopeStable(grid, p)) {
				tempSlopeStable = true;
				break;
			}
		}
		
		//if after doing all these tests the value is still false,
		//then the piece is not supported. Return false.
		if (tempSlopeStable == true) {
			piece.leftSlopeStatus = PhysicsStatus.Stable;
			return true;
		} else {
			piece.leftSlopeStatus = PhysicsStatus.Moving;
			return false;
		}
	}
	
	private static boolean CheckIfRightSlopeStable(GameGrid grid, GamePiece piece)
	//RECURSIVE VERSION.
	//This assumes that any vertical falls have already been processed!!!
	{
		//if the piece is fixed, it's stable for sure.
		if (piece.isFixed()) {
			piece.rightSlopeStatus  = PhysicsStatus.Stable;
			return true;
		}		
		//if the piece had been already calculated, return the result.
		if (piece.rightSlopeStatus == PhysicsStatus.Stable)
			return true;
		//if it is not stable, and it is not undefined, then it is sloping in some sort of way
		if (piece.rightSlopeStatus == PhysicsStatus.Moving)
			return false;

		//otherwise calculate from scratch.

		assert (piece.rightSlopeStatus == PhysicsStatus.Undefined);
		
		boolean tempSlopeStable = false;

		for (GamePiece p : GetPiecesInRightSlidePath(grid, piece)) {
			if (CheckIfRightSlopeStable(grid, p)) {
				tempSlopeStable = true;
				break;
			}
		}
		
		//if after doing all these tests the value is still false,
		//then the piece is not supported. Return false.
		if (tempSlopeStable == true) {
			piece.rightSlopeStatus = PhysicsStatus.Stable;
			return true;
		} else {
			piece.rightSlopeStatus = PhysicsStatus.Moving;
			return false;
		}
	}

	public static boolean hasPieceAnythingToPushAgainst(GameGrid grid, GamePiece piece) {
		if (!GetPiecesToLeftOfLine(grid, piece.getLeftEdge()).isEmpty()) return true;
		if (!GetPiecesToRightOfLine(grid, piece.getRightEdge()).isEmpty()) return true;
		return false;
	}
	
}
