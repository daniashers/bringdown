package com.greasybubbles.bringdown.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import com.greasybubbles.bringdown.Constants;
import com.greasybubbles.bringdown.PieceList;
import com.greasybubbles.bringdown.model.GamePiece.PhysicsStatus;
import com.greasybubbles.bringdown.model.GamePiece.Shape;
import com.greasybubbles.bringdown.uielements.Explosive;

import static com.greasybubbles.bringdown.Constants.*;


public class GameGrid {
	
	// Variables
	
	//The contents of the game field, in full.
	private GameCell gameGrid[][];

	private PieceList pieceList;
	private PieceList scrappedPieces;
	private PieceList recycledPieces;
	private PieceList blownPieces;

	public ArrayList<iLine> allValidLines;
	//this 'helper' array will store all valid edges, split points, etc etc to check against
	//possible lines drawn by the user. It's just to speed things up.
	
	Explosive currentExplosive;
		
	//these final objects will serve to define some "fictitious" cells that will be used
	//to represent the ground for our physics engine's purposes.
	private final GameCell SOLID_CELL;
	private final GameCell EMPTY_CELL;
	
	//
	// Constructor
	//
	public GameGrid() {
		pieceList = new PieceList();
		scrappedPieces = new PieceList();
		recycledPieces = new PieceList();
		blownPieces = new PieceList();

		allValidLines = new ArrayList<iLine>();
		
		gameGrid = new GameCell [TOTAL_X] [TOTAL_Y];
		for (int i=0; i<TOTAL_X; i++)
			for (int j=0; j<TOTAL_Y; j++)
				gameGrid[i][j] = new GameCell();
		this.ClearGrid();
		
		SOLID_CELL = new GameCell();
		SOLID_CELL.SetWholeCellTo(GamePiece.GROUND);
		EMPTY_CELL = new GameCell();
		EMPTY_CELL.clear();
	}
	
	// Methods
	
	public GameCell getCell(int u, int v) {
		//handles the special case of v < 0. In this case don't read from the "real" grid
		//(indexes below 0 don't exist) but return either empty or a solid cell representing
		//the raised central ground platform.
		//Also, cells below -5 are solid. This is to ensure that pieces don't "sink forever"
		//while still allowing for enough space for them to sink out of view before they hit the bottom.
		if (u < MIN_GRID_X || u > Constants.MAX_GRID_X)
			return SOLID_CELL;
		if (v < -5) {
			return SOLID_CELL;
		} else if (v < 0) {
			if (u >= Constants.ACTIVE_MIN_X && u <= Constants.ACTIVE_MAX_X)
				return SOLID_CELL;
			else
				return EMPTY_CELL;
		}
		//if it is a cell in a "normal", existing index, return the actual cell.
		return gameGrid[u][v];
	}
	
	void ClearGrid() {
		for (int i=0; i<TOTAL_X; i++)
			for (int j=0; j<TOTAL_Y; j++)
				gameGrid[i][j].clear();
	}
	
	void PlaceOnGrid(GamePiece piece) {
		int x = piece.posX;
		int y = piece.posY;
		Shape pieceShape = piece.shape;

		//TODO is the below kluge to get around errors when trying to place half sunken pieces
		//worth the loss of helpful crashes when trying to do something wrong? Might it lead
		//to bad things going undetected?
		if (y<0) return;
		
		switch(pieceShape) {
		case HorzDouble:
			gameGrid[x]  [y]  .SetWholeCellTo(piece);
			gameGrid[x+1][y]  .SetWholeCellTo(piece);
			gameGrid[x+2][y]  .SetWholeCellTo(piece);
			gameGrid[x+3][y]  .SetWholeCellTo(piece);
			gameGrid[x]  [y+1].SetWholeCellTo(piece);
			gameGrid[x+1][y+1].SetWholeCellTo(piece);
			gameGrid[x+2][y+1].SetWholeCellTo(piece);
			gameGrid[x+3][y+1].SetWholeCellTo(piece);
			break;
		case VertDouble:
			gameGrid[x]  [y]  .SetWholeCellTo(piece);
			gameGrid[x+1][y]  .SetWholeCellTo(piece);
			gameGrid[x]  [y+1].SetWholeCellTo(piece);
			gameGrid[x+1][y+1].SetWholeCellTo(piece);
			gameGrid[x]  [y+2].SetWholeCellTo(piece);
			gameGrid[x+1][y+2].SetWholeCellTo(piece);
			gameGrid[x]  [y+3].SetWholeCellTo(piece);
			gameGrid[x+1][y+3].SetWholeCellTo(piece);
			break;
		case Square:
			gameGrid[x]  [y]  .SetWholeCellTo(piece);
			gameGrid[x+1][y]  .SetWholeCellTo(piece);
			gameGrid[x]  [y+1].SetWholeCellTo(piece);
			gameGrid[x+1][y+1].SetWholeCellTo(piece);
			break;
		case HorzHalf:
			gameGrid[x]  [y]  .SetWholeCellTo(piece);
			gameGrid[x+1][y]  .SetWholeCellTo(piece);
			break;
		case VertHalf:
			gameGrid[x]  [y]  .SetWholeCellTo(piece);
			gameGrid[x]  [y+1].SetWholeCellTo(piece);
			break;
		case DiagLeftBottom:
			gameGrid[x]  [y]  .SetWholeCellTo(piece);
			gameGrid[x+1][y]  .SetHalfCellTo(piece);
			gameGrid[x]  [y+1].SetHalfCellTo(piece);
			break;
		case DiagLeftTop:
			gameGrid[x]  [y]  .SetHalfCellTo(piece);
			gameGrid[x]  [y+1].SetWholeCellTo(piece);
			gameGrid[x+1][y+1].SetHalfCellTo(piece);
			break;
		case DiagRightBottom:
			gameGrid[x]  [y]  .SetHalfCellTo(piece);
			gameGrid[x+1][y]  .SetWholeCellTo(piece);
			gameGrid[x+1][y+1].SetHalfCellTo(piece);
			break;
		case DiagRightTop:
			gameGrid[x+1][y]  .SetHalfCellTo(piece);
			gameGrid[x]  [y+1].SetHalfCellTo(piece);
			gameGrid[x+1][y+1].SetWholeCellTo(piece);
			break;
		default:
			throw new RuntimeException();
		}
	}
	
	//this both populates the grid with the piece data and also takes an inventory
	//of possible lines, edges, splitpoints etc.
	public void PopulateGrid() {		
		ClearGrid();
	
		allValidLines.clear();
		iLine tempLine;
		
		//add edges and split points
		for (GamePiece piece : pieceList) {
			PlaceOnGrid(piece);

			//left edge
			tempLine = piece.getLeftEdge();
			if (tempLine != null)
				allValidLines.add(tempLine);
			//right edge
			tempLine = piece.getRightEdge();
			if (tempLine != null)
				allValidLines.add(tempLine);
			//splitpoints
			allValidLines.addAll(Arrays.asList(piece.getSplitPoints()));
		}
			
	}

	public GamePiece getPieceAt(float gridX, float gridY)
	//this method returns the GamePiece under the given Grid coordinates
	{
		//bound check: touch has to be inside the active area!
		if (!(gridX >= MIN_GRID_X && gridX <= MAX_GRID_X)) return null;
		if (!(gridY >= ACTIVE_MIN_Y && gridY <= ACTIVE_MAX_Y)) return null;
		
		GameCell touchedCell = gameGrid[(int)gridX][(int)gridY];
		
		if (touchedCell.isEmpty()) return null;
		
		if (touchedCell.isSolid()) //if the cell is solid, it's pretty simple
			return touchedCell.getContentTop();
		else { //if it contains diagonal piece(s) it's a bit of a pain.
			float fractX = gridX % 1; //fractional part of x coordinate
			float fractY = gridY % 1; //fractional part of y coordinate

			//now this executes if there is some diagonal piece(s) in the cell.
			//it attempts to figure out based on the fractional coordinates of the touch
			//in which "sector" the touch is and checks if a piece is there.
			//Trying to understand this code may be harmful to your health.
			
			if (fractY < 1-fractX) {
				if (touchedCell.ContainsShape(Shape.DiagLeftBottom))
					return touchedCell.GetPieceWithShape(Shape.DiagLeftBottom);
			} else {
				if (touchedCell.ContainsShape(Shape.DiagRightTop))
					return touchedCell.GetPieceWithShape(Shape.DiagRightTop);
			}

			if (fractY > fractX) {
				if (touchedCell.ContainsShape(Shape.DiagLeftTop))
					return touchedCell.GetPieceWithShape(Shape.DiagLeftTop);
			} else {
				if (touchedCell.ContainsShape(Shape.DiagRightBottom))
					return touchedCell.GetPieceWithShape(Shape.DiagRightBottom);
			}
		}
		
		//if we reached this piece it means there really is nothing where we have touched.
		//return an empty piece.
		return null;
	}
	
	
	public void RemovePieceAsBlown(GamePiece piece) {
		if (pieceList.contains(piece)) {
			blownPieces.add(piece);
			RemovePiece(piece);
		}
	}

	public void RemovePieceAsScrapped(GamePiece piece) {
		if (pieceList.contains(piece)) {
			scrappedPieces.add(piece);
			RemovePiece(piece);
		}
	}
	
	public void RemovePieceAsRecycled(GamePiece piece) {
		if (pieceList.contains(piece)) {
			recycledPieces.add(piece);
			RemovePiece(piece);
		}
	}
	public void RemovePiece(GamePiece piece) {
		if (pieceList.contains(piece)) pieceList.remove(piece);
	}
	
	public void SplitPiece(GamePiece p, SplitMode mode) {
		switch (p.shape) {
		case HorzHalf:
		case VertHalf:
		case DiagLeftBottom:
		case DiagLeftTop:
		case DiagRightBottom:
		case DiagRightTop:
			throw new RuntimeException("Invalid piece splitting!");
		case VertDouble:
			if (mode == SplitMode.Horizontal) {
				pieceList.add(new GamePiece(Shape.Square, p.material, p.posX, p.posY));
				pieceList.add(new GamePiece(Shape.Square, p.material, p.posX, p.posY + 2));
				pieceList.remove(p);
			} else {
				throw new RuntimeException("Invalid piece splitting!");
			}
			break;
		case HorzDouble:
			if (mode == SplitMode.Vertical) {
				pieceList.add(new GamePiece(Shape.Square, p.material, p.posX, p.posY));
				pieceList.add(new GamePiece(Shape.Square, p.material, p.posX + 2, p.posY));
				pieceList.remove(p);
			} else {
				throw new RuntimeException("Invalid piece splitting!");
			}
			break;
		case Square:
			switch (mode) {
			case Horizontal:
				pieceList.add(new GamePiece(Shape.HorzHalf, p.material, p.posX, p.posY));
				pieceList.add(new GamePiece(Shape.HorzHalf, p.material, p.posX, p.posY + 1));
				pieceList.remove(p);
				break;
			case Vertical:
				pieceList.add(new GamePiece(Shape.VertHalf, p.material, p.posX, p.posY));
				pieceList.add(new GamePiece(Shape.VertHalf, p.material, p.posX + 1, p.posY));
				pieceList.remove(p);
				break;
			case DiagSlash:
				pieceList.add(new GamePiece(Shape.DiagLeftTop, p.material, p.posX, p.posY));
				pieceList.add(new GamePiece(Shape.DiagRightBottom, p.material, p.posX, p.posY));
				pieceList.remove(p);
				break;
			case DiagBackslash:
				pieceList.add(new GamePiece(Shape.DiagLeftBottom, p.material, p.posX, p.posY));
				pieceList.add(new GamePiece(Shape.DiagRightTop, p.material, p.posX, p.posY));
				pieceList.remove(p);
				break;
			default:
				throw new RuntimeException("Invalid piece splitting!");
			}
			break;
		default:
			throw new RuntimeException("Invalid piece splitting!");
		}
	}
	
	public PieceList getLivePieceList() {
		//danger! returns a reference to the REAL, live piece list and not a copy!
		//if the caller modifies it, it removes them for the actual game!
		return pieceList;
	}
			
	public PieceList getPieceList_NoFixed() {
		PieceList activePieces = new PieceList(pieceList);
		//remove all the fixed pieces before returning the list
		for (int i = activePieces.size()-1; i >= 0; i--) {
			if (activePieces.get(i).isFixed())
				activePieces.remove(i);
		}
		return activePieces;
	}
	


	
	public void SortBottomToTop() {
		Collections.sort(pieceList, new Comparator<GamePiece>() {
			public int compare(GamePiece a, GamePiece b) {
				if (a.posY > b.posY) {
					return +1;
				} else if (a.posY == b.posY) {
					if ((a.shape == Shape.DiagLeftBottom || a.shape == Shape.DiagRightBottom) &&
							(b.shape == Shape.DiagLeftTop || b.shape == Shape.DiagRightTop)) {
						return -1; 
					} else if ((b.shape == Shape.DiagLeftBottom || b.shape == Shape.DiagRightBottom) &&
							(a.shape == Shape.DiagLeftTop || a.shape == Shape.DiagRightTop)) {
						return 1;
					} else
						return 0;
				} else
					return -1;
			}
		});
	}
	
	public void SortLeftToRight(PieceList listOfPieces) {
		Collections.sort(listOfPieces, new Comparator<GamePiece>() {
			public int compare(GamePiece a, GamePiece b) {
				if (a.posX > b.posX) {
					return +1;
				} else if (a.posX == b.posX) {
					if ((a.shape == Shape.DiagLeftBottom || a.shape == Shape.DiagLeftTop) &&
							(b.shape == Shape.DiagRightBottom || b.shape == Shape.DiagRightTop)) {
						return -1; 
					} else if ((b.shape == Shape.DiagLeftBottom || b.shape == Shape.DiagLeftTop) &&
							(a.shape == Shape.DiagRightBottom || a.shape == Shape.DiagRightTop)) {
						return 1;
					} else
						return 0;
				} else
					return -1;
			}
		});
	}

	public void SortRightToLeft(PieceList listOfPieces) {
		Collections.sort(listOfPieces, new Comparator<GamePiece>() {
			public int compare(GamePiece a, GamePiece b) {
				if (a.posX < b.posX) {
					return +1;
				} else if (a.posX == b.posX) {
					if ((a.shape == Shape.DiagLeftBottom || a.shape == Shape.DiagLeftTop) &&
							(b.shape == Shape.DiagRightBottom || b.shape == Shape.DiagRightTop)) {
						return +1; 
					} else if ((b.shape == Shape.DiagLeftBottom || b.shape == Shape.DiagLeftTop) &&
							(a.shape == Shape.DiagRightBottom || a.shape == Shape.DiagRightTop)) {
						return -1;
					} else
						return 0;
				} else
					return -1;
			}
		});
	}

	public void ResetPieceDynamics() {
		for (GamePiece p : pieceList) {
			p.fallingStatus = PhysicsStatus.Undefined;
			p.leftSlopeStatus = PhysicsStatus.Undefined;
			p.rightSlopeStatus = PhysicsStatus.Undefined;			
		}
	}
	
	//private int GetGameStateHash() {
		//to know when it is time to repopulate the GameGrid array...
		//TODO...
		//return 0;
	//}
	
};
