package com.greasybubbles.bringdown;

import static com.greasybubbles.bringdown.Constants.*;

import org.andengine.util.color.Color;

//The purpose of this class is to provide a mapping between the shape
//of the pieces and the number of the graphics tile that represents it.

public class TileMapper {

	// Constants
	//
	
	
	// Methods
	//
	
	public static int getTileForStripType(ExplMode mode, LineType type) {
		int offset;
		if (mode == ExplMode.Apart) offset = 0;
		else if (mode == ExplMode.Split) offset = 0;
		else throw new RuntimeException("Wrong expl specified.");
		switch(type) {
		case VertDouble:
			return offset+0;
		case DiagSlash:
		case DiagBackslash:
			return offset+1;
		case VertSingle:
		case HorzSingle:
			return offset+2;
		case VertHalf:
			return offset+3;
		default:
			return -1;
		}
	}

	public static int XGridToScreen(float gridX)
	{
		return Math.round(GRID_START_X_ON_SCREEN + gridX * PIXELS_PER_CELL_ON_SCREEN);
	}
	
	public static int YGridToScreen(float gridY)
	{
		//TODO Y range checking
		return Math.round(GRID_START_Y_ON_SCREEN + (8.0f - gridY) * PIXELS_PER_CELL_ON_SCREEN);
	}



	public static float TouchXToGridCell(int touchX)
	{
		return (float)(touchX - GRID_START_X_ON_SCREEN) / PIXELS_PER_CELL_ON_SCREEN;
	}
	
	public static float TouchYToGridCell(int touchY)
	{
		return (8.0f - (float)(touchY - GRID_START_Y_ON_SCREEN) / PIXELS_PER_CELL_ON_SCREEN);
	}
	
	public static Color argbToColor(int argb) {
        return new Color(
        		((float) ((argb >> 16) & 0xff)) / 255f,
        		((float) ((argb >> 8 ) & 0xff)) / 255f,
        		((float) ((argb      ) & 0xff)) / 255f);
	}
	
	public static int CalculateDisplacementForWeight(int weight) {
		switch (weight) {
		case 1:
			return 4;
		case 2:
			return 3;
		case 3:
			return 2;
		case 4:
			return 1;
		default:
			return 0;
		}
	}
	
	//these functions combine the coords of a line between two int coordinate points
	//(within the range of the grid, so 0<X<16 and 0<Y<40) into a single INT via binary logic.
	public int ilMakeLine(int x1, int y1, int x2, int y2) {
		assert (x1 >= 0 && y1 >=0 && x2 >=0 && y2 >= 0); //TODO is this worth it/necessary?
		return (x1 << 24 + y1 << 16 + x2 << 8 + y2);
	}
	public int ilGetX1(int line) {
		return (line >> 24) & 0x7F;
	}
	public int ilGetY1(int line) {
		return (line >> 16) & 0x7F;
	}
	public int ilGetX2(int line) {
		return (line >> 8) & 0x7F;
	}
	public int ilGetY2(int line) {
		return line & 0x7F;
	}
	public int ilOrderByY(int line) { //swaps the line points so the first has always lower Y
		if (ilGetY1(line) < ilGetY2(line))
			return line;
		else {
			return ((line >> 16) & 0x00007F7F) | ((line << 16) & 0x7F7F0000);
		}
	}
}
