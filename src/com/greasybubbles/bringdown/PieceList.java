package com.greasybubbles.bringdown;

import java.util.ArrayList;

import com.greasybubbles.bringdown.model.GamePiece;

public class PieceList extends ArrayList<GamePiece> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9158333965257336035L;
	
	public PieceList() {
		super();
	}
	
	public PieceList(PieceList listToClone) {
		super(listToClone);
	}
	
	//convenience override: does the check for me whether it already
	//contains the element we are trying to add.

	/*@Override 
	public boolean add(GamePiece piece) {
		if (!this.contains(piece))
			return this.add(piece);
		else return false;
	}*/
	
	
	//a method that adds a piece to the list but not if the list already contains it.
	//It also allows null pieces to be passed and ignore them without generating errors.
	public void addUnique(GamePiece piece) {
		if (piece != null)
			if (!this.contains(piece))
				this.add(piece);
	}
	
	public void addAllUnique(PieceList list) {
		for (GamePiece piece : list)
			this.addUnique(piece);
	}
}
