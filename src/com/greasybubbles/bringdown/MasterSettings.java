package com.greasybubbles.bringdown;

import android.content.Context;
import android.content.SharedPreferences;

public class MasterSettings {

	private static SharedPreferences storedSettings;
	private static SharedPreferences storedProgress;

	static boolean loaded = false;
	
	private static Context context = null;
	
	public static final boolean DEBUG = true;
	
	public static boolean music = true;
	public static boolean sfx = true;
	
	public static boolean[] levelsWon = new boolean[Constants.NUM_LEVELS+1];
	public static int[] levelScores = new int[Constants.NUM_LEVELS+1];
	public static int[] levelTopMoves = new int[Constants.NUM_LEVELS+1];
	public static int[] levelTimesPlayed = new int[Constants.NUM_LEVELS+1];
	public static int[] levelTimesWon = new int[Constants.NUM_LEVELS+1];
	public static int[] levelAttemptsBeforeWin = new int[Constants.NUM_LEVELS+1];
	
	public static void setContext(Context c) {
		context = c;
	}
	
	public static void loadPreferences() {
		storedSettings = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
		storedProgress = context.getSharedPreferences("progress", Context.MODE_PRIVATE);
		
		music = storedProgress.getBoolean("music", true);
		sfx = storedProgress.getBoolean("sfx", true);
		
		for (int i = 0; i < Constants.NUM_LEVELS+1; i++) {
			levelsWon[i] = storedProgress.getBoolean("win"+String.format("%03d", i), false);
			levelScores[i] = storedProgress.getInt("score"+String.format("%03d", i), 0);
			levelTopMoves[i] = storedProgress.getInt("moves"+String.format("%03d", i), 0);
			levelTimesPlayed[i] = storedProgress.getInt("nplayed"+String.format("%03d", i), 0);
			levelTimesWon[i] = storedProgress.getInt("nwon"+String.format("%03d", i), 0);
			levelAttemptsBeforeWin[i] = storedProgress.getInt("tries"+String.format("%03d", i), 0);
		}
		
		loaded = true;
	}
	
	public static void saveLevelStats(boolean success, int level, int score, int moves) {
		if (!loaded) throw new RuntimeException("Trying to save data without first loading it.");
		if (level < 0) return; //negative levels are tutorials. There's no score to keep for them.

		SharedPreferences.Editor progressEditor = storedProgress.edit(); //prepare to edit saved data
		
		if (success) {
			if (!levelsWon[level]) { //is this the first time that we manage to win this level?
				levelsWon[level] = true;
				progressEditor.putBoolean("win"+String.format("%03d", level), true);
			}
			if (score > levelScores[level]) { //is this a new top score?
				levelScores[level] = score;
				progressEditor.putInt("score"+String.format("%03d", level), score);
			}
			if (moves < levelTopMoves[level]) { //a new record number of moves used? (the fewer the better)
				levelTopMoves[level] = moves;
				progressEditor.putInt("moves"+String.format("%03d", level), moves);
			}
			levelTimesWon[level]++;
			progressEditor.putInt("nwon"+String.format("%03d", level), levelTimesWon[level]);
		} else {
			if (!levelsWon[level]) {
				levelAttemptsBeforeWin[level]++;
				progressEditor.putInt("tries"+String.format("%03d", level), levelAttemptsBeforeWin[level]);
			}
		}
		levelTimesPlayed[level]++;
		progressEditor.putInt("nplayed"+String.format("%03d", level), levelTimesPlayed[level]);		
		
		progressEditor.commit();
	}
	
	public static void saveMusicSfxSettings(boolean musicOn, boolean sfxOn) {
		SharedPreferences.Editor soundSettingsEditor = storedSettings.edit(); //prepare to edit saved data
		soundSettingsEditor.putBoolean("music", musicOn);
		soundSettingsEditor.putBoolean("sfx", sfxOn);
		soundSettingsEditor.commit();		
	}
} 
