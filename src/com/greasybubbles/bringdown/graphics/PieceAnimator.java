package com.greasybubbles.bringdown.graphics;

import static com.greasybubbles.bringdown.Constants.CELL_GRAVITY_ACCELERATION;

import com.greasybubbles.bringdown.PieceList;
import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.bringdown.model.GameGrid;
import com.greasybubbles.bringdown.model.GamePiece;

public class PieceAnimator {

	// CONSTANTS
	
	private static final float DIAG_SLIDE_SPEED = 5.0f;
	
	private static boolean animating;
	
	//variables used by falling animation
	private static PieceList currentlyFallingPieces = new PieceList();
	private static float currentFallingSpeed;
	//variables used by lateral animation
	private static PieceList currentlyMovingPieces_L = new PieceList();
	private static PieceList currentlyMovingPieces_R = new PieceList();
	private static PieceList piecesThatWillKnock = new PieceList();
	private static float currentLateralSpeed_L;
	private static float currentLateralSpeed_R;
	private static float decayRate_L;
	private static float decayRate_R;
	

	private static PieceList recycleBin = new PieceList();
	public static PieceList sfxQueueThud = new PieceList();
	public static PieceList sfxQueue_L = new PieceList();
	public static PieceList sfxQueue_R = new PieceList();	
	public static PieceList sfxQueueSplash = new PieceList();
	
	public static void DoFallingAnimations(GameGrid grid, float timeElapsed) {
		
		//if this is the first time the method is called in this turn, set up the animations
		if (animating == false) {
			currentlyFallingPieces.clear();
			for (GamePiece p : grid.getLivePieceList()) {
				if (p.offsetY != 0.0f)
					currentlyFallingPieces.add(p);
			}
			currentFallingSpeed = 0.0f;
			animating = true;
		}

		sfxQueueThud.clear();
		sfxQueueSplash.clear();
		//(these queues are regularly checked by the calling scene in order
		//to know when to instantiate special effects
		
		//I add half the new speed now and half later, to better approximate
		//the trajectory curve piecewise (mid point integration)
		currentFallingSpeed += CELL_GRAVITY_ACCELERATION * timeElapsed / 2;

		recycleBin.clear();

		float fallingStep = currentFallingSpeed * timeElapsed;
		
		for (GamePiece p : currentlyFallingPieces) {
			p.offsetY -= fallingStep;
			if (p.offsetY <= 0.0f) {
				p.offsetY = 0.0f;
				recycleBin.add(p);
				sfxQueueThud.addUnique(p); //sets a flag that tells the scene to play relevant effect.
			}
			
			final int SPLASH_LINE = -1; //the Y value at which pieces are assumed to have impacted the water
			//if we have splashed tell the caller so it can play the special effect.
			//The double conditional with AND is to catch this ONLY when it FIRST splashes and not
			//when it is already under. We want the effect only when it first goes through the surface.
			if ((p.posY + p.offsetY < SPLASH_LINE) && (p.posY + p.offsetY + fallingStep > SPLASH_LINE)) {
				sfxQueueSplash.addUnique(p);
			}
		}
	
		for (GamePiece rp : recycleBin)
			currentlyFallingPieces.remove(rp);

		//I added half the new speed before and half now, to better approximate
		//the trajectory curve piecewise (mid point integration)
		currentFallingSpeed += CELL_GRAVITY_ACCELERATION * timeElapsed / 2;
		
		if (currentlyFallingPieces.isEmpty())
			animating = false;
	}
			
	public static void DoLateralSlideAnimations(GameGrid grid, float timeElapsed) {

		//the following mathematical things are designed to make the animations last
		//around the ANIM_TIME constant (whatever the distance the blocks travel)
		//and to achieve this a small linear component is added to avoid the very slow
		//speed towards the tail of the exponential curve. EXP_THRESHOLD controls this.
		
		final float EXP_THRESHOLD = 0.05f;
		final float ANIM_TIME = 1.1f;

		final float LINEAR_K = EXP_THRESHOLD / ANIM_TIME;
		
		if (animating == false) { //starts/sets up the animation
			currentlyMovingPieces_L.clear();
			currentlyMovingPieces_R.clear();

			int weight_L = 0;
			int weight_R = 0;
			
			// Determine which pieces are going left and which ones are going right, and also
			// calculates the total weight so that we know how fast they should go at the start
			for (GamePiece p : grid.getLivePieceList()) {
				if (p.offsetX != 0.0f) {
					if (p.offsetX > 0) {
						currentlyMovingPieces_L.add(p);
						weight_L += p.getWeight();
					} else {
						currentlyMovingPieces_R.add(p);
						weight_R += p.getWeight();
					}					
				}
			}

			//this is the max number of cells that they can travel in either direction,
			//and based on this we will select an appropriate initial speed and speed decay rate.
			int displacement_L = TileMapper.CalculateDisplacementForWeight(weight_L);
			int displacement_R = TileMapper.CalculateDisplacementForWeight(weight_R);

			//calculates the pieces that will slam against other stationary ones rather
			//than just slow to a stop... for the purpose of playing impact sfx.
			piecesThatWillKnock.clear();
			for (GamePiece p : currentlyMovingPieces_L) {
				if (displacement_L > Math.round(Math.abs(p.offsetX))) piecesThatWillKnock.addUnique(p);
			}
			for (GamePiece p : currentlyMovingPieces_R) {
				if (displacement_R > Math.round(Math.abs(p.offsetX))) piecesThatWillKnock.addUnique(p);
			}

			//now does some math magic formulas... wish I could explain it here.
			if (displacement_L > 0) decayRate_L = (float)Math.log((float)displacement_L/EXP_THRESHOLD) / ANIM_TIME;
			else decayRate_L = 0;
			if (displacement_R > 0) decayRate_R = (float)Math.log((float)displacement_R/EXP_THRESHOLD) / ANIM_TIME;
			else decayRate_R = 0;

			//Don't worry too much about understanding why the motion parameters are 
			//calculated this way. It's all mathematically proven to produce the desired effect...
			currentLateralSpeed_L = decayRate_L * displacement_L;
			currentLateralSpeed_R = decayRate_R * displacement_R;

			animating = true;
		}
			
		sfxQueue_L.clear();
		recycleBin.clear();		
		for (GamePiece p : currentlyMovingPieces_L) {
			p.offsetX = p.offsetX - (currentLateralSpeed_L + LINEAR_K) * timeElapsed;
			if (p.offsetX < 0) { //If the piece has already slid as far as it was planned to...
				p.offsetX = 0f;  //Ensure it's left with a round "zero" offset and not some small fraction
				if (piecesThatWillKnock.contains(p))
					sfxQueue_L.add(p); //If the piece was still going fast when it knocked, enqueue a sfx
				recycleBin.add(p); //Remove from the list of moving pieces.
			}
		}
		for (GamePiece rp : recycleBin) currentlyMovingPieces_L.remove(rp);

		sfxQueue_R.clear();
		recycleBin.clear();		
		for (GamePiece p : currentlyMovingPieces_R) {
			p.offsetX = p.offsetX + (currentLateralSpeed_R + LINEAR_K) * timeElapsed;
			if (p.offsetX > 0) { //if the piece has already slid as far as it was planned to...
				p.offsetX = 0f;  //ensure it's left with a round "zero" offset and not some small fraction
				if (piecesThatWillKnock.contains(p))
					sfxQueue_R.add(p);
				recycleBin.add(p);
			}
		}
		for (GamePiece rp : recycleBin) currentlyMovingPieces_R.remove(rp);
		
		//speed decays exponentially.
		currentLateralSpeed_L = currentLateralSpeed_L * (float)Math.exp(-decayRate_L * timeElapsed);
		currentLateralSpeed_R = currentLateralSpeed_R * (float)Math.exp(-decayRate_R * timeElapsed);
					
		if (currentlyMovingPieces_L.isEmpty() && currentlyMovingPieces_R.isEmpty())
			animating = false;
	}
	
	public static void DoDiagonalAnimations(GameGrid grid, float timeElapsed) {
		if (!animating) {
			currentlyMovingPieces_L.clear();
			currentlyMovingPieces_R.clear();

			for (GamePiece p : grid.getLivePieceList()) {
				if (p.offsetY > 0) {
					if (p.offsetX > 0) {
						currentlyMovingPieces_L.add(p);
					} else if (p.offsetX < 0) {
						currentlyMovingPieces_R.add(p);
					}
				}
			}
			animating = true;
		}

		//pre-calculates (to avoid re-calculating it over and over) the amount that our pieces will
		//move during this step. Note: Diagonal slides unlike falls and lateral slide are constant speed.
		float aniStep = DIAG_SLIDE_SPEED * timeElapsed;
		
		recycleBin.clear();
		for (GamePiece p : currentlyMovingPieces_L) {
			if (Math.abs(p.offsetX) > aniStep) {
				p.offsetX -= aniStep;
				p.offsetY -= aniStep;
			} else {
				p.offsetY = 0.0f;
				p.offsetX = 0.0f;
				recycleBin.add(p);
			}	
		}
		for (GamePiece rp : recycleBin) currentlyMovingPieces_L.remove(rp);
		
		recycleBin.clear();
		for (GamePiece p : currentlyMovingPieces_R) {
			if (Math.abs(p.offsetX) >= aniStep) {
				p.offsetX += aniStep;
				p.offsetY -= aniStep;
			} else {
				p.offsetY = 0.0f;
				p.offsetX = 0.0f;
				recycleBin.add(p);
			}	
		}
		for (GamePiece rp : recycleBin) currentlyMovingPieces_R.remove(rp);
		
		if (currentlyMovingPieces_L.isEmpty() && currentlyMovingPieces_R.isEmpty())
			animating = false;
	}
	
	public static boolean isAnimating() {
		return (animating);
	}
	
	public static boolean FinishedAnimating() {
		return (!animating);
	}
	
}
