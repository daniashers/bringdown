package com.greasybubbles.bringdown.graphics;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.util.color.Color;

import android.graphics.Typeface;


public class FontAsset {
	private Font fFont;
	
	public FontAsset(String fileName, float size, int width, int height, GraphicSystemParameters params) {
		fFont = FontFactory.createFromAsset(params.fontManager, params.textureManager, width, height,
				params.context.getAssets(), fileName, size, true, Color.WHITE_ARGB_PACKED_INT);
		fFont.load();
	}
	
	public FontAsset(float size, int width, int height, GraphicSystemParameters params) {
		fFont = FontFactory.create(params.fontManager, params.textureManager, width, height,
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), size, true, Color.WHITE_ARGB_PACKED_INT);
		fFont.load();
	}
	
	public Font getFont() {
		return fFont;
	}
	
	//TODO add a 'destructor' that releases the textures
}
