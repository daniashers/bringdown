package com.greasybubbles.bringdown.graphics;

import java.util.Random;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyTheme;
import com.greasybubbles.greasytools.GreasyScene;

public class GbgCityBackground extends GreasyTheme {

	private static final int TOTAL_WIDTH = 800; //pixels
	private static final int TOTAL_HEIGHT = 480;
	
	private static final int NUM_LANES = 8;
	private static final int NUM_CARS_SLOW = 40; //cars per line (4 lines)
	private static final int NUM_CARS_FAST = 8;
	private static final float CAR_Y_BOTTOM = TOTAL_HEIGHT; //the y values at top/bottom of active screen
	private static final float HORIZON_Y = 200;   //at which cars will spawn/be removed
	private static final float CAR_RELATIVE_X_BOTTOM[] = {-.4f, -.3f, -.15f, -.05f, 1.05f, 1.15f, 1.3f, 1.4f}; //% of screen width
	private static final float NEAR_Z = 10; //z = distance from observer
	private static final float FAR_Z = 150;
	private static final float CAR_SPEED = 2f;

	private static final float SCALE_VARIATION = 0.3f;
	private static final float SPACING_VARIATION = 0.5f; //percentage of standard spacing
	private static final float FLICKER_RANGE = 0.6f; //the % by which we allow the car lights to flicker
	private static final float FLICKER_RATE = 6f;
	private static final float ZVAR_RANGE = 0.4f; //the % by which we allow car spacings to dynamically vary
	private static final float ZVAR_RATE = 1f;
	
	
	private CarSprite[][] cars;
	private Sprite background;

	private final float NEAR_SCALE = 1;
	
	private Random rnd = new Random();
	
	public GbgCityBackground(GreasyScene mScene) {
		super(mScene);
	
		//sorts out the background city image
		Rectangle blackRect = new Rectangle (-1, -1, TOTAL_WIDTH+2, TOTAL_HEIGHT+2, mScene.vboManager);
		blackRect.setColor(Color.BLACK);
		this.attachChild(blackRect);
		background = new Sprite(0f, 0f, GreasyAssets.getTexture("trafficbg"), mScene.vboManager);
		this.attachChild(background);

		//generates the car sprites
		TextureRegion carLights = GreasyAssets.getTexture("carlights");
		cars = new CarSprite[NUM_LANES][];
		for (int lane = 0; lane<NUM_LANES; lane++)
			cars[lane] = new CarSprite[(isFastLane(lane) ? NUM_CARS_FAST : NUM_CARS_SLOW )];

		for (int lane = 0; lane < NUM_LANES; lane++) {
			for (int i = 0; i < cars[lane].length; i++) {
				cars[lane][i] = new CarSprite(0f, 0f, carLights, mScene.vboManager);
				if (isSeenFromBehind(lane))
					cars[lane][i].setColor(Color.RED);
				else
					cars[lane][i].setColor(getRandomHeadlightColour());
				float carSpacing = (FAR_Z - NEAR_Z)/(isFastLane(lane) ? NUM_CARS_FAST : NUM_CARS_SLOW );
				cars[lane][i].z = NEAR_Z + carSpacing * (i + rnd.nextFloat()*SPACING_VARIATION); //spaces cars equally
				//^the cars moving away from us are seen from the back therefore RED tail-lights
				cars[lane][i].relativeSize = 1 - rnd.nextFloat()*SCALE_VARIATION;
				this.attachChild(cars[lane][i]);
			}
		}
		
		background.detachSelf();
		this.attachChild(background);
		//^detach and reattach the background image so that it's on top of all the cars now (this means 
		//that if a car gets too close and overlaps the buildings, it will go 'behing' i.e. disappear)
		
		UpdateCars(0);
	}

	private boolean isSeenFromBehind(int laneNumber) {
		if (laneNumber == 0 || laneNumber == 1)
			return true;
		else if (laneNumber == 4 || laneNumber == 5)
			return true;
		else
			return false;
	}
	
	private boolean isFastLane(int laneNumber) {
		if (laneNumber == 1 || laneNumber == 2)
			return true;
		else if (laneNumber == 5 || laneNumber == 6)
			return true;
		else return false;
	}
	
	private float getScaleForZ(float z) {
		return NEAR_SCALE * NEAR_Z / z; 
	}
	
	private float getXForZ(float z, int lane) {
		final float X_CENTRE = TOTAL_WIDTH / 2;
		return X_CENTRE + TOTAL_WIDTH * (getScaleForZ(z) * (CAR_RELATIVE_X_BOTTOM[lane] - .5f)) - 32;
		//^ the "minus 32" is because the sprite is 64 pixels wide and we want to handle it 'by its centre'
	}
	
	private float getYForZ(float z) {
		return HORIZON_Y + getScaleForZ(z) * (CAR_Y_BOTTOM - HORIZON_Y);
	}

	private float getAlphaForZ(float z) {
		return 1;
	}

	private Color getRandomHeadlightColour() {
		int rndNumber = rnd.nextInt(3);
		switch (rndNumber) {
		case 0:
			return Color.WHITE;
		case 1:
			return new Color(1f,1f,0.80f); //yellowish
		case 2:
			return new Color(0.90f,0.90f,1f); //bluish
		default:
			return Color.WHITE;
		}
	}
	
	private void UpdateCars(float timeElapsed) {
		for (int lane = 0; lane < NUM_LANES; lane++) {
			for (CarSprite thisCar : cars[lane]) {
				if (isSeenFromBehind(lane)) {
					thisCar.z += CAR_SPEED * timeElapsed * (isFastLane(lane) ? 1.6 : 1);
					//wrap around position if reached the limit
					if (thisCar.z > FAR_Z) thisCar.z -= (FAR_Z - NEAR_Z);
				} else {
					thisCar.z -= CAR_SPEED * timeElapsed * (isFastLane(lane) ? 1.6 : 1);
					//wrap around position if reached the limit
					if (thisCar.z < NEAR_Z) thisCar.z += (FAR_Z - NEAR_Z);
				}
			}
		}

		float currentIntensity;
		float currentZVariation;
		float thisZ;
		
		for (int lane = 0; lane < NUM_LANES; lane++) {
			for (CarSprite thisCar : cars[lane]) {
				//makes car lights flicker
				
				currentIntensity = thisCar.intensity;
				currentIntensity += (rnd.nextFloat()-.5f) * FLICKER_RATE*timeElapsed;
				if (currentIntensity > 1) currentIntensity = 1;
				if (currentIntensity < (1 - FLICKER_RANGE)) currentIntensity = 1 - FLICKER_RANGE;
				thisCar.intensity = currentIntensity;
				thisCar.setAlpha(currentIntensity);
		
				//varies the spacing between cars in a random fashion
		
				currentZVariation = thisCar.zVar;
				currentZVariation += (rnd.nextFloat()-.5f) * ZVAR_RATE*timeElapsed;
				if (currentZVariation > ZVAR_RANGE/2) currentZVariation = ZVAR_RANGE/2;
				if (currentZVariation < -ZVAR_RANGE/2) currentZVariation = -ZVAR_RANGE/2;
				thisCar.zVar = currentZVariation;
		
				//render cars
	
				thisZ = thisCar.z + thisCar.zVar;
				thisCar.setScale(getScaleForZ(thisZ) * thisCar.relativeSize);
				thisCar.setX(getXForZ(thisZ, lane));
				thisCar.setY(getYForZ(thisZ));
				thisCar.setAlpha(thisCar.getAlpha()*getAlphaForZ(thisZ));
			}
		}
	}
	
	@Override
	public void Update(float timeElapsed) {
		UpdateCars(timeElapsed);
	}

	//internal class, slightly expanded Sprite
	class CarSprite extends Sprite {
		public float intensity = 1;
		public float relativeSize = 1;
		public float z;
		public float zVar = 0;
		
		public CarSprite(float x, float y, TextureRegion tr, VertexBufferObjectManager vbom) {
			super(x,y,tr,vbom);
		}
	}
	
}
