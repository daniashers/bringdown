package com.greasybubbles.bringdown.graphics;

import java.util.Random;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.color.Color;

import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyTheme;
import com.greasybubbles.greasytools.GreasyScene;

public class GbgSwampBackground extends GreasyTheme {

	static final String[] reqdAssets = { "swampbg" };
	static final int NUM_BANDS = 64;
	static final float TURBULENCE_RANGE = 0.2f; //how much the bands will move and stretch (as % of height)
	static final float MASTER_RATE = (float)(2*Math.PI)/60; //default clock is one cycle/minute
	static final float RATE_MULTIPLIER = 35f;
	static final float HORIZONTAL_PIXEL_WOBBLE = 0.65f;
	static final float REFLECTION_Y_ORIGIN = 275; //the line below which the reflection begins
	//ratio between the thickness of two consecutive bands of water surface (the relative increment)
	static final float THICKNESS_RATIO = 1.1f;
	
	static final int NUM_FLIES = 8;
	
	final float totalWidth;
	
	private TextureRegion bgRegion;
	private float[] bandRestPosition;
	private float[] bandDynamicPosition;
	private float[] bandThickness;
	private float[] bandPhaseOffset;
	private Sprite[] bandSprites;
	private Rectangle cloudiness;
	private Sprite rightSideUpSwamp;

	private Random rnd = new Random();
	
	//FLIES
	private Rectangle[] flies = new Rectangle[NUM_FLIES];
	private float[] flyVelX = new float[NUM_FLIES];
	private float[] flyVelY = new float[NUM_FLIES];	
	private float[] attractX = new float[NUM_FLIES];
	private float[] attractY = new float[NUM_FLIES];

	private float masterPhase = 0;
	private float bandScales[];

	public GbgSwampBackground(GreasyScene mScene) {
		super(mScene);

		bandRestPosition = new float[NUM_BANDS];
		bandDynamicPosition = new float[NUM_BANDS];
		bandThickness = new float[NUM_BANDS];
		bandSprites = new Sprite[NUM_BANDS];
		bandScales = new float[NUM_BANDS];
		bandPhaseOffset = new float[NUM_BANDS];
		
		//loads the texture we use for the background
		bgRegion = GreasyAssets.getTexture("swampbg");
		totalWidth = bgRegion.getWidth();
		float reflectionHeight = REFLECTION_Y_ORIGIN;
		//^^ the height of the background region that will be reflected goes from the top of the screen
		//(y=0) to the line below which the reflection begins (REFLECTION_Y_ORIGIN). So numerically
		//it is equal to REFLECTION_Y_ORIGIN.
		
		//calculates the phase offset of each band
		for (int i=0; i<NUM_BANDS; i++) {
			bandPhaseOffset[i] = rnd.nextFloat()*2*(float)Math.PI;
		}
		
		//calculates thickness of each band
		float[] bandThickness_arbitraryUnits = new float[NUM_BANDS];
		float totalThickness_arbitraryUnits = 0;
		//in these arbitrary units, the thickness of the thinnest band is 1
		for (int i=0; i<NUM_BANDS; i++) {
			float t = (float)Math.max(Math.pow(THICKNESS_RATIO, i), Math.pow(THICKNESS_RATIO, NUM_BANDS/2));
			bandThickness_arbitraryUnits[i] = t;
			totalThickness_arbitraryUnits += t;
		}
		//convert the arbitrary units into real units with the constraint
		//that total thickness = height of the background texture up to the reflection origin line
		final float CONVERSION_FACTOR = reflectionHeight / totalThickness_arbitraryUnits;
		for (int i=0; i<NUM_BANDS; i++) {
			bandThickness[i] = bandThickness_arbitraryUnits[i] * CONVERSION_FACTOR;
			if (i == 0) {
				bandRestPosition[i] = 0;
			} else {
				bandRestPosition[i] = bandRestPosition[i-1] + bandThickness[i-1];
			}
		}
		
		//assigns to each band the relevant portion of texture of the image
		//(remember that the image is upside down (vertically flipped) as it is the reflection)
		for (int i = 0; i < NUM_BANDS; i++) {
			TextureRegion thisBandTexture = bgRegion.deepCopy();
			thisBandTexture.setTextureHeight(bandThickness[i]);
			//our 'band positions' are made in reference to the reflected (vertically flipped) image.
			//in order to assign the correct texture portion of the UNFLIPPED image, we must calculate this:
			float textureY_in_unflipped_texture = reflectionHeight - bandRestPosition[i] - bandThickness[i];
			//^^draw a diagram to convince yourself of the above
			thisBandTexture.setTextureY(textureY_in_unflipped_texture);
			bandSprites[i] = new Sprite(0f, 0f, thisBandTexture, mScene.vboManager);
			bandSprites[i].setScaleCenter(0,0);
			bandSprites[i].setScaleX((float)(totalWidth + HORIZONTAL_PIXEL_WOBBLE*2.5) / totalWidth);
			//^ scales the sprites horizontally VERY slightly so that it is a tiny bit
			//wider than the screen to allow for a small horizontal oscillation effect without 
			//revealing any gaps at the edges of the screen.
			bandSprites[i].setFlippedVertical(true);
			this.attachChild(bandSprites[i]);
		}
		masterPhase = 0;

		//now on top of the bands I attach a plain-coloured rectangle, semi-transparent, that will
		//have the effect of making the reflection of the water less sharp and a bit cloudy.
		cloudiness = new Rectangle(0f, REFLECTION_Y_ORIGIN, totalWidth, reflectionHeight, mScene.vboManager);
		cloudiness.setColor(new Color(0.64f, 0.70f, 0.61f));
		cloudiness.setAlpha(0.5f);
		this.attachChild(cloudiness);
		
		//finally attaches the image of the swamp "right side up". I attach it last so it will
		//be layered ABOVE all of the layers of water.
		rightSideUpSwamp = new Sprite(0f, 0f, bgRegion, mScene.vboManager);
		this.attachChild(rightSideUpSwamp);
		
		UpdateBands();
		
		
		//FLIES
		for (int j = 0; j < NUM_FLIES; j++) {
			flies[j] = new Rectangle(0,0,3,2,mScene.vboManager);
			flies[j].setColor(Color.BLACK);
			flies[j].setX(rnd.nextFloat()*totalWidth);
			flies[j].setY(rnd.nextFloat()*REFLECTION_Y_ORIGIN);
			flyVelX[j] = 0;
			flyVelY[j] = 0;
			attractX[j] = rnd.nextFloat()*totalWidth;
			attractY[j] = rnd.nextFloat()*REFLECTION_Y_ORIGIN;
			this.attachChild(flies[j]);
		}
	}

	private void UpdateBands() {
		//calculates the new positions of the bands after distorting them to create a 'wave effect'
		bandDynamicPosition[0] = 0; //the first band is a special case, its top is always anchored to the screen top
		for (int i = 1; i < NUM_BANDS; i++) {
			float thisBandPhase = masterPhase*RATE_MULTIPLIER + bandPhaseOffset[i];
			float thisBandDistortedPosition = bandRestPosition[i] + 
					(float)Math.sin(thisBandPhase) * TURBULENCE_RANGE * bandThickness[i];
			if (thisBandDistortedPosition < bandDynamicPosition[0])
				(thisBandDistortedPosition) = bandDynamicPosition[0];
			bandDynamicPosition[i] = thisBandDistortedPosition;
		}
		//calculates the new vertical scale for each band (the last one is a special case)
		for (int i = 0; i < NUM_BANDS - 1; i++) {
			float thisBandRequiredHeight = bandDynamicPosition[i+1] - bandDynamicPosition[i];
			thisBandRequiredHeight++; //adds one just to cover any 'gaps' between the bands
										//that may arise due to rounding/sync.
			float thisBandScaleRequired = thisBandRequiredHeight / bandThickness[i];
			bandScales[i] = thisBandScaleRequired;
		}
		int lastBand = NUM_BANDS-1;
		float bottomOfReflectionY = bandRestPosition[lastBand] + bandThickness[lastBand];
		bandScales[NUM_BANDS-1] = (bottomOfReflectionY - bandDynamicPosition[lastBand]) / 
				bandThickness[lastBand];
				
		//now apply the bands positions and scales
		for (int i = 0; i < NUM_BANDS; i++) {
			bandSprites[i].setY(REFLECTION_Y_ORIGIN + bandDynamicPosition[i]);
			bandSprites[i].setScaleY(bandScales[i]);
		}
		//apply the small horizontal wobble
		for (int i = 0; i < NUM_BANDS; i++) {
			final float WOBBLE_OFFSET = 0.25f;
			float thisBandPhase = masterPhase*RATE_MULTIPLIER + bandPhaseOffset[i] + WOBBLE_OFFSET;
			bandSprites[i].setX(HORIZONTAL_PIXEL_WOBBLE*(float)Math.sin(thisBandPhase));
		}
		//apply colour variation to simulate light reflection
		for (int i = 0; i < NUM_BANDS; i++) {
			float thisBandPhase = masterPhase*RATE_MULTIPLIER + bandPhaseOffset[i];
			float colourParameter = .5f+.5f*(.5f*(1+(float)Math.sin(thisBandPhase)));
			bandSprites[i].setColor(new Color(colourParameter,colourParameter,colourParameter));
		}
		
		
	}

	private void UpdateFlies(float timeElapsed) {
		final float MAX_VEL = 80;
		final float FLY_ACCEL = 200;
		final float FLY_PROXIMITY= 40;
		
		// FLIES
		// this is a very rough random motion simulator whereby each fly is attracted by a random
		// point and the point changes when the fly gets close to it
		// PURPOSEFULLY NOT COMMENTED!
		for (int j = 0; j < NUM_FLIES; j++) {
			if (attractX[j] > flies[j].getX()) flyVelX[j] += FLY_ACCEL*timeElapsed;
			else flyVelX[j] += -FLY_ACCEL*timeElapsed;
			if (attractY[j] > flies[j].getY()) flyVelY[j] += FLY_ACCEL*timeElapsed;
			else flyVelY[j] += -FLY_ACCEL*timeElapsed;
			if (flyVelX[j] > MAX_VEL) flyVelX[j] = MAX_VEL;
			else if (flyVelX[j] < -MAX_VEL) flyVelX[j] = -MAX_VEL;
			if (flyVelY[j] > MAX_VEL) flyVelY[j] = MAX_VEL;
			else if (flyVelY[j] < -MAX_VEL) flyVelY[j] = -MAX_VEL;
			flies[j].setX(flies[j].getX() + flyVelX[j]*timeElapsed);
			flies[j].setY(flies[j].getY() + flyVelY[j]*timeElapsed);
			if ((Math.abs(attractX[j] - flies[j].getX()) < FLY_PROXIMITY) &&
					(Math.abs(attractY[j] - flies[j].getY()) < FLY_PROXIMITY)) {
				attractX[j] = rnd.nextFloat()*totalWidth;
				attractY[j] = rnd.nextFloat()*REFLECTION_Y_ORIGIN*1.5f;	
			}
		}
	}
	
	@Override
	public void Update(float timeElapsed) {
		masterPhase += MASTER_RATE*timeElapsed;
		if (masterPhase > 2*Math.PI)
			masterPhase -= 2*Math.PI;
		UpdateBands();
		UpdateFlies(timeElapsed);
	}	
}
