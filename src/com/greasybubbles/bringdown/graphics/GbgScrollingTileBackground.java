package com.greasybubbles.bringdown.graphics;

import org.andengine.entity.Entity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;

import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyTheme;
import com.greasybubbles.greasytools.GreasyScene;

public class GbgScrollingTileBackground extends GreasyTheme {

	private static final String[] reqdAssets = { "bgtexture" };

	TextureRegion bgTexture;

	private final float bgSpeedX = 55f; //totally out-of-thin-air numbers that make
	private final float bgSpeedY = 19f; //the animation look good...
	
	private final float UNIT_X; //the size of each tile width and height
	private final float UNIT_Y; 
	
	
	private float bgOffsetX = 0f;
	private float bgOffsetY = 0f;
	
	private Entity tileHandle;
	
	public GbgScrollingTileBackground(GreasyScene mScene, int screenWidth, int screenHeight) {
		super(mScene);
		bgTexture = GreasyAssets.getTexture("bgtexture");
		UNIT_X = bgTexture.getWidth() - 1;
		UNIT_Y = bgTexture.getHeight() - 1;
		//^^ the unit size is set to slightly smaller than that of the actual textures,
		//   causing them to slightly overlap. This is to avoid blankness showing through
		//	 possible gaps in the tiles due to rounding effects, adverse update timing, etc.
		tileHandle = new Entity(0,0);
		for (int i = 0; i < (screenWidth / UNIT_X + 2); i++) {
			for (int j = 0; j < (screenHeight / UNIT_Y + 2); j++) {
				tileHandle.attachChild(
						new Sprite(i*UNIT_X, j*UNIT_Y, bgTexture, mScene.vboManager));
			}
		}
		this.attachChild(tileHandle);
	}

	@Override
	public void Update(float timeElapsed) {

		//updates the position of the background tiles to create motion
		bgOffsetX += timeElapsed * bgSpeedX;
		bgOffsetY += timeElapsed * bgSpeedY;
		
		//wrap around if necessary to maintain the 'endless' background effect.
		if (bgOffsetX > UNIT_X) bgOffsetX %= UNIT_X; 
		if (bgOffsetY > UNIT_Y) bgOffsetY %= UNIT_Y;

		tileHandle.setPosition(-bgOffsetX, -bgOffsetY);
	}
}
