package com.greasybubbles.bringdown.graphics;

import java.util.Random;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.greasybubbles.greasytools.GreasyAssets;

public class DancingBlocks extends Entity {

	public static final boolean SAD = false;
	public static final boolean HAPPY = true;
	
	private final int SAD_NO_BLOCKS = 4;
	private final float SAD_PHASE_SHIFT = 0.4f;
	private final float SAD_PERIOD = 4f;
	private final int SAD_PERIODS_RATIO = 4;
	private final float SAD_INITIAL_OFFSET = 0f;

	private final int HAPPY_NO_BLOCKS = 4;
	private final float HAPPY_PHASE_SHIFT = 0.2f;
	private final float HAPPY_PERIOD = 2f;

	private float totalWidth, totalHeight;
	private float blockWidth, blockHeight;
	
	private boolean type;
	private Sprite[] sprites;
	private float masterPhase = 0;
	
	public DancingBlocks(boolean type, float width, float height, VertexBufferObjectManager vbom) {
		this.type = type; 
		Random rand = new Random();
		totalWidth = width;
		totalHeight = height;
		masterPhase = 0;
		if (type == SAD) {
			//create the sprites and inits them.
			sprites = new Sprite[SAD_NO_BLOCKS];
			for (int i = 0; i < SAD_NO_BLOCKS; i++) {
				sprites[i] = new Sprite(0f, 0f, GreasyAssets.getTexture("facesad"), vbom);
				//gives the sprites random colors (but adds 0.5 to make them quite bright)
				sprites[i].setColor(new Color((float)(.5+.5*rand.nextFloat()),
						(float)(.5+.5*rand.nextFloat()), (float)(.5+.5*rand.nextFloat())));
			}
		} else if (type == HAPPY) {
			//create the sprites and inits them.
			sprites = new Sprite[HAPPY_NO_BLOCKS];
			for (int i = 0; i < HAPPY_NO_BLOCKS; i++) {
				sprites[i] = new Sprite(0f, 0f, GreasyAssets.getTexture("facehappy"), vbom);
				//gives the sprites random colors (but adds 0.5 to make them quite bright)
				sprites[i].setColor(new Color((float)(.5+.5*rand.nextFloat()),
						(float)(.5+.5*rand.nextFloat()), (float)(.5+.5*rand.nextFloat())));
			}
		}
		blockWidth = sprites[0].getWidth();
		blockHeight = sprites[0].getHeight();					
		//attaches all the sprites
		for (Sprite spr : sprites) {
			this.attachChild(spr);
		}
		//draws the initial state.
		if (type == SAD)
			DrawSad();
		else if (type == HAPPY)
			DrawHappy();
		
		//registers an update handler so it will update itself!
		this.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
					Update(pSecondsElapsed); //calls the main loop.
			}			
			@Override
			public void reset() {}
		});
	}
	
	private void DrawSad() {
		//this will generate various sin/cos motions patterns
		for (int i = 0; i < SAD_NO_BLOCKS; i++) {
			float thisPhase = masterPhase + i*SAD_PHASE_SHIFT;
			
			float thisX = (totalWidth - blockWidth) *
					(.5f + .5f *(float)Math.cos(thisPhase * SAD_PERIODS_RATIO + SAD_INITIAL_OFFSET));
			float thisY = (totalHeight - blockHeight) * (.5f + .5f * (float)Math.cos(thisPhase));
			sprites[i].setPosition(thisX, thisY);
		}
	}

	private void DrawHappy() {
		//this will generate a motion of bouncing blocks (to simplify, i didn't use a parabola but
		//a sin with absolute value. It looks quite similar...)
		for (int i = 0; i < HAPPY_NO_BLOCKS; i++) {
			float thisPhase = masterPhase + i*HAPPY_PHASE_SHIFT;
			float thisX = (totalWidth - blockWidth) / (HAPPY_NO_BLOCKS - 1) * i; //space blocks equally
			float thisY = (totalHeight - blockHeight) * (1-(float)Math.abs(Math.sin(thisPhase)));
			sprites[i].setPosition(thisX, thisY);
		}
	}
	
	public void Update(float secondsElapsed) {
		if (type == HAPPY) {
			masterPhase += secondsElapsed * (2*(float)Math.PI/HAPPY_PERIOD);
			DrawHappy();
		} else if (type == SAD) {
			masterPhase += secondsElapsed * (2*(float)Math.PI/SAD_PERIOD);
			DrawSad();
		}
		
		if (masterPhase > 2*Math.PI)
			masterPhase -= 2*Math.PI;
	}
}
