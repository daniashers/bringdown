package com.greasybubbles.bringdown.graphics;

import org.andengine.engine.Engine;
import org.andengine.opengl.font.FontManager;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;

public class GraphicSystemParameters {
	public Engine engine = null;
	public VertexBufferObjectManager vboManager = null;
	public TextureManager textureManager = null;
	public FontManager fontManager = null;
	public Context context = null;
	
	public GraphicSystemParameters(Context c, Engine e) {
		this.context = c;
		this.engine = e;
		this.textureManager = e.getTextureManager();
		this.fontManager = e.getFontManager();
		this.vboManager = e.getVertexBufferObjectManager();
	}
}
