package com.greasybubbles.bringdown.graphics;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.greasybubbles.greasytools.GreasyAssets;

public class Water extends Entity {
	private static final float WATER_THICKNESS = 64;
	private static final float LEVEL_EXCURSION = 5;

	private static final float PHASE_SPEED = 1f;	
	
	private static final float RECT_HEIGHT = 4;

	private Rectangle waterBoundaryB;
	private Rectangle waterBoundaryW;
	
	private float waterLevel;
	
	private TextureRegion lavaTextureRegion;
	
	private float masterPhase = 0;
	
	public Water(float bottomLeftX, float bottomLeftY, float width, VertexBufferObjectManager vbom) {
		super(bottomLeftX, bottomLeftY);
		lavaTextureRegion = GreasyAssets.getTexture("lava").deepCopy();
		
		//sets the 'portion' of the texture to display to the bottom of the texture
		//(it will gradually move upwards later to generate the flowing effect.
		lavaTextureRegion.setTextureHeight(WATER_THICKNESS);
		
		//adds the rectangles to create sharp boundaries between lava and non-lava
		waterBoundaryB = new Rectangle(0f, -(WATER_THICKNESS+RECT_HEIGHT), width, RECT_HEIGHT, vbom);
		waterBoundaryB.setColor(new Color(.25f,.1f,0));
		waterBoundaryB.setAlpha(.5f);
		waterBoundaryW = new Rectangle(0f, -(WATER_THICKNESS), width, RECT_HEIGHT, vbom);
		waterBoundaryW.setColor(new Color(1,.75f,.5f));
		waterBoundaryW.setAlpha(.5f);
		this.attachChild(waterBoundaryB);
		this.attachChild(waterBoundaryW);
		
		masterPhase = 0;		
		
		Update(0);
	}
	
	public void Update(float deltaTime) {
		//cycles the angle for level varying effect
		masterPhase += PHASE_SPEED * deltaTime;
		if (masterPhase > 2*Math.PI) {
			//masterPhase -= 2*Math.PI;
		}
		//a random formula to produce a smoothly fluctuating value
		float lavaLevelAnomaly = LEVEL_EXCURSION*(float)(Math.sin(0.7*masterPhase) + 0.3*Math.sin(1.8*masterPhase));
		
		waterLevel = WATER_THICKNESS + lavaLevelAnomaly;
		
		
		//move the top Rectangles (the surface of the lava!)
		//to keep them at the top of the lava.
		waterBoundaryB.setY(-(waterLevel+RECT_HEIGHT));
		waterBoundaryW.setY(-waterLevel);	
	}
	
}
