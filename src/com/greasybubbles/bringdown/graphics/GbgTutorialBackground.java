package com.greasybubbles.bringdown.graphics;

import java.util.Random;

import org.andengine.entity.sprite.Sprite;
import org.andengine.util.color.Color;

import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyTheme;
import com.greasybubbles.greasytools.GreasyScene;

public class GbgTutorialBackground extends GreasyTheme {

	static private final float colorVariationSpeed = 0.3f;
	
	private Random rnd;
	
	private Color currentColor, previousColor, nextColor;
	private float colorMix;
	
	private Sprite background;
	
	public GbgTutorialBackground(GreasyScene mScene) {
		super(mScene);
		background = new Sprite(0f, 0f, GreasyAssets.getTexture("tutorialbg"), mScene.vboManager);

		rnd = new Random();
		
		previousColor = Color.WHITE;
		nextColor = generateRandomColor();
		currentColor = new Color(Color.WHITE);
		colorMix = 0;
		
		this.attachChild(background);		
	}
	
	@Override
	public void Update(float timeElapsed) {
		currentColor.mix(previousColor, 1-colorMix, nextColor, colorMix);
		background.setColor(currentColor);
		
		colorMix += colorVariationSpeed * timeElapsed;

		if (colorMix >= 1f) {
			previousColor = nextColor;
			nextColor = generateRandomColor();
			colorMix = 0;
		}
	}	
	
	private Color generateRandomColor() {
		return new Color(.5f+rnd.nextFloat()*.5f, .5f+rnd.nextFloat()*.5f, .5f+rnd.nextFloat()*.5f);
	}
}
