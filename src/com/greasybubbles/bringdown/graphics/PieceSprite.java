package com.greasybubbles.bringdown.graphics;

import static com.greasybubbles.bringdown.Constants.*;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.bringdown.model.GamePiece;
import com.greasybubbles.bringdown.model.GamePiece.Material;
import com.greasybubbles.bringdown.model.GamePiece.Shape;

//TODO handle the material as well as the shape!!!

public class PieceSprite extends Sprite {

	//
	// CONSTANTS
	//
	// Allows to 'find' the region within the texture atlas corresponding to the required shape/material
	private static final int V_STRIDE = 97; //the distance vertically between tilesets of 2 different materials
	private static final int U_STRIDE = 97;
	private static final int V_OFFSET = 1;
	private static final int U_OFFSET = 1;

	private static final int LARGE_DEBRIS_U = U_STRIDE * 9 + U_OFFSET;
	private static final int LARGE_DEBRIS_V = 0;
	private static final int SMALL_DEBRIS_U = U_STRIDE * 9 + U_OFFSET + PIXELS_PER_CELL_ON_FILE;
	private static final int SMALL_DEBRIS_V = 0;
	
	
	// Static variables
	//this will store a reference to the Texture sheet that holds the piece graphics!
	static private TextureRegion textureSheet = null;
		
	public PieceSprite(GamePiece pPiece, VertexBufferObjectManager vbom) {
		//version of the constructor that receives a whole GamePiece object
		//and extracts the necessary information from there.
		super(0, 0, TextureTrim(textureSheet, pPiece), vbom);

		int pixelX = TileMapper.XGridToScreen(pPiece.posX);
		int pixelY = TileMapper.YGridToScreen(pPiece.posX)-2*PIXELS_PER_CELL_ON_SCREEN;
		//(i subtract 2*pixelspercellonscreen as the sprite position has origin at top left
		//while our pieces are referenced to lower left. it is a bit of a hack, a better
		//clearer solution is required)
		this.setPosition(pixelX, pixelY);

		//now deal with the special case in which it is a Double height
		//block, in this case the sprite must be rotated counterclockwise by 90 degrees.
		//(it is stored in the file rotated for practical reasons)
		if (pPiece.shape == Shape.VertDouble) {
			this.setRotationCenter(PIXELS_PER_CELL_ON_FILE, PIXELS_PER_CELL_ON_FILE);
			//^^rotation centre is in middle of bottom 'square', think about it:
			//              ___
			//             |   |
			//  ______     |   | 
			// | .    | -> | . |   
			// |______|    |___|  
			this.setRotation(-90f);
		}		

	}

	public static void setTextureSheet(TextureRegion texSheet) {
		textureSheet = texSheet;
	}
	
	public void setGridPosition(float gridX, float gridY) {
		int pixelX = TileMapper.XGridToScreen(gridX);
		int pixelY = TileMapper.YGridToScreen(gridY) - (int)this.getHeight();
		//^^ I subtract the height to the position because the engine handles pieces by their top left
		//corner, while my game handles them by their BOTTOM left corner!!
		this.setPosition(pixelX, pixelY);
	}
	
	private static int getRelevantTextureU(GamePiece piece) {
		switch (piece.shape) {
		case Square:
			return (U_OFFSET + U_STRIDE * 0);
		case HorzDouble:
			return (U_OFFSET + U_STRIDE * 1);
		case VertDouble:
			return (U_OFFSET + U_STRIDE * 3);
		case DiagLeftBottom:
			return (U_OFFSET + U_STRIDE * 5);
		case DiagLeftTop:
			return (U_OFFSET + U_STRIDE * 6);
		case DiagRightBottom:
			return (U_OFFSET + U_STRIDE * 7);
		case DiagRightTop:
			return (U_OFFSET + U_STRIDE * 8);
		case HorzHalf:
			return (U_OFFSET + U_STRIDE * 9);
		case VertHalf:
			return (U_OFFSET + U_STRIDE * 10);			
		default:
			throw new RuntimeException("Invalid piece type");
		}
	}

	
	
	private static int getRelevantTextureV(Material mat) {
		switch (mat) {
		case Indestructible:
			return (V_OFFSET + V_STRIDE * 0);
		case Indivisible:
			return (V_OFFSET + V_STRIDE * 1);
		case Versatile:
			return (V_OFFSET + V_STRIDE * 2);
		case Glass:
			return (V_OFFSET + V_STRIDE * 3);
		case Fixed:
			return (V_OFFSET + V_STRIDE * 4);
		case SplittableOnly:
			return (V_OFFSET + V_STRIDE * 5);
		default:
			return 1; //TODO this is temp!!
			//TODO throw new RuntimeException("Invalid piece type");
		}
	}

	private static int getRelevantTextureV(GamePiece piece) {
		int v = getRelevantTextureV(piece.material);
		if (piece.shape == Shape.HorzHalf)
			v += PIXELS_PER_CELL_ON_FILE;
		return v;
	}
	
	//TextureTrim takes the big texture atlas and a piece, and returns a smaller region of the texture
	//containing only the relevant piece desired.
	private static TextureRegion TextureTrim(TextureRegion bigTexture, GamePiece piece) {
		if (textureSheet == null)
			throw new RuntimeException("Trying to use PieceSprite without first loading texture sheet!");
		TextureRegion trimmedRegion = bigTexture.deepCopy();
		if (piece.shape == Shape.VertDouble) { //vertdouble a special case: is stored rotated 90 degrees
			trimmedRegion.setTextureWidth(4*PIXELS_PER_CELL_ON_FILE);
			trimmedRegion.setTextureHeight(2*PIXELS_PER_CELL_ON_FILE);	
		} else if (piece.shape == Shape.HorzDouble) { //horzdouble is twice as wide!
			trimmedRegion.setTextureWidth(4*PIXELS_PER_CELL_ON_FILE);
			trimmedRegion.setTextureHeight(2*PIXELS_PER_CELL_ON_FILE);
		} else if (piece.shape == Shape.HorzHalf) { //horzhalf is half as tall!
			trimmedRegion.setTextureWidth(2*PIXELS_PER_CELL_ON_FILE);
			trimmedRegion.setTextureHeight(1*PIXELS_PER_CELL_ON_FILE);
		} else if (piece.shape == Shape.VertHalf) { //verthalf is half as wide!
				trimmedRegion.setTextureWidth(1*PIXELS_PER_CELL_ON_FILE);
				trimmedRegion.setTextureHeight(2*PIXELS_PER_CELL_ON_FILE);
		} else {
			trimmedRegion.setTextureWidth(2*PIXELS_PER_CELL_ON_FILE);
			trimmedRegion.setTextureHeight(2*PIXELS_PER_CELL_ON_FILE);
		}
		trimmedRegion.setTextureX(getRelevantTextureU(piece));
		trimmedRegion.setTextureY(getRelevantTextureV(piece));
		return trimmedRegion;
	}
	
	public static TextureRegion getLargeDebrisTextureForMaterial(Material mat) {
		if (textureSheet == null)
			throw new RuntimeException("Trying to use PieceSprite without first loading texture sheet!");
		TextureRegion debrisRegion = textureSheet.deepCopy();
		debrisRegion.setTextureX(LARGE_DEBRIS_U);
		debrisRegion.setTextureY(getRelevantTextureV(mat) + LARGE_DEBRIS_V);
		debrisRegion.setTextureWidth(1*PIXELS_PER_CELL_ON_FILE-1);
		debrisRegion.setTextureHeight(1*PIXELS_PER_CELL_ON_FILE-1);
		return debrisRegion;
	}
	public static TextureRegion getSmallDebrisTextureForMaterial(Material mat) {
		if (textureSheet == null)
			throw new RuntimeException("Trying to use PieceSprite without first loading texture sheet!");
		TextureRegion debrisRegion = textureSheet.deepCopy();
		debrisRegion.setTextureX(SMALL_DEBRIS_U);
		debrisRegion.setTextureY(getRelevantTextureV(mat) + SMALL_DEBRIS_V);
		debrisRegion.setTextureWidth(1*PIXELS_PER_CELL_ON_FILE-1);
		debrisRegion.setTextureHeight(1*PIXELS_PER_CELL_ON_FILE-1);
		return debrisRegion;
	}
}
