package com.greasybubbles.bringdown.graphics;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.greasybubbles.greasytools.GreasyAssets;

public class Lava extends Entity {
	private final float LAVA_THICKNESS = 64;
	private final float LEVEL_EXCURSION = 5;
	private final float COLOUR_EXCURSION = 1f;
	private final float LAVA_V_SPEED = 3; //the speed in pixels/sec that we will cycle our texture V value
											//to achieve a 'flowing' effect
	private final float PHASE_SPEED = 1f;	
	
	private final Color GRADIENT_COLOR_1 = new Color( 1 ,  1 , .5f);
	private final Color GRADIENT_COLOR_2 = new Color(.5f, .5f,  0 );
	
	private final float RECT_HEIGHT = 4;
	
	private final float TEXTURE_HEIGHT;
	private final float TEXTURE_WIDTH;

	private final float MAX_TEXTURE_V;
	
	private Sprite[] sprites1;
	private Rectangle lavaBoundaryB;
	private Rectangle lavaBoundaryW;
	
	private Color lavaColour;
	private float lavaLevel;
	
	private TextureRegion lavaTextureRegion;
	
	private float textureV = 0;
	private float masterPhase = 0;
	
	public Lava(float bottomLeftX, float bottomLeftY, float width, VertexBufferObjectManager vbom) {
		super(bottomLeftX, bottomLeftY);
		lavaTextureRegion = GreasyAssets.getTexture("lava").deepCopy();
		TEXTURE_HEIGHT = lavaTextureRegion.getHeight();
		TEXTURE_WIDTH = lavaTextureRegion.getWidth();
		assert (TEXTURE_HEIGHT > LAVA_THICKNESS);
		MAX_TEXTURE_V = TEXTURE_HEIGHT - LAVA_THICKNESS;

		//sets the 'portion' of the texture to display to the bottom of the texture
		//(it will gradually move upwards later to generate the flowing effect.
		lavaTextureRegion.setTextureHeight(LAVA_THICKNESS);
		lavaTextureRegion.setTextureY(MAX_TEXTURE_V);
		
		//calculates how many sprites are necessary to be tiled to cover requested width
		int spritesRequired = (int)Math.ceil(width / (lavaTextureRegion.getWidth()));
		sprites1 = new Sprite[spritesRequired];
		for (int i=0; i<spritesRequired; i++) {
			sprites1[i] = new Sprite(TEXTURE_WIDTH * i, -LAVA_THICKNESS, lavaTextureRegion, vbom);
			sprites1[i].setScaleCenterY(0);
			this.attachChild(sprites1[i]);
		}
		
		//adds the rectangles to create sharp boundaries between lava and non-lava
		lavaBoundaryB = new Rectangle(0f, -(LAVA_THICKNESS+RECT_HEIGHT), width, RECT_HEIGHT, vbom);
		lavaBoundaryB.setColor(new Color(.25f,.1f,0));
		lavaBoundaryB.setAlpha(.5f);
		lavaBoundaryW = new Rectangle(0f, -(LAVA_THICKNESS), width, RECT_HEIGHT, vbom);
		lavaBoundaryW.setColor(new Color(1,.75f,.5f));
		lavaBoundaryW.setAlpha(.5f);
		this.attachChild(lavaBoundaryB);
		this.attachChild(lavaBoundaryW);
		
		textureV = 0;
		masterPhase = 0;		

		lavaColour = new Color(1,1,1);
		
		Update(0);
	}
	
	public void Update(float deltaTime) {
		//cycles the V value for flowing effect
		textureV -= LAVA_V_SPEED * deltaTime;
		if (textureV < 0) {
			textureV += MAX_TEXTURE_V;
		}
		//cycles the angle for level varying effect
		masterPhase += PHASE_SPEED * deltaTime;
		if (masterPhase > 2*Math.PI) {
			//masterPhase -= 2*Math.PI;
		}
		//a random formula to produce a smoothly fluctuating value
		float lavaLevelAnomaly = LEVEL_EXCURSION*(float)(Math.sin(0.7*masterPhase) + 0.3*Math.sin(1.8*masterPhase));
		float lavaColourParameter =
				.5f+COLOUR_EXCURSION*.5f*(float)(0.79*Math.sin(masterPhase) + 0.21*Math.sin(3.8*masterPhase));
		lavaColour.mix(GRADIENT_COLOR_1, lavaColourParameter, GRADIENT_COLOR_2, 1-lavaColourParameter);
		
		lavaLevel = LAVA_THICKNESS + lavaLevelAnomaly;
		
		//applies the changes
		lavaTextureRegion.setTextureY(textureV);
		for (Sprite s : sprites1) {
			s.setColor(lavaColour);
			s.setY(-lavaLevel);
			s.setScaleY((lavaLevel+5) / LAVA_THICKNESS);
			s.setFlipped(true,true); s.setFlipped(false,false);
			//^^ A kluge to force U,V reupdating 
		}
		
		//move the top Rectangles (the surface of the lava!)
		//to keep them at the top of the lava.
		lavaBoundaryB.setY(-(lavaLevel+RECT_HEIGHT));
		lavaBoundaryW.setY(-lavaLevel);	
	}
	
	private void Redraw() {
		
	}
	
}
