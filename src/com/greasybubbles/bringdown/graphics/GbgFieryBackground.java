package com.greasybubbles.bringdown.graphics;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;

import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyTheme;
import com.greasybubbles.greasytools.GreasyScene;

public class GbgFieryBackground extends GreasyTheme {

	static final String[] reqdAssets = { "bgset_fiery" };
	static final int NUM_BANDS = 16;
	static final float TURBULENCE_RANGE = 0.33f; //how much the bands will move and stretch (as % of height)
	static final float TURBULENCE_RATE = 1.9f; //how fast the bands will move
	static final float BAND_PHASE_DELAY = 1.2f;

	private TextureRegion bgRegion;
	private TextureRegion[] bandTextures;
	private Sprite[] bandSprites;
		
	private float eachBandHeight;
	private float masterPhase = 0;
	private float bandPositions[];
	private float bandScales[];
	
	public GbgFieryBackground(GreasyScene mScene) {
		super(mScene);
		bandTextures = new TextureRegion[NUM_BANDS];
		bandSprites = new Sprite[NUM_BANDS];
		bandPositions = new float[NUM_BANDS];
		bandScales = new float[NUM_BANDS];
		bgRegion = GreasyAssets.getTexture("fierybg");
		float bgHeight = bgRegion.getHeight();
		eachBandHeight = bgHeight/NUM_BANDS;
		for (int i = 0; i < NUM_BANDS; i++) {
			bandTextures[i] = bgRegion.deepCopy(); //copies the full background texture into each band;
			bandTextures[i].setTextureHeight(bgHeight/NUM_BANDS); //resizes each band from the full
																	//backrground to just band sized
			bandTextures[i].setTextureY((bgHeight/NUM_BANDS) * i); //positions them to span the whole bg
			bandSprites[i] = new Sprite(0f, 0f, bandTextures[i], mScene.vboManager);
			bandSprites[i].setScaleCenter(0,0);
			this.attachChild(bandSprites[i]);
		}
		masterPhase = 0;
		UpdateBands();
	}
	
	private void UpdateBands() {
		//calculates the new positions of the bands after the 'heat distortion'
		bandPositions[0] = 0; //the first band is a special case, its top is always anchored to the screen top
		for (int i = 1; i < NUM_BANDS; i++) {
			float thisBandPhase = masterPhase + i*BAND_PHASE_DELAY;
			float thisBandBasePosition = eachBandHeight * i;
			float thisBandDistortedPosition = thisBandBasePosition + 
					(float)Math.sin(thisBandPhase) * TURBULENCE_RANGE * eachBandHeight;
			bandPositions[i] = thisBandDistortedPosition;
		}
		//calculates the new vertical scale for each band (the last one is a special case)
		for (int i = 0; i < NUM_BANDS - 1; i++) {
			float thisBandRequiredHeight = bandPositions[i+1] - bandPositions[i];
			thisBandRequiredHeight++; //adds one just to cover any 'gaps' between the bands
										//that may arise due to rounding/sync.
			float thisBandScaleRequired = thisBandRequiredHeight / eachBandHeight;
			bandScales[i] = thisBandScaleRequired;
		}
		bandScales[NUM_BANDS-1] = ((eachBandHeight*NUM_BANDS) - bandPositions[NUM_BANDS-1])/eachBandHeight;
		//now apply the bands positions and scales
		for (int i = 0; i < NUM_BANDS; i++) {
			bandSprites[i].setY(bandPositions[i]);
			bandSprites[i].setScaleY(bandScales[i]);
		}
	}

	@Override
	public void Update(float timeElapsed) {
		masterPhase += TURBULENCE_RATE*timeElapsed;
		if (masterPhase > 2*Math.PI)
			masterPhase -= 2*Math.PI;
		UpdateBands();
	}
}
