package com.greasybubbles.bringdown;

import java.util.ArrayList;

import com.greasybubbles.bringdown.model.GamePiece;
import com.greasybubbles.bringdown.model.GamePiece.Material;
import com.greasybubbles.bringdown.model.GamePiece.Shape;

import static com.greasybubbles.bringdown.Constants.*;

public class LevelData {
		
	static final LevelDefinition[] levels;
	static final LevelDefinition[] tutorials;


	static {
		levels = new LevelDefinition[NUM_LEVELS+1];
		tutorials = new LevelDefinition[20]; //TODO 20 is a random number
				
		levels[1] = new LevelDefinition("1-1", THEME_FIERY, 20, "Welcome! This is the first level",
				new GamePiece(Shape.VertDouble, Material.Indestructible, 4, 4),
				new GamePiece(Shape.DiagLeftTop, Material.Indivisible, 6, 6),
				new GamePiece(Shape.VertHalf, Material.Versatile, 6, 4),
				new GamePiece(Shape.Square, Material.Glass, 7, 4),
				new GamePiece(Shape.Square, Material.Fixed, 8, 6),
				new GamePiece(Shape.HorzHalf, Material.Versatile, 9, 4),
				new GamePiece(Shape.DiagRightTop, Material.Indestructible, 10, 5),
				new GamePiece(Shape.DiagLeftBottom, Material.Glass, 10, 5),
				new GamePiece(Shape.HorzDouble, Material.Versatile, 4, 2),
				new GamePiece(Shape.VertHalf, Material.Indestructible, 6, 0),
				new GamePiece(Shape.VertHalf, Material.Glass, 7, 0),
				new GamePiece(Shape.VertDouble, Material.Versatile, 8, 0),
				new GamePiece(Shape.DiagRightBottom, Material.Indestructible, 10, 2),
				new GamePiece(Shape.HorzHalf, Material.Indivisible, 10, 1),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 10, 0));
		levels [2] = new LevelDefinition("1-2", THEME_FIERY, 18, "",
				new GamePiece(Shape.DiagRightBottom, Material.Glass, 4, 6),
				new GamePiece(Shape.DiagLeftBottom, Material.Glass, 10, 6),
				new GamePiece(Shape.Square, Material.Indivisible, 4, 4),
				new GamePiece(Shape.Square, Material.Indivisible, 10, 4),
				new GamePiece(Shape.HorzHalf, Material.Indivisible, 7, 7),
				new GamePiece(Shape.DiagRightBottom, Material.Indestructible, 6, 5),
				new GamePiece(Shape.DiagLeftBottom, Material.Indestructible, 8, 5),
				new GamePiece(Shape.HorzHalf, Material.Indivisible, 6, 4),
				new GamePiece(Shape.HorzHalf, Material.Indivisible, 8, 4),
				new GamePiece(Shape.VertDouble, Material.Fixed, 4, 0),
				new GamePiece(Shape.VertDouble, Material.Versatile, 10, 0),
				new GamePiece(Shape.VertHalf, Material.Indivisible, 6, 2),
				new GamePiece(Shape.VertHalf, Material.Indivisible, 9, 2),
				new GamePiece(Shape.VertHalf, Material.Indivisible, 6, 0),
				new GamePiece(Shape.VertHalf, Material.Indivisible, 9, 0));			
		levels[3] = new LevelDefinition("1-3", THEME_FIERY, 24, "",
				new GamePiece(Shape.HorzDouble, Material.Fixed, 4, 2),
				new GamePiece(Shape.Square, Material.Glass, 8, 0),
				new GamePiece(Shape.Square, Material.Versatile, 10, 0),
				new GamePiece(Shape.Square, Material.Indestructible, 10, 2),
				new GamePiece(Shape.VertDouble, Material.Versatile, 8, 4),
				new GamePiece(Shape.Square, Material.Glass, 4, 4),
				new GamePiece(Shape.Square, Material.Indivisible, 6, 4),
				new GamePiece(Shape.Square, Material.Indestructible, 6, 6),
				new GamePiece(Shape.DiagLeftBottom, Material.Glass, 7, 8));
		levels[4] = new LevelDefinition("2-1", THEME_SWAMP, 3, "This level can be done in one move!",
				new GamePiece(Shape.DiagRightBottom, Material.Fixed, 4, 0),
				new GamePiece(Shape.DiagRightBottom, Material.Fixed, 6, 2),
				new GamePiece(Shape.DiagRightBottom, Material.Fixed, 8, 4),
				new GamePiece(Shape.DiagRightBottom, Material.Fixed, 10, 6),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 3, 5),
				new GamePiece(Shape.VertHalf, Material.Fixed, 3, 6),
				new GamePiece(Shape.VertHalf, Material.Indestructible, 4, 6));
		levels[5] = new LevelDefinition("2-2", THEME_SWAMP, 30, "",
				new GamePiece(Shape.VertDouble,Material.Indestructible,4,0),
				new GamePiece(Shape.DiagLeftBottom,Material.Glass,6,0),
				new GamePiece(Shape.HorzHalf,Material.Fixed,8,0),
				new GamePiece(Shape.Square,Material.SplittableOnly,10,0),
				new GamePiece(Shape.Square,Material.SplittableOnly,7,1),
				new GamePiece(Shape.DiagLeftTop,Material.Indivisible,9,1),
				new GamePiece(Shape.DiagLeftTop,Material.Indestructible,6,2),
				new GamePiece(Shape.VertHalf,Material.Glass,11,2),
				new GamePiece(Shape.HorzHalf,Material.Indivisible,9,3),
				new GamePiece(Shape.HorzDouble,Material.Versatile,4,4),
				new GamePiece(Shape.DiagLeftTop,Material.Fixed,8,4),
				new GamePiece(Shape.DiagRightBottom,Material.SplittableOnly,8,4),
				new GamePiece(Shape.VertDouble,Material.Indestructible,10,4),
				new GamePiece(Shape.Square,Material.Glass,5,6),
				new GamePiece(Shape.DiagRightBottom,Material.Indivisible,7,6));
		levels[6] = new LevelDefinition("?", THEME_SWAMP, 16, "",
				new GamePiece(Shape.Square,Material.Fixed,3,0),
				new GamePiece(Shape.Square,Material.Fixed,7,0),
				new GamePiece(Shape.Square,Material.Fixed,11,0),
				new GamePiece(Shape.DiagLeftTop,Material.Indestructible,4,2),
				new GamePiece(Shape.DiagRightTop,Material.Indestructible,6,2),
				new GamePiece(Shape.DiagLeftTop,Material.Indestructible,8,2),
				new GamePiece(Shape.DiagRightTop,Material.Indestructible,10,2),
				new GamePiece(Shape.Square,Material.Versatile,5,4),
				new GamePiece(Shape.Square,Material.Versatile,9,4),
				new GamePiece(Shape.VertDouble,Material.SplittableOnly,7,4),
				new GamePiece(Shape.HorzHalf,Material.Indivisible,5,6),
				new GamePiece(Shape.HorzHalf,Material.Indivisible,9,6));
		levels[7] = new LevelDefinition("3-1", THEME_TRAFFIC, 15, "",
				new GamePiece(Shape.Square, Material.SplittableOnly, 6, 0),
				new GamePiece(Shape.DiagRightBottom, Material.Indivisible, 5, 2),
				new GamePiece(Shape.DiagLeftBottom, Material.Indivisible, 7, 2),
				new GamePiece(Shape.Square, Material.Indestructible, 9, 0),
				new GamePiece(Shape.Square, Material.Indestructible, 9, 2),
				new GamePiece(Shape.DiagRightBottom, Material.Indivisible, 8, 4),
				new GamePiece(Shape.DiagLeftBottom, Material.Indivisible, 10, 4),
				new GamePiece(Shape.VertHalf, Material.Versatile, 5, 0),
				new GamePiece(Shape.DiagLeftTop, Material.Versatile, 5, 2),
				new GamePiece(Shape.Square, Material.Versatile, 5, 4),
				new GamePiece(Shape.DiagRightBottom, Material.Indivisible, 4, 6),
				new GamePiece(Shape.DiagLeftBottom, Material.Indivisible, 6, 6),
				new GamePiece(Shape.VertHalf, Material.Fixed, 4, 2));
		levels[8] = new LevelDefinition("3-2", THEME_TRAFFIC, 25, "",
				new GamePiece(Shape.VertHalf, Material.Versatile, 4, 0),
				new GamePiece(Shape.VertHalf, Material.Versatile, 4, 2),
				new GamePiece(Shape.VertHalf, Material.Versatile, 7, 0),
				new GamePiece(Shape.VertHalf, Material.Versatile, 7, 2),
				new GamePiece(Shape.HorzDouble, Material.Versatile, 8, 0),
				new GamePiece(Shape.VertHalf, Material.Versatile, 11, 2),
				new GamePiece(Shape.VertHalf, Material.Glass, 8, 2),
				new GamePiece(Shape.Square, Material.Glass, 9, 2),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 4, 4),
				new GamePiece(Shape.HorzHalf, Material.SplittableOnly, 6, 4),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 8, 4),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 10, 4),
				new GamePiece(Shape.HorzDouble, Material.Indivisible, 4, 5),
				new GamePiece(Shape.Square, Material.SplittableOnly, 8, 5),
				new GamePiece(Shape.DiagLeftBottom, Material.SplittableOnly, 10, 5),
				new GamePiece(Shape.DiagRightBottom, Material.SplittableOnly, 4, 7),
				new GamePiece(Shape.DiagLeftBottom, Material.SplittableOnly, 6, 7),
				new GamePiece(Shape.VertHalf, Material.Indestructible, 9, 7));
		levels[9] = new LevelDefinition("3-3", THEME_TRAFFIC, 10, "",
				new GamePiece(Shape.Square, Material.Fixed, 7, 0),
				new GamePiece(Shape.Square, Material.Versatile, 5, 0),
				new GamePiece(Shape.Square, Material.Versatile, 6, 2),
				new GamePiece(Shape.Square, Material.Versatile, 7, 4),
				new GamePiece(Shape.DiagLeftBottom, Material.Versatile, 9, 0));
		levels[10] = new LevelDefinition("4-1", THEME_TUTORIAL, 10, "",
				new GamePiece(Shape.HorzHalf, Material.Indivisible, 5, 0),
				new GamePiece(Shape.HorzHalf, Material.Indivisible, 9, 0),
				new GamePiece(Shape.VertHalf, Material.Indestructible, 6, 1),
				new GamePiece(Shape.VertHalf, Material.Indestructible, 9, 1),
				new GamePiece(Shape.DiagRightBottom, Material.Versatile, 6, 4),
				new GamePiece(Shape.DiagLeftBottom, Material.SplittableOnly, 8, 4),
				new GamePiece(Shape.DiagRightTop, Material.SplittableOnly, 6, 2),
				new GamePiece(Shape.DiagLeftTop, Material.Versatile, 8, 2));
		levels[11] = new LevelDefinition("4-2", THEME_TUTORIAL, 8, "",
				new GamePiece(Shape.VertHalf, Material.Fixed, 7, 0),
				new GamePiece(Shape.DiagLeftBottom, Material.Fixed, 4, 2),
				new GamePiece(Shape.DiagRightBottom, Material.Fixed, 10, 1),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 6, 5),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 8, 5),
				new GamePiece(Shape.VertHalf, Material.Fixed, 3, 5),
				new GamePiece(Shape.VertHalf, Material.Fixed, 12, 5),
				new GamePiece(Shape.HorzDouble, Material.SplittableOnly, 6, 6));
		levels[12] = new LevelDefinition("4-3", THEME_TUTORIAL, 20, "",
				new GamePiece(Shape.HorzHalf, Material.Indivisible, 5, 0),
				new GamePiece(Shape.HorzHalf, Material.Indivisible, 7, 0),
				new GamePiece(Shape.HorzHalf, Material.Indivisible, 9, 0),
				new GamePiece(Shape.VertDouble, Material.Indestructible, 4, 1),
				new GamePiece(Shape.VertDouble, Material.Indestructible, 10, 1),
				new GamePiece(Shape.Square, Material.SplittableOnly, 7, 1));


				
				

		
		tutorials[1] = new LevelDefinition("TUTORIAL 1.1", THEME_TUTORIAL, 1,
				"Welcome to BringDown.\n" +
				"In this tutorial you will\n" +
				"learn the basics.");
		tutorials[2] = new LevelDefinition("TUTORIAL 1.2", THEME_TUTORIAL, 1,
				"The game is won by clearing all pieces.\n" +
				"Your main tool is the ability to blast\n" +
				"two or more pieces apart from each other\n" +
				"by swiping between them. Give it a go!\n",
						new GamePiece(Shape.VertHalf, Material.Indestructible, 7, 0),
						new GamePiece(Shape.VertHalf, Material.SplittableOnly, 8, 0));
		tutorials[3] = new LevelDefinition("TUTORIAL 1.3", THEME_TUTORIAL, 1,
				"You can swipe diagonally too!",
				new GamePiece(Shape.DiagLeftBottom, Material.SplittableOnly, 6, 0),
				new GamePiece(Shape.DiagRightTop, Material.Indestructible, 6, 0));
		tutorials[4] = new LevelDefinition("TUTORIAL 1.4", THEME_TUTORIAL, 2,
				"Some levels have structural blocks\n" + 
				"which you cannot move or clear,\n" +
				"like this one. Just concentrate on\n" +
				"getting rid of the active pieces!\n",
				new GamePiece(Shape.Square, Material.Fixed, 7, 0),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 6, 2),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 8, 2),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 10, 2),
				new GamePiece(Shape.DiagRightBottom, Material.Fixed, 8, 3),
				new GamePiece(Shape.Square, Material.Indestructible, 5, 0),
				new GamePiece(Shape.DiagLeftBottom, Material.SplittableOnly, 10, 3));
		tutorials[5] = new LevelDefinition("TUTORIAL 1.5", THEME_TUTORIAL, 3,
				"The heavier the block (or blocks)\n" +
				"you try to push, the shorter distance\n" +
				"they will move. Above a certain weight,\n" +
				"pieces won't move at all.",
				new GamePiece(Shape.VertHalf, Material.Fixed, 10, 0),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 10, 2),
				new GamePiece(Shape.VertHalf, Material.Fixed, 10, 3),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 10, 5),
				new GamePiece(Shape.VertHalf, Material.Fixed, 10, 6),
				new GamePiece(Shape.HorzDouble, Material.Indestructible, 11, 0),
				new GamePiece(Shape.Square, Material.Indestructible, 11, 3),
				new GamePiece(Shape.VertHalf, Material.Indestructible, 11, 6));
		tutorials[6] = new LevelDefinition("TUTORIAL 1.6", THEME_TUTORIAL, 2,
				"Certain pieces (not all!) can be split.\n" +
				"Just swipe along the desired split line.\n" +
				"Try to clear this level in two moves!",
				new GamePiece(Shape.Square, Material.SplittableOnly, 7, 0));
		tutorials[7] = new LevelDefinition("TUTORIAL 1.7", THEME_TUTORIAL, 2,
				"You can split diagonally too.\n" +
				"Can you solve the next puzzle?",
				new GamePiece(Shape.HorzHalf, Material.Fixed, 4, 1),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 6, 1),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 8, 1),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 10, 1),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 5, 0),
				new GamePiece(Shape.HorzHalf, Material.Fixed, 9, 0),
				new GamePiece(Shape.Square, Material.SplittableOnly, 8, 2));
		tutorials[8] = new LevelDefinition("TUTORIAL 1.8", THEME_TUTORIAL, 2,
				"Certain pieces can be destroyed\n"+
				"outright by double tapping them.\n" +
				"Try it now!",
				new GamePiece(Shape.VertDouble, Material.Versatile, 5, 0),
				new GamePiece(Shape.Square, Material.Indivisible, 9, 0));
		tutorials[9] = new LevelDefinition("TUTORIAL 1.9", THEME_TUTORIAL, 2,
				"Destroying pieces should only be\n" +
				"a last resort, as you will be\n" +
				"penalised! But sometimes you have\n" +
				"no choice, like here!",
				new GamePiece(Shape.Square, Material.Indestructible, 4, 0),
				new GamePiece(Shape.Square, Material.Indivisible, 6, 0));
		tutorials[10] = new LevelDefinition("Tutorial 1.10", THEME_TUTORIAL, 1,
				"Take advantage of slopes!",
				new GamePiece(Shape.DiagRightBottom, Material.Fixed, 4, 0),
				new GamePiece(Shape.Square, Material.Fixed, 6, 0),
				new GamePiece(Shape.DiagRightBottom, Material.Fixed, 6, 2),
				new GamePiece(Shape.VertDouble, Material.Fixed, 8, 0),
				new GamePiece(Shape.DiagRightBottom, Material.Fixed, 8, 4),
				new GamePiece(Shape.Square, Material.Fixed, 10, 0),
				new GamePiece(Shape.VertDouble, Material.Fixed, 10, 2),
				new GamePiece(Shape.VertHalf, Material.Fixed, 12, 5),
				new GamePiece(Shape.Square, Material.Indestructible, 10, 6));
		tutorials[11] = new LevelDefinition("Tutorial 1.11", THEME_TUTORIAL, 2,
				"Be smart and create your own\n" +
				"slopes! Can you clear this level\n" +
				"in only TWO moves?",
				new GamePiece(Shape.Square, Material.Versatile, 4, 0),
				new GamePiece(Shape.HorzDouble, Material.SplittableOnly, 2, 2),
				new GamePiece(Shape.HorzHalf, Material.Indestructible, 4, 4),
				new GamePiece(Shape.DiagLeftBottom, Material.Indivisible, 4, 5));
		tutorials[12] = new LevelDefinition("Tutorial 1.12", THEME_TUTORIAL, 1,
				"This is the end of the\n" +
				"tutorial. Enjoy the game!");
				
	}
	
	public static boolean exists(int levelNumber) {
		if (levelNumber > NUM_LEVELS) {
			return false;
		} else if (levelNumber >= 0) {
			return (levels[levelNumber] != null);
		} else { // (levelNumber < 0)
			return (tutorials[-levelNumber] != null);
		}		
	}
	
	public static void FillLevel(ArrayList<GamePiece> level, int levelNumber) {
		if (levelNumber >= 0) {
			for (GamePiece p : levels[levelNumber].layout) {
				level.add(new GamePiece(p));
			}
		} else { // (levelNumber < 0)
			for (GamePiece p : tutorials[-levelNumber].layout) {
				level.add(new GamePiece(p));
			}
		}
	}
		
	public static boolean hasTipScreen(int levelNumber) {
		//returns true only if there is actually a hint string associated with the level.
		return (getHint(levelNumber).length() > 0);
	}

	public static int getTheme(int levelNumber) {
		if (levelNumber >= 0) {
			return levels[levelNumber].theme;
		} else { // (levelNumber < 0)
			return tutorials[-levelNumber].theme;
		}		
	}
	
	public static int getMoveLimit(int levelNumber) {
		if (levelNumber >= 0) {
			return levels[levelNumber].moves;
		} else { // (levelNumber < 0)
			return tutorials[-levelNumber].moves;
		}		
	}
	
	public static String getHint(int levelNumber) {
		if (levelNumber >= 0) {
			return levels[levelNumber].hint;
		} else { // (levelNumber < 0)
			return tutorials[-levelNumber].hint;
		}
	}
	
	private static class LevelDefinition {
		String name;
		int theme;
		int moves;
		String hint;
		GamePiece[] layout;
		public LevelDefinition(String name, int theme, int moves, String hint, GamePiece... pieces) {
			this.name = name;
			this.theme = theme;
			this.moves = moves;
			this.hint = hint;
			layout = new GamePiece[pieces.length];
			for (int i = 0; i < pieces.length; i++) {
				layout[i] = pieces[i];
			}
		}
	}

}
