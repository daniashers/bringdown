package com.greasybubbles.bringdown;

import java.util.ArrayList;

//This class contains lots of hard-coded info about our assets. Is it a good design strategy?
//I don't know, but for now it will do.

public class AssetList {

	public static final int NOT_PACKED = 0;
	public static final int POST_PACKED = 1;
	public static final int PRE_PACKED_MAP = 2;
	public static final int PRE_PACKED = 3;
	public static final int TILED = 4;
		
	public static final AssetInfo[] ASSET_INFO = {
			new AssetInfo("grass_",	800, 100, 	POST_PACKED, "background_elements", 0, 480),

			new AssetInfo("cancel",			64,	256,	NOT_PACKED),
			
			new AssetInfo("spark", 			64, 64, 	POST_PACKED, "specialeffects", 0, 0),
			new AssetInfo("dust", 			64, 64, 	POST_PACKED, "specialeffects", 64, 0),
			new AssetInfo("fireball", 		64, 64, 	POST_PACKED, "specialeffects", 128, 0),
			new AssetInfo("star", 			32, 32,		POST_PACKED, "specialeffects", 0, 64),
			new AssetInfo("droplet", 		64, 64,		POST_PACKED, "specialeffects", 64, 64),
			
			new AssetInfo("title", 			512, 256, 	POST_PACKED, "titleelements", 0, 0),
			new AssetInfo("bgtexture", 		256, 256, 	POST_PACKED, "titleelements", 0, 256),
			new AssetInfo("button", 		256, 64, 	POST_PACKED, "titleelements", 256, 256),
			new AssetInfo("buttonsmall", 	64, 64, 	POST_PACKED, "titleelements", 256, 320),

			new AssetInfo("check",			32, 32,		NOT_PACKED),
			
			new AssetInfo("backbutton",		64, 64,		NOT_PACKED),
			new AssetInfo("explstrips", 	192, 192, 	TILED, 8, 1),
			new AssetInfo("expldestr",		48, 48,		NOT_PACKED),
			
			new AssetInfo("popupbanners",	512, 128,	PRE_PACKED_MAP),
			new AssetInfo("bannerblue",		512, 64,	PRE_PACKED, "popupbanners", 0, 0),
			new AssetInfo("bannerred",		512, 64,	PRE_PACKED, "popupbanners", 0, 64),
			
			new AssetInfo("bgset_fiery",	1024, 1024, PRE_PACKED_MAP),
			new AssetInfo("fierybg",		800, 480, 	PRE_PACKED, "bgset_fiery", 0, 0),
			new AssetInfo("fierygrass",		800, 100, 	PRE_PACKED, "bgset_fiery", 0, 480),
			new AssetInfo("lava",			128, 128, 	PRE_PACKED, "bgset_fiery", 0, 580),

			new AssetInfo("bgset_swamp",	1024, 1024, PRE_PACKED_MAP),
			new AssetInfo("swampbg",		800, 480, 	PRE_PACKED, "bgset_swamp", 0, 0),
			new AssetInfo("swampgrass",		800, 100, 	PRE_PACKED, "bgset_swamp", 0, 480),
			
			new AssetInfo("bgset_tut",		1024, 1024, PRE_PACKED_MAP),
			new AssetInfo("tutorialbg",		800, 480, 	PRE_PACKED, "bgset_tut", 0, 0),
			new AssetInfo("tutorialgrass",	800, 100, 	PRE_PACKED, "bgset_tut", 0, 480),

			new AssetInfo("bgset_traffic",	1024, 512, PRE_PACKED_MAP),
			new AssetInfo("trafficbg",		800, 256, 	PRE_PACKED, "bgset_traffic", 0, 0),
			new AssetInfo("trafficgrass",	800, 100, 	PRE_PACKED, "bgset_traffic", 0, 256),
			new AssetInfo("carlights",		64, 64,		PRE_PACKED, "bgset_traffic", 0, 356),
			
			new AssetInfo("faces",			256, 128,	PRE_PACKED_MAP),
			new AssetInfo("facehappy",		128, 128,	PRE_PACKED, "faces", 0, 0),
			new AssetInfo("facesad",		128, 128,	PRE_PACKED, "faces", 128, 0),
						
			new AssetInfo("newsheet",		1024, 1024,	NOT_PACKED),
			
			new AssetInfo("grass_titian",	800, 100,	NOT_PACKED),

	};
	
	private static AssetInfo getAsset(String id) {
		for (AssetInfo i : ASSET_INFO) {
			if (i.id.equals(id)) return i;
		}
		//if the requested asset was not found, throw an error
		throw new RuntimeException("Asset not found: <" + id + ">");	
	}
	
	//
	// Public getters/setters
	//
	
	public static int getWidthOf(String id) {
		return getAsset(id).width;
	}
	
	public static int getHeightOf(String id) {
		return getAsset(id).height;
	}

	public static int getOffsetXOf(String id) {
		return getAsset(id).packedOffsetX;
	}
	
	public static int getOffsetYOf(String id) {
		return getAsset(id).packedOffsetY;
	}
	
	public static int getTileColumnsOf(String id) {
		return getAsset(id).tileColumns;
	}
	
	public static int getTileRowsOf(String id) {
		return getAsset(id).tileRows;
	}
	
	public static String getParentAtlasOf(String id) {
		return getAsset(id).textureAtlasId;
	}

	public static String[] getAllRegionsInAtlas(String atlasId) {
		ArrayList<String> tempList = new ArrayList<String>();
		for (AssetInfo i : ASSET_INFO) {
			if (i.textureAtlasId.equals(atlasId)) {
				tempList.add(i.id);
			}
		}
		return((String[])(tempList.toArray(new String[tempList.size()])));
	}
	
	public static boolean isTiled(String id) {
		if (getAsset(id).packedType == TILED) return true; else return false;
	}

	public static boolean isPrePackedMap(String id) {
		if (getAsset(id).packedType == PRE_PACKED_MAP) return true; else return false;
	}

	public static boolean isPrePacked(String id) {
		if (getAsset(id).packedType == PRE_PACKED) return true; else return false;
	}
	
	public static boolean isPostPacked(String id) {
		if (getAsset(id).packedType == POST_PACKED) return true; else return false;
	}
	
	public static boolean isPacked(String id) {
		if ((getAsset(id).packedType == POST_PACKED) ||
			(getAsset(id).packedType == PRE_PACKED)) return true; else return false;
	}

	public static boolean isNotPacked(String id) {
		if ((getAsset(id).packedType == NOT_PACKED)) return true; else return false;
	}

	
	
	//
	// INTERNAL CLASSES
	//
	
	static class AssetInfo {
		final String id;
		final int width;
		final int height;
		final int packedType;
		final String textureAtlasId;
		final int packedOffsetX;
		final int packedOffsetY;
		final int tileColumns;
		final int tileRows;
		
		public AssetInfo(String id, int width, int height, int packed, String textureAtlasId, int packedOffsetX, int packedOffsetY) {
			assert ((packed == PRE_PACKED)||(packed == POST_PACKED));
			//^this constructor should only be used for post-packed textures (i.e. those that will be
			//combined at run-time into a large texture atlas. as such the above argument is redundant
			//but left for readability
			this.id = id;
			this.width = width;
			this.height = height;
			this.textureAtlasId = textureAtlasId;
			this.packedOffsetX = packedOffsetX;
			this.packedOffsetY = packedOffsetY;
			this.tileColumns = 1;
			this.tileRows = 1;
			this.packedType = packed;
		}
		
		public AssetInfo(int width, int height, int packed, String textureAtlasId) {
			assert (packed == PRE_PACKED_MAP);
			//^this constructor should only be used for pre-packed texture maps
			this.id = textureAtlasId;
			this.width = NearestPowerOf2(width);
			this.height = NearestPowerOf2(height);
			this.textureAtlasId = textureAtlasId;
			this.packedOffsetX = 0;
			this.packedOffsetY = 0;
			this.tileColumns = 1;
			this.tileRows = 1;
			this.packedType = packed;
		}
		
		
		public AssetInfo(String id, int width, int height, int packed) {
			assert (packed == NOT_PACKED);
			//^this constructor should only be used for unpacked textures (i.e. those that contain only
			//one texture in its own texture atlas. as such the above argument is redundant but left
			//for readability
			this.id = id;
			this.width = NearestPowerOf2(width);
			this.height = NearestPowerOf2(height);
			this.tileColumns = 1;
			this.tileRows = 1;
			this.textureAtlasId = "";
			this.packedOffsetX = 0;
			this.packedOffsetY = 0;
			this.packedType = packed;
		}
		
		public AssetInfo(String id, int width, int height, int packed, int tileColumns, int tileRows) {
			assert (packed == TILED);
			//^this constructor should only be used for regularly tiled textures. As such the above
			//argument is redundant but left for readability
			this.id = id;
			this.width = NearestPowerOf2(width);
			this.height = NearestPowerOf2(height);
			this.tileColumns = tileColumns;
			this.tileRows = tileRows;
			this.textureAtlasId = "";
			this.packedOffsetX = 0;
			this.packedOffsetY = 0;
			this.packedType = packed;
		}

		//TODO implement pre-packed textures
		
		private int NearestPowerOf2(int size) {
			//quick and nasty but more readable than obscure
			//binary logic, shifts and shit.
			     if (size <=   32) return   32;
			else if (size <=   64) return   64;
			else if (size <=  128) return  128;
			else if (size <=  256) return  256;
			else if (size <=  512) return  512;
			else if (size <= 1024) return 1024;
			else throw new RuntimeException("Invalid asset size (max 1024!)");
		}
	}
}

// EXPLANATION:
//
// NOT PACKED means that the texture region resides alone in its own texture atlas.
// PRE-PACKED means that the texture atlas loaded (and thus the image file) contains more than
//            a texture, and they will be extracted at load time and assigned to different regions.
// POST-PACKED means that the images files contain one texture region each, but they will be combined
//             at runtime into a big texture atlas.
// TILED means that the image file and thus the texture atlas contains tiles that can be accessed
//       via the standard TiledTextureRegion methods. (tiles are of the same size etc.)
