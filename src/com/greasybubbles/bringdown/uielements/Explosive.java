package com.greasybubbles.bringdown.uielements;

import static com.greasybubbles.bringdown.Constants.*;

import org.andengine.entity.Entity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;

import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.bringdown.Constants.LineType;
import com.greasybubbles.bringdown.model.GamePiece;
import com.greasybubbles.bringdown.sfx.FxFuseSparks;
import com.greasybubbles.bringdown.sfx.SpecialEffectsManager;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

public class Explosive extends Entity {
	
	//constants
	//
	private final int STRIP_SPRITE_WIDTH = 24;
	private final int STRIP_SPRITE_HEIGHT = 192;
	
	Sprite explSprite;
	
	// members
	//
	
	private final ExplMode mode;
	public final int x1, y1, x2, y2;
	public final GamePiece affectedPiece;
	public final LineType type;

	//this constructor is for when the explosive is placed on top of the whole piece
	//with the purpose of destroying it.
	public Explosive(GamePiece piece, GreasyScene scene) {
	
		this.mode = ExplMode.Destroy;
		this.type = null;
		affectedPiece = piece;
		x1 = y1 = x2 = y2 = 0;
		
		float centreX;
		float centreY;
		
		switch (piece.shape) {
		case HorzDouble:
			centreX = piece.posX+2;
			centreY = piece.posY+1;
			break;
		case VertDouble:
			centreX = piece.posX+1;
			centreY = piece.posY+2;
			break;
		case Square:
			centreX = piece.posX+1;
			centreY = piece.posY+1;
			break;
		case HorzHalf:
			centreX = piece.posX+1;
			centreY = piece.posY+0.5f;
			break;
		case VertHalf:
			centreX = piece.posX+0.5f;
			centreY = piece.posY+1;
			break;
		case DiagLeftBottom:
			centreX = piece.posX+0.5f;
			centreY = piece.posY+0.5f;
			break;
		case DiagLeftTop:
			centreX = piece.posX+0.5f;
			centreY = piece.posY+1.5f;
			break;
		case DiagRightBottom:
			centreX = piece.posX+1.5f;
			centreY = piece.posY+0.5f;
			break;
		case DiagRightTop:
			centreX = piece.posX+1.5f;
			centreY = piece.posY+1.5f;
			break;		
		default:
			throw new RuntimeException("Invalid block expl!!!");
		}
		
		explSprite = new Sprite(0, 0, GreasyAssets.getTexture("expldestr"), scene.vboManager);

		int pixelX = (int) (TileMapper.XGridToScreen(centreX) - explSprite.getWidth()/2);
		int pixelY = (int) (TileMapper.YGridToScreen(centreY) - explSprite.getHeight()/2);
		explSprite.setPosition(pixelX, pixelY);

		explSprite.setScaleCenter(explSprite.getWidth()/2, explSprite.getHeight()/2);
		
		this.attachChild(explSprite);
		
		explSprite.registerEntityModifier(new AlphaModifier(0.25f, 0f, 1f));		
		explSprite.registerEntityModifier(new ScaleModifier(FuseCancelButton.FUSE_TIME, 1f, 2f));
		//^ TODO it is bad insulation to use another class's constant here... see if you can merge
		//the two classes, "fuse cancel button" and "explosive" into one.
		
		SpecialEffectsManager.AddEffect(new FxFuseSparks(centreX, centreY, scene));
	}
	
	//this constructor is for when the explosive is placed across a piece with the purpose
	//of splitting it in two.
	public Explosive(GamePiece piece, LineType type, GreasyScene scene) {

		this.mode = ExplMode.Split;
		this.type = type;
		affectedPiece = piece;
		x1 = y1 = x2 = y2 = 0;
		
		float centreX, centreY;
		
		switch (piece.shape) {
		case HorzDouble:
			centreX = piece.posX + 2;
			centreY = piece.posY + 1;
			break;
		case VertDouble:
			centreX = piece.posX + 1;
			centreY = piece.posY + 2;
			break;
		case Square:
			centreX = piece.posX + 1;
			centreY = piece.posY + 1;
			break;
		default:
			throw new RuntimeException("Invalid expl split specified!");
		}
		
		explSprite = new TiledSprite(0, 0,
				GreasyAssets.getTextureTiled("explstrips"), scene.vboManager);
		//select the right tile for our strip
		((TiledSprite)explSprite).setCurrentTileIndex(TileMapper.getTileForStripType(mode,type));

		//HOW TO CORRECTLY POSITION THE EXPLOSIVE SPRITE:
		
		//place the sprite, still unscaled and unrotated, with its centre in the middle 
		//of the drawn explosive line.
		int pixelX = TileMapper.XGridToScreen(centreX) - STRIP_SPRITE_WIDTH/2;
		int pixelY = TileMapper.YGridToScreen(centreY) - STRIP_SPRITE_HEIGHT/2;
		explSprite.setPosition(pixelX, pixelY);
		
		//now set scale centre and rotation centre in the centre point of the sprite
		explSprite.setRotationCenter(explSprite.getWidth()/2, explSprite.getHeight()/2);
		explSprite.setScaleCenter(explSprite.getWidth()/2, explSprite.getHeight()/2);
		
		//now apply scale and rotation
		
		switch (type) {
		case DiagSlash:
			explSprite.setRotation(+45);
			break;
		case DiagBackslash:
			explSprite.setRotation(-45);
			break;
		case HorzSingle:
			explSprite.setRotation(+90);
			break;
		default:
			explSprite.setRotation(0);
		}

		//finally attach the sprite to the object
		this.attachChild(explSprite);
		
		explSprite.registerEntityModifier(new AlphaModifier(1.0f, 1.0f, 0.0f));		
		
		SpecialEffectsManager.AddEffect(new FxFuseSparks(centreX, centreY, scene));

	}
	
	
	//this constructor is for when the explosive is placed between two pieces
	//with the purpose of blasting them apart.
	public Explosive(int originX, int originY, LineType type, GreasyScene scene) {
		
		this.mode = ExplMode.Apart;		
		this.type = type;
		this.affectedPiece = null;
		
		int lengthX, lengthY;
		switch (type) {
		case VertDouble:
			lengthX = 0;
			lengthY = 4;
			break;
		case VertSingle:
			lengthX = 0;
			lengthY = 2;
			break;
		case VertHalf:
			lengthX = 0;
			lengthY = 1;
			break;
		case DiagSlash:
			lengthX = 2;
			lengthY = 2;
			break;
		case DiagBackslash:
			lengthX = -2;
			lengthY = 2;
			break;
		default:
			throw new RuntimeException("Wrong expl type specified.");
		}

		this.x1 = originX;
		this.y1 = originY;
		this.x2 = originX + lengthX;
		this.y2 = originY + lengthY;

		float centreX = (float)originX + (float)lengthX / 2;
		float centreY = (float)originY + (float)lengthY / 2;
		
		explSprite = new TiledSprite(0, 0,
				GreasyAssets.getTextureTiled("explstrips"), scene.vboManager);
		//select the right tile for our strip
		((TiledSprite)explSprite).setCurrentTileIndex(TileMapper.getTileForStripType(mode, type));

		//HOW TO CORRECTLY POSITION THE EXPLOSIVE SPRITE:
		
		//place the sprite, still unscaled and unrotated, with its centre in the middle 
		//of the drawn explosive line.
		int pixelX = TileMapper.XGridToScreen(centreX) - STRIP_SPRITE_WIDTH/2;
		int pixelY = TileMapper.YGridToScreen(centreY) - STRIP_SPRITE_HEIGHT/2;
		explSprite.setPosition(pixelX, pixelY);
		
		//now set scale centre and rotation centre in the centre point of the sprite
		explSprite.setRotationCenter(explSprite.getWidth()/2, explSprite.getHeight()/2);
		explSprite.setScaleCenter(explSprite.getWidth()/2, explSprite.getHeight()/2);
		
		//now apply scale and rotation
		
		switch (type) {
		case DiagSlash:
			explSprite.setRotation(+45);
			break;
		case DiagBackslash:
			explSprite.setRotation(-45);
			break;
		case HorzSingle:
			explSprite.setRotation(+90);
			break;
		default:
			explSprite.setRotation(0);
		}

		//finally attach the sprite to the object
		this.attachChild(explSprite);
		
		explSprite.registerEntityModifier(new AlphaModifier(0.25f, 0.0f, 1.0f));

		SpecialEffectsManager.AddEffect(new FxFuseSparks(centreX, centreY, scene));
		//TODO it is not well that this class attaches stuff directly to the scene...
		//it is bad insulation... find another way.
	}
		
	public ExplMode getExplMode() {
		return mode;
	}
}
