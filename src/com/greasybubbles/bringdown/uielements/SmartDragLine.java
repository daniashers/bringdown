package com.greasybubbles.bringdown.uielements;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

public class SmartDragLine extends Entity {

	Rectangle mainLine;
	Rectangle hintSegment;
	VertexBufferObjectManager vbom;
	
	private static final Color LINE_COLOR = new Color(0.35f, 0.15f, 1.0f);
	private static final Color HINT_COLOR = new Color(0.35f, 1.0f, 0.15f);	
	
	private static final float LINE_WIDTH = 7.0f;
	private static final float HINT_WIDTH = 10.0f;

	private float lastx1 = 0, lasty1 = 0;
	
	public SmartDragLine(float x1, float y1, float x2, float y2, VertexBufferObjectManager vbom) {
		super();
		this.vbom = vbom;
		
		//sets main line properties
		mainLine = new Rectangle(0, 0, 0, 0, vbom);
		mainLine.setHeight(LINE_WIDTH);
		mainLine.setColor(LINE_COLOR);
		mainLine.setRotationCenter(LINE_WIDTH/2, LINE_WIDTH/2);
		mainLine.setVisible(false); //initially invisible until ShowLine method called
		this.attachChild(mainLine);
		
		//sets hint line properties
		hintSegment = new Rectangle(0, 0, 0, 0, vbom);
		hintSegment.setHeight(HINT_WIDTH);
		hintSegment.setColor(HINT_COLOR);
		hintSegment.setRotationCenter(HINT_WIDTH/2, HINT_WIDTH/2);
		hintSegment.setVisible(false);
		this.attachChild(hintSegment);		

		this.Show(x1, y1, x2, y2);
	}

	public void Show(float x1, float y1, float x2, float y2) {
		this.setVisible(true);
		//set dimensions and position

		//if the starting point of the line has changed since before, reposition it.
		if (x1 != lastx1 || y1 != lasty1) {
			mainLine.setVisible(false);
			mainLine.setRotation(0f);
			mainLine.setPosition(x1 - LINE_WIDTH/2, y1 - LINE_WIDTH/2);
		}
		//the following must run whether the initial point has changed or not.
		float length = (float)Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1)) + LINE_WIDTH;
		float angle = (float)Math.toDegrees(Math.atan((y2-y1)/(x2-x1))) + (x2 < x1 ? 180 : 0);
		mainLine.setWidth(length);
		mainLine.setRotation(angle);
		if (!mainLine.isVisible())
			mainLine.setVisible(true);
	}
	
	public void ShowHint(float x1, float y1, float x2, float y2) {
		hintSegment.setVisible(false);
		hintSegment.setRotation(0f);
		hintSegment.setPosition(x1 - HINT_WIDTH/2, y1 - HINT_WIDTH/2);
		float length = (float)Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1)) + HINT_WIDTH;
		float angle = (float)Math.toDegrees(Math.atan((y2-y1)/(x2-x1))) + (x2 < x1 ? 180 : 0);
		hintSegment.setWidth(length);
		hintSegment.setRotation(angle);
		hintSegment.setVisible(true);
	}

	public void HideHint() {
		if (hintSegment.isVisible())
			hintSegment.setVisible(false);
	}
	
	public void Hide() {
		this.hintSegment.setVisible(false);
		this.setVisible(false);
	}
}
