package com.greasybubbles.bringdown.uielements;

import org.andengine.entity.Entity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;

import com.greasybubbles.bringdown.graphics.GraphicSystemParameters;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

public class FuseCancelButton extends Entity {

	private boolean active = false;
	private boolean exploded;
	
	private Sprite buttonBody;
	private Rectangle timerBar;
	private float timerPercentage;
	
	//private Explosive linkedExplosive;

	public static final float FUSE_TIME = 0.5f; //the duration in seconds of the fuse.
	private static final int TIMERBAR_MAX_LENGTH = 176;
	private static final int TIMERBAR_ORIGIN_X = 16;
	private static final int TIMERBAR_ORIGIN_Y = 188;
	private static final int TIMERBAR_WIDTH = 32;
	
	private static final int BUTTON_POS_X_HIDDEN = -65;
	private static final int BUTTON_POS_X_ACTIVE = 10;
	private static final int BUTTON_POS_Y = 100;
	
	private final float ANIMATION_TIME = 0.25f;
	
	public FuseCancelButton(GreasyScene sc, GraphicSystemParameters gsp) {
		super(0,0);
		
		//linkedExplosive = expl;
		
		buttonBody = new Sprite(0, 0, GreasyAssets.getTexture("cancel"), gsp.vboManager) {
			@Override
			public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
					if (active) {
						OnFuseDefused();
						return true;
					}
				}
				return false;
			}
		};
		timerBar = new Rectangle(TIMERBAR_ORIGIN_X, TIMERBAR_ORIGIN_Y, TIMERBAR_WIDTH, 0, gsp.vboManager);
		timerBar.setColor(0.8f, 0.1f, 0.1f);
		
		this.attachChild(buttonBody);
		this.attachChild(timerBar);

		this.setPercentage(1f);

		//moves the whole structure into the "stowed" position (just outside of the screen, not visible)
		this.setX(BUTTON_POS_X_HIDDEN);
		this.setY(BUTTON_POS_Y);

		this.active = false;
		
		sc.registerTouchArea(buttonBody);
	}
	
	public void setPercentage(float perc) {
		if (perc < 0f) perc = 0f;
		if (perc > 1f) perc = 1f;
		timerPercentage = perc;
		timerBar.setHeight((float)TIMERBAR_MAX_LENGTH*perc);
		timerBar.setY(TIMERBAR_ORIGIN_Y - timerBar.getHeight());
		if (perc <= 0f) {
			OnTimeUp();
		}
	}
	
	public void Update(float timePassed) {
		float percentageSpent = timePassed/FUSE_TIME;
		if (this.timerPercentage > percentageSpent) {
			setPercentage(timerPercentage - percentageSpent);
		} else {
			setPercentage(0f);
			OnTimeUp();
		}
	}

	public void Start() {
		this.active = true;
		this.exploded = false;
		this.setPercentage(1f);
		this.Show();
	}
	
	public void Show() {
		this.registerEntityModifier(new MoveModifier(ANIMATION_TIME,
				BUTTON_POS_X_HIDDEN, BUTTON_POS_X_ACTIVE, BUTTON_POS_Y, BUTTON_POS_Y));
	}
	
	public void Hide() {
		this.registerEntityModifier(new MoveModifier(ANIMATION_TIME,
				BUTTON_POS_X_ACTIVE, BUTTON_POS_X_HIDDEN, BUTTON_POS_Y, BUTTON_POS_Y));
	}
	
	public void OnTimeUp() {
		this.active = false;
		this.exploded = true;
		this.Hide();
	}
	
	public void OnFuseDefused() {
		this.active = false;
		this.Hide();
	}
	
	public boolean isActive() {
		return active;
	}
	
	public boolean hasExploded() {
		return exploded;
	}
	
}
