package com.greasybubbles.bringdown.sfx;

import static com.greasybubbles.bringdown.Constants.*;

import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;

import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

public class FxLevelEndBanner extends SpecialEffect {

	public static final int GOOD = 1;
	public static final int BAD = 2;
	
	private static final long effectDuration = 5000; //milliseconds
		
	private Sprite bannerBackground;
	private Text bannerText;

	public FxLevelEndBanner(int style, String text, GreasyScene scene) {
		super(scene, effectDuration);
		String assetName;
		if (style==GOOD) assetName = "bannerblue";
		else if (style==BAD) assetName = "bannerred";
		else throw new RuntimeException();
		bannerBackground = new Sprite(0, 0, GreasyAssets.getTexture(assetName), scene.vboManager);
		bannerText = new Text(0, 0, GreasyAssets.fBigFont.getFont(), text, scene.vboManager);
	}
	
	@Override
	void Instantiate() {
		//calculate screen constants
		final float BAN_X = (RASTER_WIDTH - bannerBackground.getWidth())/2;
		final float BAN_START_Y = 0 - 10 - bannerBackground.getHeight();
		final float BAN_MID_Y = (RASTER_HEIGHT - bannerBackground.getHeight())/2;
		final float BAN_END_Y = RASTER_HEIGHT + 10;
		final float TEXT_Y = (RASTER_HEIGHT - bannerText.getHeight())/2;
		final float TEXT_START_X = 0 - 10 - bannerText.getWidth();
		final float TEXT_MID_X = (RASTER_WIDTH - bannerText.getWidth())/2;
		final float TEXT_END_X = RASTER_WIDTH + 10;

		final float ENTRY_TIME = 0.2f;
		final float TEXT_STAY_TIME = 1.5f;
		
		bannerBackground.registerEntityModifier(new SequenceEntityModifier(
				new MoveModifier(ENTRY_TIME, BAN_X, BAN_X, BAN_START_Y, BAN_MID_Y),
				new DelayModifier(ENTRY_TIME + TEXT_STAY_TIME + ENTRY_TIME),
				new MoveModifier(ENTRY_TIME, BAN_X, BAN_X, BAN_MID_Y, BAN_END_Y)));

		bannerText.registerEntityModifier(new SequenceEntityModifier(
				new DelayModifier(ENTRY_TIME),
				new MoveModifier(ENTRY_TIME, TEXT_START_X, TEXT_MID_X, TEXT_Y, TEXT_Y),
				new DelayModifier(TEXT_STAY_TIME),
				new MoveModifier(ENTRY_TIME, TEXT_MID_X, TEXT_END_X, TEXT_Y, TEXT_Y)));
					
		scene.layerOVERLAY1.attachChild(bannerBackground);
		scene.layerOVERLAY1.attachChild(bannerText);
	}
	
	@Override
	void Destroy() {
		if (bannerBackground.hasParent())
			bannerBackground.detachSelf();
		if (bannerText.hasParent())
			bannerText.detachSelf();
	}	
}
