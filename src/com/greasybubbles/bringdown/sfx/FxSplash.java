package com.greasybubbles.bringdown.sfx;

import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.IParticleEmitter;
import org.andengine.entity.particle.emitter.RectangleParticleEmitter;
import org.andengine.entity.particle.initializer.AccelerationParticleInitializer;
import org.andengine.entity.particle.initializer.ColorParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.ScaleParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.sprite.Sprite;
import org.andengine.util.color.Color;

import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

import static com.greasybubbles.bringdown.Constants.*;

public class FxSplash extends SpecialEffect {

	private static final long effectDuration = 3000; //milliseconds
	
	private SpriteParticleSystem partSys;
	
	private int pixelX, pixelY, pixelWidth;	
	private int numberOfDrops;
	private Color color1, color2;
	
	public FxSplash(int x, int width, int y, Color color1, Color color2, GreasyScene scene) {
		super(scene, effectDuration);
		pixelX = TileMapper.XGridToScreen(x);
		pixelWidth = TileMapper.XGridToScreen(x + width) - pixelX;
		pixelY = TileMapper.YGridToScreen(y);
		numberOfDrops = width * 2;
		this.color1 = color1;
		this.color2 = color2;
	}
	
	@Override
	void Instantiate() {
		final float EMITTER_HEIGHT = 2; //pixels
		IParticleEmitter myEmitter = new RectangleParticleEmitter
				(pixelX-32 + pixelWidth/2, pixelY-32 + EMITTER_HEIGHT/2, pixelWidth, EMITTER_HEIGHT);
		//TODO find a way to disappear the "magic number" 32 (half the height of the texture - so it's centred)
		
		partSys = new SpriteParticleSystem(myEmitter, 500, 500, numberOfDrops,
				GreasyAssets.getTexture("droplet"), scene.vboManager);
		partSys.addParticleInitializer(new VelocityParticleInitializer<Sprite>(-200, 200, -600, -400));
		partSys.addParticleInitializer(new RotationParticleInitializer<Sprite>(0.0f, 359.0f));
		partSys.addParticleInitializer(new ScaleParticleInitializer<Sprite>(0.3f));
		partSys.addParticleInitializer(new AccelerationParticleInitializer<Sprite>(0f,PIXEL_GRAVITY_ACCELERATION));
		partSys.addParticleInitializer(new ColorParticleInitializer<Sprite>(color1, color2));
		scene.getFXLayer().attachChild(partSys);
		partSys.setParticlesSpawnEnabled(true);
	}
	
	@Override
	void Destroy() {
		partSys.setParticlesSpawnEnabled(false);
		partSys.detachSelf();
		partSys.dispose();
	}
}
