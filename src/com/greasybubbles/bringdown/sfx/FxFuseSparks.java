package com.greasybubbles.bringdown.sfx;

import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.IParticleEmitter;
import org.andengine.entity.particle.emitter.PointParticleEmitter;
import org.andengine.entity.particle.initializer.AccelerationParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.particle.modifier.ExpireParticleInitializer;
import org.andengine.entity.sprite.Sprite;

import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

public class FxFuseSparks extends SpecialEffect {

	private static final long effectDuration = 1000;
	
	private static final int MAX_SPARKS_ON_SCREEN_AT_ONCE = 8;
	
	SpriteParticleSystem sparkParticles;
	
	private int pixelX, pixelY;
	
	public FxFuseSparks(float gridX, float gridY, GreasyScene scene) {
		super(scene, effectDuration);
		pixelX = TileMapper.XGridToScreen(gridX);
		pixelY = TileMapper.YGridToScreen(gridY);
	}
	
	@Override
	void Instantiate() {
		IParticleEmitter sparkEmitter = new PointParticleEmitter(pixelX, pixelY);
		
		sparkParticles = new SpriteParticleSystem(sparkEmitter, 10.0f, 10.0f, MAX_SPARKS_ON_SCREEN_AT_ONCE,
				GreasyAssets.getTexture("spark"), scene.vboManager);

		sparkParticles.addParticleInitializer(new AccelerationParticleInitializer<Sprite>(0));
		sparkParticles.addParticleInitializer(new VelocityParticleInitializer<Sprite>(-100.0f, 100.0f, -100.0f, 100.0f));
		sparkParticles.addParticleInitializer(new ExpireParticleInitializer<Sprite>(0.5f));
		sparkParticles.addParticleModifier(new AlphaParticleModifier<Sprite>(0.4f, 0.45f, 1.0f, 0.0f));
		
		scene.getFXLayer().attachChild(sparkParticles);
		sparkParticles.setParticlesSpawnEnabled(true);
	}

	@Override
	public void Stop() {
		sparkParticles.setParticlesSpawnEnabled(false);
	}
	
	@Override
	void Destroy() {
		sparkParticles.setParticlesSpawnEnabled(false);
		sparkParticles.detachSelf();
		sparkParticles.dispose();
	}

}
