package com.greasybubbles.bringdown.sfx;

import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.IParticleEmitter;
import org.andengine.entity.particle.emitter.RectangleParticleEmitter;
import org.andengine.entity.particle.initializer.AlphaParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.sprite.Sprite;

import com.greasybubbles.bringdown.Constants;
import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

public class FxKnockStars extends SpecialEffect {

	private static final long effectDuration = 1000; //milliseconds
	
	private SpriteParticleSystem starsPS;
	
	private int pixelX, pixelY;
	private int pixelHeight;
	
	public FxKnockStars(float x, float y, int height, GreasyScene scene) {
		super(scene, effectDuration);
		pixelX = TileMapper.XGridToScreen(x);
		pixelY = TileMapper.YGridToScreen(y + 0.5f * height);
		pixelHeight = height * Constants.PIXELS_PER_CELL_ON_SCREEN;
	}
	
	@Override
	void Instantiate() {
		IParticleEmitter starEmitter = new RectangleParticleEmitter(pixelX-16, pixelY-16, 1, pixelHeight);
		//TODO find a way to disappear the "magic number" 16 (half the height of the texture - so it's centred)
		
		starsPS = new SpriteParticleSystem(starEmitter, 200.0f, 200.0f, 2,
				GreasyAssets.getTexture("star"), scene.vboManager);
		starsPS.addParticleInitializer(new VelocityParticleInitializer<Sprite>(-50.0f, 50.0f, 1.0f, 1.0f));
		starsPS.addParticleInitializer(new RotationParticleInitializer<Sprite>(0.0f, 359.0f));
		starsPS.addParticleInitializer(new AlphaParticleInitializer<Sprite>(0.5f));
		starsPS.addParticleModifier(new AlphaParticleModifier<Sprite>(0.1f, 0.3f, 1f, 0.0f));
		
		scene.getFXLayer().attachChild(starsPS);
		starsPS.setParticlesSpawnEnabled(true);
	}
	
	@Override
	void Destroy() {
		starsPS.setParticlesSpawnEnabled(false);
		starsPS.detachSelf();
		starsPS.dispose();
	}
}
