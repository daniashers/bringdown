package com.greasybubbles.bringdown.sfx;

import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.text.Text;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.ease.EaseCubicOut;

import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

public class FxTextPopUp extends SpecialEffect {

	private static final long effectDuration = 2000; //milliseconds
	
	private int pixelX, pixelY;
	
	private Text ptsText;
	private String text;
	private Color color;
	
	public FxTextPopUp(float gridX, float gridY, String textToWrite, Color textColor, GreasyScene scene) {
		super(scene, effectDuration);
		pixelX = TileMapper.XGridToScreen(gridX);
		pixelY = TileMapper.YGridToScreen(gridY);
		text = textToWrite;
		color = textColor;
	}
	
	@Override
	void Instantiate() {
		//TODO make magic numbers go away
		ptsText = new Text(pixelX, pixelY, GreasyAssets.fMainFont.getFont(), text,
				scene.vboManager);
		ptsText.setColor(color);
		
		ptsText.registerEntityModifier(new MoveModifier(0.75f, ptsText.getX(), ptsText.getX(),
				ptsText.getY(), ptsText.getY()-50, EaseCubicOut.getInstance()));
		ptsText.registerEntityModifier(new SequenceEntityModifier(
				new DelayModifier(0.75f), new ParallelEntityModifier(
						new ScaleModifier(0.25f, 1.0f, 2.0f), new AlphaModifier(0.25f, 1.0f, 0.0f))));
		
		scene.getFXLayer().attachChild(ptsText);
	}
	
	@Override
	void Destroy() {
		// TODO Auto-generated method stub
		ptsText.detachSelf();
		ptsText.dispose();
	}
}
