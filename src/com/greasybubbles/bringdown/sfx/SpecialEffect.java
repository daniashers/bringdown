package com.greasybubbles.bringdown.sfx;

import com.greasybubbles.greasytools.GreasyScene;

public abstract class SpecialEffect {

	//types: "one shot", "continue until manually destroyed"

	protected long sunsetTime; //the time at which this effect must be removed from the scene
	
	protected GreasyScene scene;

	public SpecialEffect(GreasyScene scene, long effectDuration) {
		this.scene = scene;
		sunsetTime = System.currentTimeMillis() + effectDuration;
	}
	
	abstract void Instantiate();
		
	abstract void Destroy();

	public void Stop() {
		
	}
	
	boolean HasTerminated() {
		if (System.currentTimeMillis() > sunsetTime)
			return true;
		else
			return false;		
	}
}
