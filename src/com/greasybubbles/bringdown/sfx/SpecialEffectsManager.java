package com.greasybubbles.bringdown.sfx;

import java.util.ArrayList;

public class SpecialEffectsManager {
	
	private static ArrayList<SpecialEffect> extantEffects = new ArrayList<SpecialEffect>();
	
	public static void AddEffect(SpecialEffect fx) {
		if (!extantEffects.contains(fx)) {
			extantEffects.add(fx);
			fx.Instantiate();
		}		
	}
	
	public static void ProcessEffects() {
		//(i iterate over a clone so that i don't get a concurrent modification exception
		//for trying to remove elements from the same array. Inefficient)
		//TODO think of something better.
		for (SpecialEffect fx : new ArrayList<SpecialEffect>(extantEffects)) {
			if (fx.HasTerminated()) {
				extantEffects.remove(fx);
				fx.Destroy();
			}
		}
	}
	
	public static void DestroyAllEffects() {
		for (SpecialEffect fx : extantEffects) {
			fx.Destroy();
		}
		extantEffects.clear();
	}
}
