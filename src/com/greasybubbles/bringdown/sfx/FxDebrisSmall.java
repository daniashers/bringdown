package com.greasybubbles.bringdown.sfx;

import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.IParticleEmitter;
import org.andengine.entity.particle.emitter.RectangleParticleEmitter;
import org.andengine.entity.particle.initializer.AccelerationParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.ScaleParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;

import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.greasytools.GreasyScene;

import static com.greasybubbles.bringdown.Constants.*;

public class FxDebrisSmall extends SpecialEffect {

	private static final long effectDuration = 3000; //milliseconds
	
	private SpriteParticleSystem partSys;
	
	private int pixelX, pixelY;
	private int pixelHeight, pixelWidth;

	private int numberOfDebris;
	private TextureRegion textureRegion;
	
	public FxDebrisSmall(float x, float y, int width, int height, TextureRegion texture, GreasyScene scene) {
		super(scene, effectDuration);
		pixelX = TileMapper.XGridToScreen(x + 0.5f * width);
		pixelY = TileMapper.YGridToScreen(y + 0.5f * height);
		pixelWidth = width * PIXELS_PER_CELL_ON_SCREEN;
		pixelHeight = height * PIXELS_PER_CELL_ON_SCREEN;
		numberOfDebris = Math.round(2.5f*width*height);
		textureRegion = texture;
	}
	
	@Override
	void Instantiate() {
		IParticleEmitter myEmitter = new RectangleParticleEmitter(pixelX-textureRegion.getWidth()/2,
				pixelY-textureRegion.getHeight()/2, pixelWidth, pixelHeight);
		
		partSys = new SpriteParticleSystem(myEmitter, 500.0f, 500.0f, numberOfDebris,
				textureRegion, scene.vboManager);
		final float INIT_VEL = 500;
		partSys.addParticleInitializer(new VelocityParticleInitializer<Sprite>
			(-INIT_VEL, INIT_VEL, -INIT_VEL, INIT_VEL));
		partSys.addParticleInitializer(new RotationParticleInitializer<Sprite>(0.0f, 359.0f));
		partSys.addParticleInitializer(new ScaleParticleInitializer<Sprite>(0.5f));
		partSys.addParticleInitializer(new AccelerationParticleInitializer<Sprite>(0f,PIXEL_GRAVITY_ACCELERATION));
		
		scene.getFXLayer().attachChild(partSys);
		partSys.setParticlesSpawnEnabled(true);
	}
	
	@Override
	void Destroy() {
		partSys.setParticlesSpawnEnabled(false);
		partSys.detachSelf();
		partSys.dispose();
	}

}
