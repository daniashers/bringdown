package com.greasybubbles.bringdown.sfx;

import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.IParticleEmitter;
import org.andengine.entity.particle.emitter.RectangleParticleEmitter;
import org.andengine.entity.particle.initializer.AlphaParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.ScaleParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.particle.modifier.ScaleParticleModifier;
import org.andengine.entity.sprite.Sprite;

import com.greasybubbles.bringdown.Constants;
import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

public class FxFireball extends SpecialEffect {

	private static final long effectDuration = 1000; //milliseconds

	private SpriteParticleSystem fireballPS;
	
	private int pixelX, pixelY;
	private int pixelHeight;
	
	public FxFireball(float x, float y, int height, GreasyScene scene) {
		super(scene, effectDuration);
		pixelX = TileMapper.XGridToScreen(x);
		pixelY = TileMapper.YGridToScreen(y + 0.5f * height);
		pixelHeight = height * Constants.PIXELS_PER_CELL_ON_SCREEN;
	}
	
	@Override
	void Instantiate() {
		IParticleEmitter fireballEmitter = new RectangleParticleEmitter(pixelX-32, pixelY-32, 1, pixelHeight);
		//TODO find a way to disappear the "magic number" 32 (half the height of the texture - so it's centred)
		
		fireballPS = new SpriteParticleSystem(fireballEmitter, 200.0f, 200.0f, 5,
				GreasyAssets.getTexture("fireball"), scene.vboManager);
		fireballPS.addParticleInitializer(new VelocityParticleInitializer<Sprite>(-50.0f, 50.0f, 1.0f, 1.0f));
		fireballPS.addParticleInitializer(new RotationParticleInitializer<Sprite>(0.0f, 359.0f));
		fireballPS.addParticleInitializer(new AlphaParticleInitializer<Sprite>(0.5f));
		fireballPS.addParticleInitializer(new ScaleParticleInitializer<Sprite>(0.2f));
		fireballPS.addParticleModifier(new AlphaParticleModifier<Sprite>(0.2f, 0.4f, 1.0f, 0.0f));
		fireballPS.addParticleModifier(new ScaleParticleModifier<Sprite>(0.0f, 0.4f, 0.2f, 2.0f));
		
		scene.getFXLayer().attachChild(fireballPS);
		fireballPS.setParticlesSpawnEnabled(true);
	}
	
	@Override
	void Destroy() {
		fireballPS.setParticlesSpawnEnabled(false);
		fireballPS.detachSelf();
		fireballPS.dispose();
	}
}
