package com.greasybubbles.bringdown.sfx;

import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.IParticleEmitter;
import org.andengine.entity.particle.emitter.RectangleParticleEmitter;
import org.andengine.entity.particle.initializer.AlphaParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.sprite.Sprite;

import com.greasybubbles.bringdown.Constants;
import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

public class FxImpactDust extends SpecialEffect {

	private static final long effectDuration = 1000; //milliseconds
	
	private SpriteParticleSystem dustCloudPS;
	
	private int pixelX, pixelY;
	private int pixelWidth;
	
	public FxImpactDust(float x, float y, int width, GreasyScene scene) {
		super(scene, effectDuration);
		pixelX = TileMapper.XGridToScreen(x + 0.5f * width);
		pixelY = TileMapper.YGridToScreen(y);
		pixelWidth = width * Constants.PIXELS_PER_CELL_ON_SCREEN;
	}
	
	@Override
	void Instantiate() {
		IParticleEmitter dustEmitter = new RectangleParticleEmitter(pixelX-32, pixelY-32, pixelWidth, 1);
		//TODO find a way to disappear the "magic number" 32 (half the height of the texture - so it's centred)
		
		dustCloudPS = new SpriteParticleSystem(dustEmitter, 200.0f, 200.0f, 4,
				GreasyAssets.getTexture("dust"), scene.vboManager);
		dustCloudPS.addParticleInitializer(new VelocityParticleInitializer<Sprite>(-50.0f, 50.0f, 1.0f, 1.0f));
		dustCloudPS.addParticleInitializer(new RotationParticleInitializer<Sprite>(0.0f, 359.0f));
		dustCloudPS.addParticleInitializer(new AlphaParticleInitializer<Sprite>(0.5f));
		dustCloudPS.addParticleModifier(new AlphaParticleModifier<Sprite>(0.0f, 0.5f, 0.5f, 0.0f));
		
		
		scene.getFXLayer().attachChild(dustCloudPS);
		dustCloudPS.setParticlesSpawnEnabled(true);
	}
	
	@Override
	void Destroy() {
		dustCloudPS.setParticlesSpawnEnabled(false);
		dustCloudPS.detachSelf();
		dustCloudPS.dispose();
	}
}
