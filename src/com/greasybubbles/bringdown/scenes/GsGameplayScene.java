package com.greasybubbles.bringdown.scenes;

import static com.greasybubbles.bringdown.Constants.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.shape.RectangularShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.util.color.Color;

import com.greasybubbles.bringdown.LevelData;
import com.greasybubbles.bringdown.MasterSettings;
import com.greasybubbles.bringdown.PieceList;
import com.greasybubbles.bringdown.TileMapper;
import com.greasybubbles.bringdown.Waitblock;
import com.greasybubbles.bringdown.Constants.SplitMode;
import com.greasybubbles.bringdown.graphics.GbgCityBackground;
import com.greasybubbles.bringdown.graphics.GbgFieryBackground;
import com.greasybubbles.bringdown.graphics.GbgSwampBackground;
import com.greasybubbles.bringdown.graphics.GbgTutorialBackground;
import com.greasybubbles.bringdown.graphics.GraphicSystemParameters;
import com.greasybubbles.bringdown.graphics.Lava;
import com.greasybubbles.bringdown.graphics.PieceAnimator;
import com.greasybubbles.bringdown.graphics.PieceSprite;
import com.greasybubbles.bringdown.model.GameGrid;
import com.greasybubbles.bringdown.model.GamePiece;
import com.greasybubbles.bringdown.model.GridMath;
import com.greasybubbles.bringdown.model.iLine;
import com.greasybubbles.bringdown.sfx.FxDebris;
import com.greasybubbles.bringdown.sfx.FxDebrisSmall;
import com.greasybubbles.bringdown.sfx.FxFireball;
import com.greasybubbles.bringdown.sfx.FxFuseSparks;
import com.greasybubbles.bringdown.sfx.FxImpactDust;
import com.greasybubbles.bringdown.sfx.FxKnockStars;
import com.greasybubbles.bringdown.sfx.FxSplash;
import com.greasybubbles.bringdown.sfx.FxLevelEndBanner;
import com.greasybubbles.bringdown.sfx.FxTextPopUp;
import com.greasybubbles.bringdown.sfx.SpecialEffectsManager;
import com.greasybubbles.bringdown.uielements.Explosive;
import com.greasybubbles.bringdown.uielements.FuseCancelButton;
import com.greasybubbles.bringdown.uielements.SmartDragLine;
import com.greasybubbles.greasytools.CallbackParams;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyTheme;
import com.greasybubbles.greasytools.IGreasyCallback;
import com.greasybubbles.greasytools.GreasyScene;
import com.greasybubbles.greasytools.GreasyTouchProcessor;

import org.andengine.entity.Entity;

public class GsGameplayScene extends GreasyScene implements IGreasyCallback {
	//
	// CONSTANTS
	//
	
	private final String[] assetsRequired = { "droplet",
			"explstrips", "expldestr", "spark", "dust", "star", "fireball",
			"cancel", "popupbanners", "newsheet", "bgset_fiery",
			"bgset_tut", "bgset_traffic", "bgset_swamp"};
	
	private final String[] COMBO_CONGRATS = { "", "GOOD!", "GREAT!", "SUPER!", "AWESOME!", "TERRIFIC!" };
	
	private enum MachineState {
		LEVEL_INTRO,
		TUTORIAL_BEING_DISPLAYED,
		READY,
		FUSE_BURNING,
		ANIMATING_LATERAL,
		ANIMATING_DIAGONAL,
		ANIMATING_FALLS,
		FROZEN,
		CHECKING_FOR_LEVEL_END,
		END_OF_LEVEL_SCORE_COUNT,
	};
	
	private MachineState machineState;
	
	private GsPauseScene pauseScene; //this will contain a reference to an instance of the pause child scene
	private GsHintScene hintScene;

	// Score tweaks
	private final int DESTROY_MALUS = 10; //per unit of weight
	private final int LEFTOVER_MALUS = 50; //per unit of weight
	private final int SCRAP_BONUS = 10; //per unit of weight
	private final int REMAINING_MOVES_BONUS = 10;
	
	// Game State Variables
	//
		
	public GameGrid gameGrid;
	private int level;
	private int theme;
	private int score;
	private int movesLeft;
	
	private int comboCounter;
	private int weightScrapped;
	
	private boolean isTutorialLevel = false;
	private boolean splashesEnabled;
	private Color splashColor1 = Color.BLACK;
	private Color splashColor2 = Color.BLACK;
	
	//this HashMap stores the connections between the game pieces and their sprites.
	private HashMap<GamePiece, PieceSprite> spriteTable;
	
	private Explosive explObject;
		
	//WORLD OBJECTS
	private GreasyTheme liveBackground;
	private Lava animatedObject1;
	
	//HUD OBJECTS
	private Text scoreText;
	private Text labelMovesLeft;
	private Text numberMovesLeft;
	private FuseCancelButton cancelButton; 
	private SmartDragLine dragLine;

	//position placeholders
	private Entity posScoreText = new Entity(RASTER_WIDTH - 10, +100);
	private Entity posScoreTextMagnified = new Entity(RASTER_WIDTH/2, RASTER_HEIGHT/3);
	private Entity posLabelMovesLeft = new Entity(RASTER_WIDTH - 10, +10);
	private Entity posNumberMovesLeft = new Entity(RASTER_WIDTH - 10, +50);	
	
	//these variables are necessary to keep track of the touch input and when it's changed.
	private int lastX1 = 0, lastY1 = 0, lastX2 = 0, lastY2 = 0;
	private iLine currentHint = null;
		
	//
	// Constructor
	//
	public GsGameplayScene(GraphicSystemParameters gsp, int level) {
		super(gsp);		
		this.level = level;
		theme = LevelData.getTheme(level);
		if (level < 0) {
			isTutorialLevel = true;
		}
		
		BlankScreen();		
		setBackground(new Background(0.5f, 0.5f, 0.5f));

		// LOAD REQUIRED ASSETS
		//---------------------
		//sends the command to load the list of assets (data files, textures etc) needed.
		GreasyAssets.LoadAssets(assetsRequired, gsp);

		PieceSprite.setTextureSheet(GreasyAssets.getTexture("newsheet"));
		
		CreateAndAttachWorldElements();		
		CreateAndAttachHUDElements();
		
		// create game state model ('grid')
		//
		gameGrid = new GameGrid();
		
		//initialises the table that allows us to link GamePieces and their respective sprites
		spriteTable = new HashMap<GamePiece, PieceSprite>();

		LoadLevel(level);
		
		RedrawPieces();
				
		FadeIn();
		
		GreasyTouchProcessor.setProcessedTouchListener(this); //TODO << kludgy debug

		if (LevelData.hasTipScreen(level) || isTutorialLevel) {
			machineState = MachineState.TUTORIAL_BEING_DISPLAYED;
		} else {
			machineState = MachineState.LEVEL_INTRO;
		}
	}
	
	private void CreateAndAttachWorldElements() {
		String grassTextureName;
		
		//creates the background
		switch (theme) {
		case THEME_TUTORIAL:
			liveBackground = new GbgTutorialBackground(this);
			grassTextureName = "tutorialgrass";
			splashesEnabled = false;
			break;
		case THEME_FIERY:
			liveBackground = new GbgFieryBackground(this);
			animatedObject1 = new Lava(0, RASTER_HEIGHT, 800, vboManager);
			grassTextureName = "fierygrass";
			splashesEnabled = true;
			splashColor1 = Color.RED;
			splashColor2 = Color.YELLOW;
			break;
		case THEME_SWAMP:
			liveBackground = new GbgSwampBackground(this);
			grassTextureName = "swampgrass";
			splashesEnabled = true;
			splashColor1 = Color.GREEN;
			splashColor2 = Color.CYAN;
			break;
		case THEME_TRAFFIC:
			liveBackground = new GbgCityBackground(this);
			grassTextureName = "trafficgrass";
			splashesEnabled = false;
			break;
		default:			
			liveBackground = GreasyTheme.NullBackground(this);
			grassTextureName = "fierygrass"; //TODO set default
			splashesEnabled = false;
		}		
		this.layerBG0.attachChild(liveBackground);
		
		//generates the front-ground
		if (animatedObject1 != null) layerFRONT.attachChild(animatedObject1); //attach lava BEFORE grass
		Sprite grassSprite = new Sprite(0, 0, GreasyAssets.getTexture(grassTextureName), graphicSystemParameters.vboManager); //TODO kluge
		grassSprite.setPosition(0, RASTER_HEIGHT-grassSprite.getHeight());
		layerFRONT.attachChild(grassSprite);
	}

	private void CreateAndAttachHUDElements() {
		Rectangle hudRectangle = new Rectangle(RASTER_WIDTH-130, 5, 125, 125, this.vboManager);
		hudRectangle.setColor(Color.BLACK);
		hudRectangle.setAlpha(.5f);
		layerOVERLAY0.attachChild(hudRectangle);
		
		scoreText = new Text(0, 0, GreasyAssets.fMainFont.getFont(), "", 100, this.vboManager);
		scoreText.setColor(Color.YELLOW);
		scoreText.setPosition(RightJustified(scoreText,posScoreText));
		layerOVERLAY1.attachChild(scoreText);		

		labelMovesLeft = new Text(0, 0, GreasyAssets.fMainFont.getFont(), "MOVES", this.vboManager);
		labelMovesLeft.setColor(Color.WHITE);
		labelMovesLeft.setPosition(RightJustified(labelMovesLeft, posLabelMovesLeft));
		layerOVERLAY1.attachChild(labelMovesLeft);

		numberMovesLeft = new Text(0, 0, GreasyAssets.fBigFont.getFont(), "", 3, this.vboManager);
		numberMovesLeft.setScaleCenter(numberMovesLeft.getWidth()/2, numberMovesLeft.getHeight()/2);
		numberMovesLeft.setColor(MovesLeftColor(movesLeft));
		layerOVERLAY1.attachChild(numberMovesLeft);

		cancelButton = new FuseCancelButton(this, graphicSystemParameters);
		this.attachChild(cancelButton);
		
		//inits the line used to illustrate the player's move.
		dragLine = new SmartDragLine(0,0,0,0,this.vboManager);
		layerOVERLAY0.attachChild(dragLine);
		
		if (MasterSettings.DEBUG) {
			layerOVERLAY1.attachChild(new Text(10, 10, GreasyAssets.fSimpleFont.getFont(),
					"(c) Danielle Ashley 2014\nv0.80 Debug", this.vboManager));
		}
	}
	
	private void LoadLevel(int level) {
		gameGrid.getLivePieceList().clear();
		LevelData.FillLevel(gameGrid.getLivePieceList(), level); //loads the level into the proper grid!
		movesLeft = LevelData.getMoveLimit(level);
		gameGrid.PopulateGrid();
	}
	
	public void RedrawPieces() {
		//iterates through all the pieces in the piece list.
		//if there are any new ones for which there is no sprite, create the sprite.
		//then, update the positions accordingly.
		for (GamePiece p : gameGrid.getLivePieceList()) {
			if (!spriteTable.containsKey(p))
			{
				PieceSprite ps = new PieceSprite(p, vboManager);
				spriteTable.put(p, ps);
				layerSPRITE.attachChild(ps);
			}
			spriteTable.get(p).setGridPosition(p.posX + p.offsetX, p.posY + p.offsetY);
		}
		
		//cleans up any sprites whose pieces have been removed from play

		ArrayList<GamePiece> markedForRemoval = new ArrayList<GamePiece>();
		for (GamePiece tp : spriteTable.keySet()) {
			if (!gameGrid.getLivePieceList().contains(tp)) {
				markedForRemoval.add(tp);
			}
		}
		for (GamePiece tp : markedForRemoval) {
			layerSPRITE.detachChild(spriteTable.get(tp));
			spriteTable.remove(tp);
		}
	}
	
	private void PlaceExplosive(Explosive expl) {
		explObject = expl;
		layerSFX.detachChildren();
		layerSFX.attachChild(expl);
		cancelButton.Start();
		machineState = MachineState.FUSE_BURNING;
	}
	
	//this function will be called by the Special Effects Manager
	//so that it knows to what entity to attach the special effects.
	public Entity GetFXLayer() {
		return layerSFX;
	}
	
	private void UpdateOverlays() {
		numberMovesLeft.setText(""+(movesLeft));
		numberMovesLeft.setPosition(RightJustified(numberMovesLeft, posNumberMovesLeft));
		
		scoreText.setText("$ "+score);
		if (machineState != MachineState.END_OF_LEVEL_SCORE_COUNT) // <kluge
			scoreText.setPosition(RightJustified(scoreText, posScoreText));
	}

	private void DecreaseMoves() {
		movesLeft--;
		numberMovesLeft.setColor(MovesLeftColor(movesLeft));
		numberMovesLeft.setText(""+movesLeft);
		numberMovesLeft.setPosition(RightJustified(numberMovesLeft, posNumberMovesLeft));
		numberMovesLeft.registerEntityModifier(new ScaleModifier(0.5f, 2f, 1f)); //nice effect
	}
	
	private void DecreaseMoves_noFX() {
		movesLeft--;
		numberMovesLeft.setColor(MovesLeftColor(movesLeft));
		numberMovesLeft.setText(""+movesLeft);
		numberMovesLeft.setPosition(RightJustified(numberMovesLeft, posNumberMovesLeft));
	}

	
	private Color MovesLeftColor(int movesLeft) {
		if (movesLeft <= 5) return Color.RED;
		else if (movesLeft <= 10) return Color.YELLOW;
		else return Color.GREEN;
	}
	
	@Override
	public void UpdateLoop(float secondsElapsed) {
		
		//these must be done regardless of which mode we are in.
		SpecialEffectsManager.ProcessEffects();
		if (this.liveBackground != null) this.liveBackground.Update(secondsElapsed);
		if (this.animatedObject1 != null) this.animatedObject1.Update(secondsElapsed);		
		//TODO vv do the 'is active' check INSIDE the button code so we can call this every time w/o checking?
		if (cancelButton.isActive()) cancelButton.Update(secondsElapsed);
		this.UpdateOverlays();

		//the big switch loop!
		switch (machineState) {
		case READY:
			//do nothing
			break;
		case LEVEL_INTRO:
			//START WAITBLOCK
			Waitblock introblock = Waitblock.getInstance("introblock");
			switch (introblock.state()) {
			case Waitblock.START:
				SpecialEffectsManager.AddEffect(new FxLevelEndBanner
						(FxLevelEndBanner.GOOD, "READY", this));
				introblock.wait(2.0f, this);
				introblock.YieldAt(1);
			case 1:
				if (!introblock.ready()) break;
				SpecialEffectsManager.AddEffect(new FxLevelEndBanner
						(FxLevelEndBanner.GOOD, "GO!", this));
				introblock.wait(2.0f, this);
				introblock.YieldAt(2);
			case 2:
				if (!introblock.ready()) break;
				introblock.end();
				machineState = MachineState.ANIMATING_FALLS; //we go to Anim. falls instead of Ready in case
			}												//the starting state has some unstable pieces...
			//END WAITBLOCK
			break;
		case TUTORIAL_BEING_DISPLAYED:
			if (this.hasChildScene()) break; //if tut.scene already being displayed do nothing. //TODO kluge
			if (isTutorialLevel) { //is the whole level a tutorial level?
				hintScene = new GsHintScene(graphicSystemParameters, this, 
						LevelData.getHint(level), null);
			} else if (LevelData.hasTipScreen(level)) { //if it's a normal level, has it got a hint screen?
				hintScene = new GsHintScene(graphicSystemParameters, this,
						LevelData.getHint(level), null);
			}
			this.setChildScene(hintScene);
			GreasyTouchProcessor.setProcessedTouchListener(hintScene); //TODO << kludgy debug
			hintScene.Present();
			break;
		case CHECKING_FOR_LEVEL_END:
			if (isTutorialLevel) {
				if (isLevelClear()) {
					if (LevelData.exists(level-1)) {
						level--; //level go backwards as they are negative for tutorials: -1, -2, -3...
						LoadLevel(level);
						gameGrid.PopulateGrid();
						RedrawPieces();
						machineState = MachineState.TUTORIAL_BEING_DISPLAYED;
					} else { //next level non-existing means tutorial finished
						EndLevel(OUTCOME_SUCCESS);
					}
				} else if (movesLeft == 0 || !areThereAnyPossibleMoves()) {
					level = level + 0; //do not advance
					LoadLevel(level);
					gameGrid.PopulateGrid();
					RedrawPieces();
					machineState = MachineState.TUTORIAL_BEING_DISPLAYED;
				} else {
					machineState = MachineState.READY;					
				}
				break;
			}
			//--- if it's not a tutorial level continue here ---
			//first, conclude all the "combo counting" calculations and stuff
			//-This is the formula to calculate the points scored
			int scoreGained = (weightScrapped <=4 ? weightScrapped :
				4+4*(weightScrapped - 4)) * SCRAP_BONUS;
			if (scoreGained > 0) SpecialEffectsManager.AddEffect(
					new FxTextPopUp(14, 2, "+"+scoreGained, COLOR_POINTSPLUS, this));
			score += scoreGained;
			comboCounter = 0;
			weightScrapped = 0;
			// CASE 1: check whether we have cleared the level and therefore won.
			if (isLevelClear()) { //we have cleared the whole level! we won.
				Waitblock lcBlock = Waitblock.getInstance("clear");
				switch (lcBlock.state()) {
				case Waitblock.START:
					SpecialEffectsManager.AddEffect(new FxLevelEndBanner
							(FxLevelEndBanner.GOOD, "LEVEL CLEAR!", this));
					lcBlock.wait(2.0f, this);
					lcBlock.YieldAt(1);
					break;
				case 1:
					machineState = MachineState.END_OF_LEVEL_SCORE_COUNT;
					lcBlock.end();
				}
				break;
			}
			// CASE 2: have we run out of moves and lost?
			else if (movesLeft == 0) {
				SpecialEffectsManager.AddEffect(new FxLevelEndBanner
						(FxLevelEndBanner.BAD, "OUT OF MOVES!", this));
				machineState = MachineState.FROZEN;
				EndLevel(OUTCOME_FAILURE); //ENDS THE SCENE!
				break;
			}
			// CASE 3: check whether we have got ourselves stuck in a situation with no possible moves.
			else if (!areThereAnyPossibleMoves()) {
				Waitblock lcBlock = Waitblock.getInstance("clear");
				switch (lcBlock.state()) {
				case Waitblock.START:
					SpecialEffectsManager.AddEffect(new FxLevelEndBanner
							(FxLevelEndBanner.BAD, "NO POSSIBLE MOVES!", this));
					lcBlock.wait(2.0f, this);
					lcBlock.YieldAt(1);
					break;
				case 1:
					machineState = MachineState.END_OF_LEVEL_SCORE_COUNT;
					lcBlock.end();
				}
				break;
			}
			else
				machineState = MachineState.READY;
			break;
		case FUSE_BURNING:
			if (cancelButton.hasExploded()) {
				OnExplosion(explObject);
			} else if (!cancelButton.isActive()) {
				OnDefuse();
				machineState = MachineState.READY;
			}
			break;
		case ANIMATING_LATERAL:
			PieceAnimator.DoLateralSlideAnimations(gameGrid, secondsElapsed);
			RedrawPieces();
			
			//generates 'knocking stars' special effects if a piece has just slammed laterally on another!
			for (GamePiece fxp : PieceAnimator.sfxQueue_L) {
				SpecialEffectsManager.AddEffect(new FxKnockStars(fxp.posX, fxp.posY, fxp.getHeight(), this));
			}
			for (GamePiece fxp : PieceAnimator.sfxQueue_R) {
				SpecialEffectsManager.AddEffect(new FxKnockStars(fxp.posX+fxp.getWidth(), fxp.posY, fxp.getHeight(), this));
			}

			if (PieceAnimator.FinishedAnimating()) //if finished animating
				machineState = MachineState.ANIMATING_FALLS;
			break;
		case ANIMATING_FALLS:
			if (!PieceAnimator.isAnimating()) {
				GridMath.CalculateFalls(gameGrid);
			}
			PieceAnimator.DoFallingAnimations(gameGrid, secondsElapsed);
			RedrawPieces();
			
			//play thump-down special effects if any need to be played.
			for (GamePiece fxp : PieceAnimator.sfxQueueThud) {
				SpecialEffectsManager.AddEffect(new FxImpactDust(fxp.posX, fxp.posY, fxp.getWidth(), this));
			}
			PieceAnimator.sfxQueueThud.clear();
			
			//play splash special effects if any need to be played.
			if (splashesEnabled) {
				for (GamePiece fxp : PieceAnimator.sfxQueueSplash) {
					final int SPLASH_LINE_Y = -1;
					SpecialEffectsManager.AddEffect(new FxSplash
							(fxp.posX, fxp.getWidth(), SPLASH_LINE_Y, splashColor1, splashColor2, this));
				}
			}
			PieceAnimator.sfxQueueSplash.clear();
			
			//are there any more falling frames to come?
			if (PieceAnimator.FinishedAnimating()) { //if finished animating
				CheckForOutOfPlayPieces();
				machineState = MachineState.ANIMATING_DIAGONAL;
			}
			break;	
		case ANIMATING_DIAGONAL:
			if (!PieceAnimator.isAnimating()) {
				GridMath.CalculateDiagonalSlides(gameGrid);
				if (!GridMath.anyPiecesSliding) {
					machineState = MachineState.CHECKING_FOR_LEVEL_END;
					break;
				}	
			}
			PieceAnimator.DoDiagonalAnimations(gameGrid, secondsElapsed);
			RedrawPieces();
			if (PieceAnimator.FinishedAnimating()) //if finished animating
				machineState = MachineState.ANIMATING_FALLS;
			break;
		default:
			break;
		case END_OF_LEVEL_SCORE_COUNT:
			Waitblock waitblock = Waitblock.getInstance("destroyFinalPieces");
			//START WAITBLOCK
			switch (waitblock.state()) {
			case Waitblock.START:
				boolean anyCountingToDo;
				if (!gameGrid.getPieceList_NoFixed().isEmpty() || movesLeft > 0) {
					anyCountingToDo = true; 
					waitblock.goTo(1);
				} else {
					anyCountingToDo = false;
					waitblock.goTo(5);
				}
				break;
			case 1:
				scoreText.registerEntityModifier(new MoveModifier(0.25f, posScoreText.getX(),
						posScoreTextMagnified.getX(), posScoreText.getY(),
						posScoreTextMagnified.getY()));
				scoreText.registerEntityModifier(new ScaleModifier(0.25f, 1, 3, waitblock));
				//^'blows up' the score so we see what is going on					
				waitblock.YieldAt(2);
				break;
			case 2:
				//takes pieces one at a time and fades them adding a delay before repeating for the next
				//piece. When all the pieces are gone
				PieceList remainingPieces = gameGrid.getPieceList_NoFixed();
				if (!remainingPieces.isEmpty()) {
					GamePiece p = remainingPieces.get(0);
					if (!p.isFixed()) {
						FadeOutPiece(p);
						waitblock.wait(1f, this);
						waitblock.YieldAt(2);
						break;
					}
				} else {
					waitblock.goTo(3);
				}
				break;
			case 3:
				//takes all remaining moves and for each one adds a bonus to the score.
				if (movesLeft > 0) {
					DecreaseMoves_noFX();
					score += REMAINING_MOVES_BONUS;
					waitblock.wait(0.3f, this);
					waitblock.YieldAt(3);
					break;
				}
				waitblock.goTo(4);
				break;
			case 4:
				Entity newScorePos = RightJustified(scoreText, posScoreText);
				scoreText.registerEntityModifier(new MoveModifier(0.25f, posScoreTextMagnified.getX(),
						newScorePos.getX(), posScoreTextMagnified.getY(), newScorePos.getY()));
				scoreText.registerEntityModifier(new ScaleModifier(0.25f, 3, 1, waitblock));
				//^shrinks the score again.
				waitblock.YieldAt(5);
				break;
			case 5:	
				//at this point if after the penalty was applied, my score is still 'in the black' (positive)
				//then I've won the level anyway. Otherwise I've lost it!
				if (score >= 0) {
					EndLevel(OUTCOME_SUCCESS); //ENDS THE SCENE!
				} else {
					EndLevel(OUTCOME_FAILURE); //ENDS THE SCENE!
				}
				machineState = MachineState.FROZEN;
				waitblock.end();				
			}
			//END WAITBLOCK
			break;
		}
	}
	
	private void CheckForOutOfPlayPieces() {
		PieceList recycleBin = new PieceList();
		for (GamePiece p : gameGrid.getLivePieceList()) {
			if (p.posY <= -p.getHeight()) { //this condition is to ensure it has sunken completely out of view
				recycleBin.add(p);
			}
		}
		
		for (GamePiece rp : recycleBin) {
			weightScrapped += rp.getWeight();
			gameGrid.RemovePieceAsScrapped(rp);
			//keeps count of combo count and play some congrats text.
			comboCounter++;
			String congratsText;
			if (comboCounter < COMBO_CONGRATS.length)
				congratsText = COMBO_CONGRATS[comboCounter];
			else
				congratsText = "" + comboCounter + "x!";
			SpecialEffectsManager.AddEffect(new FxTextPopUp(rp.posX, 0, congratsText, Color.GREEN, this));
		}
	}
	
	//
	// Touch Interface Implementation
	//	
	
	@Override
	public void OnTapDown(int x, int y)
	{
		//do nothing.
	}
	
	@Override
	public void OnDrag(int startX, int startY, int currentX, int currentY){
		dragLine.Show(startX, startY, currentX, currentY);
		
		//now I calculate the nearest integer coordinates to the drag.
		//I will then check whether as a result of the drag the integer coordinates are
		//the same or have changed.
		int gridX1 = Math.round(TileMapper.TouchXToGridCell(startX));
		int gridY1 = Math.round(TileMapper.TouchYToGridCell(startY));
		int gridX2 = Math.round(TileMapper.TouchXToGridCell(currentX));
		int gridY2 = Math.round(TileMapper.TouchYToGridCell(currentY));

		boolean snappedCoordinateChanged = false;
		
		if (gridX1 != lastX1) { lastX1 = gridX1; snappedCoordinateChanged = true; }
		if (gridY1 != lastY1) { lastY1 = gridY1; snappedCoordinateChanged = true; }
		if (gridX2 != lastX2) { lastX2 = gridX2; snappedCoordinateChanged = true; }
		if (gridY2 != lastY2) { lastY2 = gridY2; snappedCoordinateChanged = true; }
		
		if (snappedCoordinateChanged) {
			currentHint = null;
			dragLine.HideHint();
			iLine[] segms = iLine.createLine(gridX1, gridY1, gridX2, gridY2).GetSubsegments();
			outerLoop: for (iLine ln : segms) {
				for (iLine edge : gameGrid.allValidLines) {
					if (ln.CompareTo(edge)) {
						dragLine.ShowHint(
							TileMapper.XGridToScreen(ln.x1), TileMapper.YGridToScreen(ln.y1),
							TileMapper.XGridToScreen(ln.x2), TileMapper.YGridToScreen(ln.y2));
						currentHint = ln;
						break outerLoop;
					}
				}
			}
		}		
	}
	
	@Override
	public void OnReleaseDrag(int startX, int startY, int endX, int endY) {		
		dragLine.Hide();
		
		//if we're not expecting any input right now, do nothing
		if (machineState != MachineState.READY)
			return;

		//currentHint stores the highlighted piece edge that is being suggested as the next move
		//to the player. If we HINT at that move, then when the player releases, that's the move that
		//we are going to do. If there's no hint, it means there's no valid move possible with the
		//current swipe.
		if (currentHint == null)
			return;
		
		int gridX1 = currentHint.x1;
		int gridY1 = currentHint.y1;
		int gridX2 = currentHint.x2;
		int gridY2 = currentHint.y2;

		//swaps the point so that point1 is always the lower (lower Y) one
		if (gridY1>gridY2) {
			int temp;
			temp = gridX1; gridX1 = gridX2; gridX2 = temp;
			temp = gridY1; gridY1 = gridY2; gridY2 = temp;
		}
				
		//FIRST, check if the line we have drawn is a valid edge between pieces.
		//if it is, place an explosive in between the pieces.
		if (GridMath.MatchVerticalOrDiagonalEdge(gameGrid, gridX1, gridY1, gridX2, gridY2)) {			
			PlaceExplosive(new Explosive(gridX1, gridY1,
					GridMath.WhatLineType(gridX1, gridY1, gridX2, gridY2), this));
			//TODO debug vv
			//TODO replace the 'fuse sparks' as a special effect with a particle system
			//internal and encapsulated inside the Explosive class.
			SpecialEffectsManager.AddEffect(new FxFuseSparks(gridX1, gridY1, this));
			return;
		}

		//SECOND, if the above wasn't the case, check if the line drawn is a valid split point for a piece.
		//if this is the case, place an explosive across the piece to split it.
		//TODO: rewrite the below to put the checks for valid splits inside the GridMath class.
		for (GamePiece p : gameGrid.getPieceList_NoFixed()) {
			if (p.isValidSplit(gridX1, gridY1, gridX2, gridY2)) {
				LineType splitType = GridMath.WhatLineType(gridX1, gridY1, gridX2, gridY2);
				PlaceExplosive(new Explosive(p, splitType, this));
				break;
			}
		}
	}
	
	
	
	private void OnDefuse() {
		explObject.detachSelf();
		//TODO add puff of smoke for defused explosive.
	}
	
	private void OnExplosion(Explosive explObject) {
	//This function gets called whenever an explosive explodes.
	//It contains logic for all three types of explosive actions.

		//a successful explosion counts as a move!
		DecreaseMoves();
		
		if (explObject == null)
			throw new RuntimeException("Something is wrong!");
		
		explObject.detachSelf(); //first, remove the explosive sprite from view.

		switch (explObject.getExplMode()) {
		case Destroy:
			//Destroy the piece!! (if it is destroyable)
			GamePiece explPiece = explObject.affectedPiece;
		
			//generates the explosion fireball effect, whether the piece actually crumbles or not.
			SpecialEffectsManager.AddEffect(
				new FxFireball(explPiece.posX+0.5f*explPiece.getWidth(), explPiece.posY, explPiece.getHeight(), this));
			
			//if the piece is destroyable, destroy it and generate the appropriate effects.
			if (explPiece.isDestroyable()) {
				DestroyPiece(explPiece);
			}
			machineState = MachineState.ANIMATING_FALLS;
			break;
		case Apart:
			iLine explLine = iLine.createLine(explObject.x1, explObject.y1, explObject.x2, explObject.y2);
			//shatter any glass panes which may have been next to the blast
			for (GamePiece p : GridMath.GetShatteredPieces(gameGrid, explLine)) {
				ShatterPiece(p);
			}
			//now calculate the lateral pushes for the pieces next to the blast
			GridMath.CalculateLateralPushes(gameGrid, explLine);
			SpecialEffectsManager.AddEffect(new FxFireball(explLine.centreX(), explLine.bottomY(), explLine.topY(), this));
			machineState = MachineState.ANIMATING_LATERAL;
			break;
		case Split:
			//Split the piece in two!
			if (explObject.affectedPiece.isSplittable()) {
				switch (explObject.type) {
				case HorzSingle:
					gameGrid.SplitPiece(explObject.affectedPiece, SplitMode.Horizontal);
					break;
				case VertSingle:
					gameGrid.SplitPiece(explObject.affectedPiece, SplitMode.Vertical);
					break;
				case DiagSlash:
					gameGrid.SplitPiece(explObject.affectedPiece, SplitMode.DiagSlash);
					break;
				case DiagBackslash:
					gameGrid.SplitPiece(explObject.affectedPiece, SplitMode.DiagBackslash);
					break;
				default:
					throw new RuntimeException("Main: wrong split.");
				}
				GamePiece splPiece = explObject.affectedPiece;
				SpecialEffectsManager.AddEffect(new FxDebrisSmall(splPiece.posX+0.5f*splPiece.getWidth(),
						1f*splPiece.posY, splPiece.getWidth()/2, splPiece.getHeight()/2,
						PieceSprite.getSmallDebrisTextureForMaterial(explObject.affectedPiece.material),this));

				
				gameGrid.PopulateGrid();
				RedrawPieces();
			}
			machineState = MachineState.ANIMATING_FALLS;
		}
		
	}
	
	@Override
	public void OnTapUp(int x, int y) {
	
	}
	
	@Override
	public void OnDoubleTapDown(int pixelX, int pixelY) {
		//if we're not expecting any input right now, do nothing
		if (machineState != MachineState.READY)
			return;
		
		float gridX = TileMapper.TouchXToGridCell(pixelX);
		float gridY = TileMapper.TouchYToGridCell(pixelY);		
		
		//check if we have touched a piece. If yes, we will try to destroy it. If there's no piece, do nothing.
		GamePiece myPiece = gameGrid.getPieceAt(gridX,gridY);		
		if (myPiece == null) return;
		
		PlaceExplosive(new Explosive(myPiece, this));
	}
	
	@Override
	public void OnDoubleTapUp(int x, int y) {

	}

	@Override
	public boolean OnBackButtonPressed() {
		pauseScene = new GsPauseScene(graphicSystemParameters, this);
		this.setChildScene(pauseScene);
		GreasyTouchProcessor.setProcessedTouchListener(pauseScene); //TODO << kludgy debug
		pauseScene.Present();
		return true;
	}

	
	//the below method will be called when the child scene has finished!
	@Override
	public void onSceneFinishedCallback(GreasyScene callerScene, CallbackParams params) {
		if (callerScene == pauseScene) {
			this.clearChildScene();
			if (pauseScene.quit) {
				SpecialEffectsManager.AddEffect(new FxLevelEndBanner
						(FxLevelEndBanner.BAD, "YOU QUIT!", this));
				machineState = MachineState.FROZEN;
				EndLevel(OUTCOME_ABORT); //ENDS THE SCENE!
			}
			pauseScene = null;			
		} else if (callerScene == hintScene) {
			machineState = MachineState.ANIMATING_FALLS; //the game starts.
			this.clearChildScene();
			hintScene = null;
		}
		//return touch control to main scene.
		GreasyTouchProcessor.setProcessedTouchListener(this); //TODO << kludgy debug		
	}	

	private boolean isLevelClear() {
		//if we find any leftover pieces that are active (not fixed) then the level is not yet clear...
		if (gameGrid.getPieceList_NoFixed().isEmpty())
			return true;
		else
			return false;
	}	
	
	private boolean areThereAnyPossibleMoves() {
		for (GamePiece p : gameGrid.getPieceList_NoFixed()) {
			if (p.isSplittable() && p.isSplittableShape()) {
				//there are some pieces that can be further split, therefore there are moves available.
				return true;
			}
			if (p.isDestroyable()) {
				//there are pieces that can be destroyed, so there are still moves possible.
				return true;
			}
			if (p.isMovable()) {
				//if there are any movable pieces AND they can be moved laterally, then there's still moves.
				if (GridMath.hasPieceAnythingToPushAgainst(gameGrid, p)) return true;
			}
		}
		//if nowhere in the above loop have I found a valid move, I decide that THERE ARE NO MORE MOVES.
		return false;
		//yes I KNOW I KNOW that this simplistic algorithm doesn't consider all possibilities
		//e.g. pieces getting stuck between fixed blocks, too much weight etc etc
		//but I'll have to live with it. If this happens, the user just has to give up. That's okay.
	}
	
	private void DestroyPiece(GamePiece piece) {
		int pointsLost = DESTROY_MALUS*piece.getWeight();
		SpecialEffectsManager.AddEffect( new FxTextPopUp(piece.posX, piece.posY,
				""+(-pointsLost), COLOR_POINTSMINUS, this));
		score -= pointsLost;
		SpecialEffectsManager.AddEffect(new FxDebris(piece.posX, piece.posY, piece.getWidth(),
				piece.getHeight(), PieceSprite.getLargeDebrisTextureForMaterial(piece.material), this));				
		gameGrid.RemovePieceAsBlown(piece);
		RedrawPieces();
		gameGrid.PopulateGrid();			
	}
	
	private void FadeOutPiece(GamePiece piece) {
		//a 'fadeout effect' used to put out of play the pieces that are stuck without any moves to do.
		int pointsLost = LEFTOVER_MALUS*piece.getWeight();
		SpecialEffectsManager.AddEffect( new FxTextPopUp(piece.posX+.5f*piece.getWidth(),
				piece.posY + .5f*piece.getHeight(), ""+(-pointsLost), COLOR_POINTSMINUS, this));
		score -= pointsLost;
		spriteTable.get(piece).registerEntityModifier(new AlphaModifier(0.5f, 1.0f, 0.5f));
		gameGrid.RemovePieceAsBlown(piece);
		//RedrawPieces();
		//gameGrid.PopulateGrid();			
	}
	
	private void ShatterPiece(GamePiece piece) {
		int pointsLost = DESTROY_MALUS*piece.getWeight();
		SpecialEffectsManager.AddEffect( new FxTextPopUp(piece.posX, piece.posY,
				""+(-pointsLost), COLOR_POINTSMINUS, this));
		score -= pointsLost;
		SpecialEffectsManager.AddEffect(new FxDebris(piece.posX, piece.posY,
				piece.getWidth(), piece.getHeight(), PieceSprite.getLargeDebrisTextureForMaterial(piece.material), this));				
		gameGrid.RemovePieceAsBlown(piece);
		RedrawPieces();
		gameGrid.PopulateGrid();			
	}
	
	private void EndLevel(int outcome) {
		MasterSettings.saveLevelStats((outcome == OUTCOME_SUCCESS ? true : false), level,
				score, (LevelData.getMoveLimit(level)-movesLeft));
		OverAndOut(2f, 1f, new CallbackParams(
				"outcome="+outcome,
				"level="+level,
				"score="+score,
				"moves="+(LevelData.getMoveLimit(level)-movesLeft)));
	}
	
	private Entity RightJustified(RectangularShape entityToPosition, Entity justifyTo) {
		return new Entity(justifyTo.getX() - entityToPosition.getWidth(), justifyTo.getY());
	}
	private Entity CentreJustified(RectangularShape entityToPosition, Entity justifyTo) {
		return new Entity(justifyTo.getX() - entityToPosition.getWidth()/2, justifyTo.getY());
	}

}