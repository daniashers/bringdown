package com.greasybubbles.bringdown.scenes;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.ITouchArea.ITouchAreaMatcher;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.color.Color;

import com.greasybubbles.bringdown.GsSubMenuAnimated;
import com.greasybubbles.bringdown.graphics.GraphicSystemParameters;
import com.greasybubbles.greasytools.CallbackParams;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.IGreasyCallback;
import com.greasybubbles.greasytools.GreasyButton;
import com.greasybubbles.greasytools.GreasyScene;
import com.greasybubbles.greasytools.GreasySubMenu;
import com.greasybubbles.greasytools.GreasyTouchProcessor;

import static com.greasybubbles.bringdown.Constants.*;

//TODO this scene still has the following problem: the game underneath still receives the touch input.
//I must find a way to block the touch input from reaching the underlying scene.

public class GsHintScene extends GreasyScene {

	private final int WINDOW_WIDTH = 560;
	private final int WINDOW_HEIGHT = 176;

	private final int ORIGIN_X = (RASTER_WIDTH - WINDOW_WIDTH) / 2;
	private final int ORIGIN_Y = (RASTER_HEIGHT - WINDOW_HEIGHT) / 2;
	
	public enum MenuStatus {
		Animating,
		Ready
	}
	
	GreasyScene callerScene;

	GsSubMenuAnimated greasySubMenu;
	
	MenuStatus menuStatus;
	
		
	Rectangle pauseWindowBackground;	
	Text hintText;
	
	public boolean quit = false;
	
	public GsHintScene(GraphicSystemParameters gsp, GreasyScene callerScene, String hintString, String imageAsset) {
		//super(gsp);
		//^ this time we are not calling the superclass constructor
		//cause not doing so will allow us to configure this scene much differently (cause it's a child scene)
		
		graphicSystemParameters = gsp;
		this.callerScene = callerScene;
		
		//sets up the Greasy Touch Listener (processed touch listener)
		this.setOnSceneTouchListener(new GreasyTouchProcessor());
		GreasyTouchProcessor.setProcessedTouchListener(this);
		//TODO ^ this listener shit is dodgy with child scenes. it may rob the main scene of events! REWORK
		
		//disables background so we can still see the parent scene behind the pause screen
		this.setBackgroundEnabled(false);
		
		//we reuse the 'pause window' background but we give it a different colour to make it different...
		pauseWindowBackground = new Rectangle(ORIGIN_X, ORIGIN_Y,
				WINDOW_WIDTH, WINDOW_HEIGHT, gsp.vboManager);
		pauseWindowBackground.setColor(Color.BLACK);
		pauseWindowBackground.setAlpha(.75f);
		
		GreasyButton.Init(this, graphicSystemParameters.vboManager);
		//^^ if we have a scene within a scene re-initialising the same static members will cause problems!
		//consider getting rid of the static members...

		hintText = new Text(ORIGIN_X+10, ORIGIN_Y+10, GreasyAssets.fSimpleFont.getFont(), hintString,
				graphicSystemParameters.vboManager);
		
		CreateHintMenu();
	}
	
	
	private void CreateHintMenu() {
		greasySubMenu = new GsSubMenuAnimated(this);

		greasySubMenu.AddElement(pauseWindowBackground);
		greasySubMenu.AddElement(new SmallButton("hintok", "OK",
				ORIGIN_X + WINDOW_WIDTH - 80, ORIGIN_Y + WINDOW_HEIGHT - 80));
		greasySubMenu.AddElement(hintText);
	}

	public void Present() {
		greasySubMenu.Present();
	}
	
	private void UnregisterAllTouchAreas() {
		this.unregisterTouchAreas(new ITouchAreaMatcher() {
			@Override
			public boolean matches(ITouchArea pObject) {
				return true;
			}
		});
	}
	
	@Override
	public void UpdateLoop(float secondsElapsed) {
		//this scene doesn't have an update loop.		
	}
	
	@Override
	public void OnTapUp(int pixelX, int pixelY) {}
	@Override
	public void OnTapDown(int pixelX, int pixelY) {}
	
	@Override
	public void OnPressButton(GreasyButton buttonPressed) {
		//if we are not ready to take touches, ignore it.
		if (menuStatus != MenuStatus.Ready)
			return;

		//process the touch according to what screen we are in.
		if (buttonPressed.getID().equals("hintok")) {
			menuStatus = MenuStatus.Animating;
			greasySubMenu.HighlightElement(buttonPressed);
			greasySubMenu.Destroy();
		}
		buttonPressed.ChangeColor();
	}

	@Override
	public void OnSubmenuCreated(GreasySubMenu subMenu) {
		menuStatus = MenuStatus.Ready;
	}
		
	@Override
	public void OnSubmenuDestroyed(GreasySubMenu subMenu) {
		UnregisterAllTouchAreas();
		((IGreasyCallback)callerScene).onSceneFinishedCallback(this, new CallbackParams());
	}

	//internal classes
	//
	
	class BigButton extends GreasyButton {
		public BigButton(String id, String text, int x, int y) {
			super(id, (TextureRegion)GreasyAssets.getTexture("button"),
					GreasyAssets.fMainFont.getFont(), text);
			this.setPosition(x,y);
		}
	}

	class SmallButton extends GreasyButton {
		public SmallButton(String id, String text, int x, int y) {
			super(id, (TextureRegion)GreasyAssets.getTexture("buttonsmall"),
					GreasyAssets.fMainFont.getFont(), text);
			this.setPosition(x,y);
		}
	}
	class InvisibleButton extends GreasyButton {
		public InvisibleButton(String id, int x, int y, int width, int height) {
			super(id, width, height);
			this.setPosition(x,y);
		}
	}
	
	class MenuHeaderText extends Text {
		public MenuHeaderText(String text, int y) {
			super(0, 0, GreasyAssets.fMainFont.getFont(), text,
					graphicSystemParameters.vboManager);
			this.setColor(Color.YELLOW);
			this.setX((RASTER_WIDTH-this.getWidth())/2);
			this.setY(y);
		}
	}
	
}
