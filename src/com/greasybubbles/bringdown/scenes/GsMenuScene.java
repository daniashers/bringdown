package com.greasybubbles.bringdown.scenes;


import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.ITouchArea.ITouchAreaMatcher;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.ease.EaseQuintInOut;
import org.andengine.util.modifier.ease.EaseSineInOut;

import static com.greasybubbles.bringdown.Constants.*;

import com.greasybubbles.bringdown.Constants;
import com.greasybubbles.bringdown.GsSubMenuAnimated;
import com.greasybubbles.bringdown.LevelData;
import com.greasybubbles.bringdown.MasterSettings;
import com.greasybubbles.bringdown.graphics.DancingBlocks;
import com.greasybubbles.bringdown.graphics.GbgScrollingTileBackground;
import com.greasybubbles.bringdown.graphics.GraphicSystemParameters;
import com.greasybubbles.greasytools.CallbackParams;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyTheme;
import com.greasybubbles.greasytools.GreasyButton;
import com.greasybubbles.greasytools.GreasyScene;
import com.greasybubbles.greasytools.GreasySubMenu;

public class GsMenuScene extends GreasyScene {

	private final String[] assetsRequired = { "title", "bgtexture",
			"button", "buttonsmall", "backbutton", "faces", "check" };
	
	public enum MenuStatus {
		Animating,
		AwaitingNewScreenToBePresented,
		Ready,
		Frozen,
	}
	
	public enum MenuScreen {
		None,
		Title,
		MainMenu,
		WorldSelect,
		LevelSelect,
		Options,
		Credits,
		LevelResults,
		Stats,
	}

	//POSITION CONSTANTS
	private final int BACKBUTTON_X = 726;
	private final int BACKBUTTON_Y = 406;
			
	
	MenuStatus menuStatus;
	MenuScreen currentScreen;
	
	GsSubMenuAnimated greasySubMenu;
	
	GreasyTheme scrollingBackground;
	
	//variables used by TITLE
	Sprite titleSprite;	
	Text pressStartText;
	
	//variables used by RESULTS
	DancingBlocks dancingBlocks;

	private int lastLevelPlayed;
	private int levelToLoad;
	
	
	
	public GsMenuScene(GraphicSystemParameters gsp, MenuScreen initialScreen) {
		super(gsp);

		BlankScreen();
		
		//sends the command to load the list of assets (data files, textures etc) needed.
		GreasyAssets.LoadAssets(assetsRequired, gsp);
		
		setBackground(new Background(0.5f, 0.5f, 0.5f));

		//creates the scrolling background

		scrollingBackground = new GbgScrollingTileBackground(this, RASTER_WIDTH, RASTER_HEIGHT);
		this.layerBG0.attachChild(scrollingBackground);
		
		GreasyButton.Init(this, gsp.vboManager);

		// creates the logo (this is done here because it will be visible throughout the menus.
		titleSprite = new Sprite(0,0,GreasyAssets.getTexture("title"),graphicSystemParameters.vboManager);

		currentScreen = initialScreen;
		menuStatus = MenuStatus.AwaitingNewScreenToBePresented;
	}

	public GsMenuScene(GraphicSystemParameters gsp) {
		this(gsp, MenuScreen.Title); //if there are no parameters, start from the title screen.
	}

	//this constructor is used for when the point of entry to the menu system is back from the game
	//i.e. we will display the level results screen!
	public GsMenuScene(GraphicSystemParameters gsp, MenuScreen initialScreen, int level, int outcome, int score) {
		this(gsp);
		assert (initialScreen == MenuScreen.LevelResults);
		currentScreen = MenuScreen.LevelResults;
		CreateLevelResultsMenu(level, outcome, score);
		menuStatus = MenuStatus.Animating;
		lastLevelPlayed = level;
	}
	
	private void CreateTitleScreen() {
		//attaches the title sprite to the scene. (it is already instantiated by the constructor)
		titleSprite.setPosition((RASTER_WIDTH-titleSprite.getWidth())/2,
				(RASTER_HEIGHT-titleSprite.getHeight())/2);
		if (!titleSprite.hasParent())
			layerSPRITE.attachChild(titleSprite);
		
		pressStartText = new Text(0, 0, GreasyAssets.fMainFont.getFont(), "TOUCH SCREEN TO CONTINUE", 36,
				graphicSystemParameters.vboManager);
		pressStartText.setPosition((RASTER_WIDTH-pressStartText.getWidth())/2, RASTER_HEIGHT-50);
		
		greasySubMenu = new GsSubMenuAnimated(this);

		greasySubMenu.AddElement(pressStartText);
		greasySubMenu.AddElement(new InvisibleButton("wholescreen", 0, 0, RASTER_WIDTH, RASTER_HEIGHT));
		
		greasySubMenu.Present();
	}
	
	private void CreateMainMenu() {

		greasySubMenu = new GsSubMenuAnimated(this);
		
		greasySubMenu.AddElement(new BigButton("play",		"PLAY",     128, 320));
		greasySubMenu.AddElement(new BigButton("tutorial",	"TUTORIAL", 128, 400));
		greasySubMenu.AddElement(new BigButton("stats",		"STATS", 	416, 320));
		greasySubMenu.AddElement(new BigButton("credits",	"CREDITS", 	416, 400));
		
		greasySubMenu.AddElement(new GraphicsButton
				("goback", GreasyAssets.getTexture("backbutton"), BACKBUTTON_X, BACKBUTTON_Y));

		greasySubMenu.Present();
	}

	private void CreateLevelSelectMenu() {
		
		final int Y_BASE = 320;
		final int X_COL1 = 72;
		final int X_COL2 = RASTER_WIDTH/2 + X_COL1;
		final int X_STRIDE = 96;
		final int Y_STRIDE = 80;
		
		greasySubMenu = new GsSubMenuAnimated(this);
		
		greasySubMenu.AddElement(new MenuHeaderText("SELECT LEVEL", Y_BASE-50));

		greasySubMenu.AddElement(new SmallButton("1", "1-1", X_COL1 + X_STRIDE*0, Y_BASE + Y_STRIDE*0));
		greasySubMenu.AddElement(new SmallButton("2", "1-2", X_COL1 + X_STRIDE*1, Y_BASE + Y_STRIDE*0));
		greasySubMenu.AddElement(new SmallButton("3", "1-3", X_COL1 + X_STRIDE*2, Y_BASE + Y_STRIDE*0));

		greasySubMenu.AddElement(new SmallButton("4", "2-1", X_COL1 + X_STRIDE*0, Y_BASE + Y_STRIDE*1));
		greasySubMenu.AddElement(new SmallButton("5", "2-2", X_COL1 + X_STRIDE*1, Y_BASE + Y_STRIDE*1));
		greasySubMenu.AddElement(new SmallButton("6", "2-3", X_COL1 + X_STRIDE*2, Y_BASE + Y_STRIDE*1));

		greasySubMenu.AddElement(new SmallButton("7", "3-1", X_COL2 + X_STRIDE*0, Y_BASE + Y_STRIDE*0));
		greasySubMenu.AddElement(new SmallButton("8", "3-2", X_COL2 + X_STRIDE*1, Y_BASE + Y_STRIDE*0));
		greasySubMenu.AddElement(new SmallButton("9", "3-3", X_COL2 + X_STRIDE*2, Y_BASE + Y_STRIDE*0));
		
		greasySubMenu.AddElement(new SmallButton("10", "4-1", X_COL2 + X_STRIDE*0, Y_BASE + Y_STRIDE*1));
		greasySubMenu.AddElement(new SmallButton("11", "4-2", X_COL2 + X_STRIDE*1, Y_BASE + Y_STRIDE*1));
		greasySubMenu.AddElement(new SmallButton("12", "4-3", X_COL2 + X_STRIDE*2, Y_BASE + Y_STRIDE*1));

		for (int i = 1; i <= 12; i++) {
			((SmallButton)(greasySubMenu.getButton(""+i))).SetCheckMark(MasterSettings.levelsWon[i]);
		}
		
		greasySubMenu.AddElement(new GraphicsButton
				("goback", GreasyAssets.getTexture("backbutton"), BACKBUTTON_X, BACKBUTTON_Y));

		greasySubMenu.Present();
	}
	
	private void CreateCreditsMenu() {
		greasySubMenu = new GsSubMenuAnimated(this);
		
		greasySubMenu.AddElement(new CenteredText("CONCEPT, PROGRAMMING,", 265, Color.YELLOW));		
		greasySubMenu.AddElement(new CenteredText("GRAPHICS:", 300, Color.YELLOW));		

		greasySubMenu.AddElement(new CenteredText("DANIELLE ASHLEY", 350, Color.WHITE));

		greasySubMenu.AddElement(new CenteredText("POWERED BY ANDENGINE", 400, Color.CYAN));
		greasySubMenu.AddElement(new CenteredText("COPYRIGHT (C) D ASHLEY 2014", 435, Color.CYAN));
		
		greasySubMenu.AddElement(new GraphicsButton
				("goback", GreasyAssets.getTexture("backbutton"), BACKBUTTON_X, BACKBUTTON_Y));

		greasySubMenu.Present();
	}


	private void CreateLevelResultsMenu(int levelno, int outcome, int score) {
		
		//creates the dancing blocks animation and attaches it to the scene!
		if (outcome == OUTCOME_SUCCESS) {
			dancingBlocks = new DancingBlocks(DancingBlocks.HAPPY, 400, 400, vboManager);
		} else {
			dancingBlocks = new DancingBlocks(DancingBlocks.SAD, 400, 400, vboManager);
		}
		dancingBlocks.setPosition(40,40);
		//this.layerSPRITE.attachChild(dancingBlocks);

		//creates the menu elements of this submenu.
		greasySubMenu = new GsSubMenuAnimated(this);
		
		final int X_CENTERPOINT = 650;
		
		greasySubMenu.AddElement(dancingBlocks);
		
		if (outcome == OUTCOME_SUCCESS)
			greasySubMenu.AddElement(new CenteredText("LEVEL COMPLETED!", X_CENTERPOINT, 165, Color.CYAN));	
		else if (outcome == OUTCOME_FAILURE)
			greasySubMenu.AddElement(new CenteredText("YOU DIDN'T MAKE IT!", X_CENTERPOINT, 165, Color.RED));
		else if (outcome == OUTCOME_ABORT) 			
			greasySubMenu.AddElement(new CenteredText("YOU GAVE UP!", X_CENTERPOINT, 165, Color.YELLOW));	
		else throw new RuntimeException("Invalid level exit state.");

		if (outcome == OUTCOME_SUCCESS)
			greasySubMenu.AddElement(new CenteredText("SCORE: "+score, X_CENTERPOINT, 125, Color.WHITE));
			//TODO Put real score
		if (outcome == OUTCOME_SUCCESS)
			greasySubMenu.AddElement(new BigButton("next", "NEXT", X_CENTERPOINT-128, 220));
		else
			greasySubMenu.AddElement(new BigButton("retry", "RETRY", X_CENTERPOINT-128, 220));
		
		greasySubMenu.AddElement(new BigButton("mainmenu", "MAIN MENU", X_CENTERPOINT-128, 294));
		
		greasySubMenu.Present();
	}
	
	private void CreateStatsMenu() {
		//creates the menu elements of this submenu.
		greasySubMenu = new GsSubMenuAnimated(this);
				
		greasySubMenu.AddElement(new CenteredText("GAME STATS", RASTER_WIDTH/2, 5, Color.YELLOW));	

		final int LINE_HEIGHT = 28;
		String statsText;
		int winPercentage;
		
		for (int i = 1; i <= Constants.NUM_LEVELS; i++) {
			if (MasterSettings.levelTimesPlayed[i] == 0) winPercentage = 0;
			else winPercentage = Math.round(100f*MasterSettings.levelTimesWon[i]/MasterSettings.levelTimesPlayed[i]);
			statsText = String.format("Level %d: Played %dx, Win%% %d%%, Top $%d, in %d, %d Attempts",
					i,
					MasterSettings.levelTimesPlayed[i],
					winPercentage,
					MasterSettings.levelScores[i],
					MasterSettings.levelTopMoves[i],
					MasterSettings.levelAttemptsBeforeWin[i]);
			greasySubMenu.AddElement(new Text(5, 40 + i*LINE_HEIGHT, GreasyAssets.fSimpleFont.getFont(),
					statsText, this.vboManager));
		}
			
		greasySubMenu.AddElement(new GraphicsButton
				("goback", GreasyAssets.getTexture("backbutton"), BACKBUTTON_X, BACKBUTTON_Y));
		
		greasySubMenu.Present();
	}
	
	private void UnregisterAllTouchAreas() {
		this.unregisterTouchAreas(new ITouchAreaMatcher() {
			@Override
			public boolean matches(ITouchArea pObject) {
				// TODO Auto-generated method stub
				return true;
			}
		});
	}
	
	private void SwitchToSubScreen(MenuScreen nextScreen) {
		currentScreen = nextScreen;
		menuStatus = MenuStatus.Animating;
		UnregisterAllTouchAreas();
		greasySubMenu.Destroy();
	}
	
	private void StowLogo() {
		//calculate a few constants.
		final float TITLE_MAGNIFIED_X = (RASTER_WIDTH - titleSprite.getWidth())/2;
		final float TITLE_MAGNIFIED_Y = (RASTER_HEIGHT - titleSprite.getHeight())/2;

		final float TITLE_MINIMISED_X = (RASTER_WIDTH - titleSprite.getWidth())/2;
		final float TITLE_MINIMISED_Y = (RASTER_HEIGHT*.66f - titleSprite.getHeight())/2;

		final float FXTIME = 0.8f;
		
		//set scale centre in the middle of the sprite.
		titleSprite.setScaleCenter(titleSprite.getWidth()/2, titleSprite.getHeight()/2);
		titleSprite.registerEntityModifier(new ParallelEntityModifier(
				new SequenceEntityModifier(new ScaleModifier(FXTIME/2, 1f, -1f, 1f, 1f, EaseSineInOut.getInstance()),
										new ScaleModifier(FXTIME/2, -1f, 1f, 1f, 1f, EaseSineInOut.getInstance())),
				new SequenceEntityModifier(new ColorModifier(FXTIME/2, 1f,.2f, 1f,.2f, 1f,1f, EaseQuintInOut.getInstance()),
						new ColorModifier(FXTIME/2, .2f,1f, .2f,1f, 1f,1f, EaseQuintInOut.getInstance())),
				new MoveModifier(FXTIME, TITLE_MAGNIFIED_X, TITLE_MINIMISED_X, TITLE_MAGNIFIED_Y, TITLE_MINIMISED_Y)));
	}
	
	private void UnstowLogo() {
		//calculate a few constants.
		final float TITLE_MAGNIFIED_X = (RASTER_WIDTH - titleSprite.getWidth())/2;
		final float TITLE_MAGNIFIED_Y = (RASTER_HEIGHT - titleSprite.getHeight())/2;

		final float TITLE_MINIMISED_X = (RASTER_WIDTH - titleSprite.getWidth())/2;
		final float TITLE_MINIMISED_Y = (RASTER_HEIGHT*.66f - titleSprite.getHeight())/2;

		final float FXTIME = 0.75f;
		
		titleSprite.registerEntityModifier(new SequenceEntityModifier(
				new DelayModifier(FXTIME*.67f), 
				new MoveModifier(FXTIME*.33f, TITLE_MINIMISED_X, TITLE_MAGNIFIED_X,
						TITLE_MINIMISED_Y, TITLE_MAGNIFIED_Y)));
	}
	
	@Override
	public void UpdateLoop(float secondsElapsed) {
		scrollingBackground.Update(secondsElapsed);
		
		//here I check whether the last menu subscreen has finished and if so I start loading the next one.
		if (menuStatus == MenuStatus.AwaitingNewScreenToBePresented) {
			switch (currentScreen) {
			case Title:
				CreateTitleScreen();
				break;
			case MainMenu:
				CreateMainMenu();
				break;
			case LevelSelect:
				CreateLevelSelectMenu();
				break;
			case Credits:
				CreateCreditsMenu();
				break;
			case Stats:
				CreateStatsMenu();
				break;
			case None:
				//ends the scene with a soft fadeout.
				OverAndOut(0f, 1.0f, new CallbackParams("level="+levelToLoad));
				break;
			default:
				break;
			}
			menuStatus = MenuStatus.Animating;
		}
		
	}
	
	@Override
	public void OnTapUp(int pixelX, int pixelY) {

	}

	@Override
	public void OnTapDown(int pixelX, int pixelY) {

	}
	
	@Override
	public void OnPressButton(GreasyButton buttonPressed) {
		//if we are not ready to take touches, ignore it.
		if (menuStatus != MenuStatus.Ready)
			return;

		//process the touch according to what screen we are in.
		if (currentScreen == MenuScreen.Title) {
			if (buttonPressed.getID().equals("wholescreen")) {
				StowLogo();
				SwitchToSubScreen(MenuScreen.MainMenu);
			}
		} else if (currentScreen == MenuScreen.MainMenu) {
			if (buttonPressed.getID().equals("goback")) {
				greasySubMenu.HighlightElement(buttonPressed);
				UnstowLogo();
				SwitchToSubScreen(MenuScreen.Title);
			} else if (buttonPressed.getID().equals("play")) {
				greasySubMenu.HighlightElement(buttonPressed);
				SwitchToSubScreen(MenuScreen.LevelSelect);
			} else if (buttonPressed.getID().equals("tutorial")) {
				greasySubMenu.HighlightElement(buttonPressed);
				levelToLoad = -1;
				SwitchToSubScreen(MenuScreen.None);
			} else if (buttonPressed.getID().equals("credits")) {
				greasySubMenu.HighlightElement(buttonPressed);
				SwitchToSubScreen(MenuScreen.Credits);
			} else if (buttonPressed.getID().equals("stats")) {
				greasySubMenu.HighlightElement(buttonPressed);
				this.titleSprite.setVisible(false);
				SwitchToSubScreen(MenuScreen.Stats);
			}
		} else if (currentScreen == MenuScreen.Credits) {
			if (buttonPressed.getID().equals("goback")) {
				greasySubMenu.HighlightElement(buttonPressed);
				SwitchToSubScreen(MenuScreen.MainMenu);
			} 
		} else if (currentScreen == MenuScreen.LevelSelect) {
			if (buttonPressed.getID().equals("goback")) {
				greasySubMenu.HighlightElement(buttonPressed);
				SwitchToSubScreen(MenuScreen.MainMenu);
			} else try {
				int levelNo = Integer.parseInt(buttonPressed.getID());
				if (LevelData.exists(levelNo)) {
					greasySubMenu.HighlightElement(buttonPressed);
					levelToLoad = levelNo;
					SwitchToSubScreen(MenuScreen.None);
				}
			} catch (NumberFormatException e) {};
		} else if (currentScreen == MenuScreen.LevelResults) {
			if (buttonPressed.getID().equals("next")) {
				if (LevelData.exists(lastLevelPlayed + 1)) {
					greasySubMenu.HighlightElement(buttonPressed);
					levelToLoad = lastLevelPlayed + 1;
					SwitchToSubScreen(MenuScreen.None);					
				}
			} else if (buttonPressed.getID().equals("retry")) {
				greasySubMenu.HighlightElement(buttonPressed);
				levelToLoad = lastLevelPlayed;
				SwitchToSubScreen(MenuScreen.None);				
			} else if (buttonPressed.getID().equals("mainmenu")) {
				greasySubMenu.HighlightElement(buttonPressed);
				SwitchToSubScreen(MenuScreen.Title);
			}
		} else if (currentScreen == MenuScreen.Stats) {
			if (buttonPressed.getID().equals("goback")) {
				greasySubMenu.HighlightElement(buttonPressed);
				this.titleSprite.setVisible(true);
				SwitchToSubScreen(MenuScreen.MainMenu);
			}
		}
	}

	@Override
	public void OnSubmenuCreated(GreasySubMenu subMenu) {
		menuStatus = MenuStatus.Ready;
	}
		
	@Override
	public void OnSubmenuDestroyed(GreasySubMenu subMenu) {
		menuStatus = MenuStatus.AwaitingNewScreenToBePresented;
	}

	//internal classes
	//
	
	class BigButton extends GreasyButton {
		public BigButton(String id, String text, int x, int y) {
			super(id, (TextureRegion)GreasyAssets.getTexture("button"),
					GreasyAssets.fMainFont.getFont(), text);
			this.setPosition(x,y);
		}
	}

	class SmallButton extends GreasyButton {
		boolean hasCheckMark = false;
		Sprite checkMark;
		public SmallButton(String id, String text, int x, int y) {
			super(id, GreasyAssets.getTexture("buttonsmall"),
					GreasyAssets.fMainFont.getFont(), text);
			checkMark = new Sprite(42, 42, GreasyAssets.getTexture("check"), vbom);
			checkMark.setVisible(false);
			this.attachChild(checkMark);
			this.setPosition(x,y);
		}
		public SmallButton SetCheckMark(boolean checkedState) {
			hasCheckMark = checkedState;
			checkMark.setVisible(checkedState);
			return this; //for convenience
		}
		public void setAlpha(float alpha) {
			checkMark.setAlpha(alpha);
			super.setAlpha(alpha);
		}
	}

	class GraphicsButton extends GreasyButton {
		public GraphicsButton(String id, TextureRegion texture, int x, int y) {
			super(id, texture, GreasyAssets.fMainFont.getFont(), "");
			this.setPosition(x,y);
		}
	}
	
	class InvisibleButton extends GreasyButton {
		public InvisibleButton(String id, int x, int y, int width, int height) {
			super(id, width, height);
			this.setPosition(x,y);
		}
	}
	
	class MenuHeaderText extends Text {
		public MenuHeaderText(String text, int y) {
			super(0, 0, GreasyAssets.fBigFont.getFont(), text,
					graphicSystemParameters.vboManager);
			this.setColor(Color.YELLOW);
			this.setX((RASTER_WIDTH-this.getWidth())/2);
			this.setY(y);
		}
	}	
	
	class CenteredText extends Text {
		public CenteredText(String text, int y) {
			super(0, 0, GreasyAssets.fMainFont.getFont(), text,
					graphicSystemParameters.vboManager);
			this.setColor(Color.WHITE);
			this.setX((RASTER_WIDTH-this.getWidth())/2);
			this.setY(y);
		}
		public CenteredText(String text, int y, Color color) {
			this(text, y);
			this.setColor(color);
		}
		public CenteredText(String text, int xCentre, int y, Color color) {
			this(text, y, color);
			this.setX(xCentre - this.getWidth()/2);
		}
	}
	
}