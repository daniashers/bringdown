package com.greasybubbles.bringdown.scenes;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.ITouchArea.ITouchAreaMatcher;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.color.Color;

import com.greasybubbles.bringdown.GsSubMenuAnimated;
import com.greasybubbles.bringdown.MasterSettings;
import com.greasybubbles.bringdown.graphics.GraphicSystemParameters;
import com.greasybubbles.greasytools.CallbackParams;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.IGreasyCallback;
import com.greasybubbles.greasytools.GreasyButton;
import com.greasybubbles.greasytools.GreasyScene;
import com.greasybubbles.greasytools.GreasySubMenu;
import com.greasybubbles.greasytools.GreasyTouchProcessor;

import static com.greasybubbles.bringdown.Constants.*;

public class GsPauseScene extends GreasyScene {
	
	public enum MenuStatus {
		Animating,
		Ready
	}
	
	GreasyScene callerScene;
	
	MenuStatus menuStatus;
	
	GsSubMenuAnimated greasySubMenu;
			
	Rectangle pauseWindowBox;
	Text pausedText;
	
	public boolean quit = false;
	
	public GsPauseScene(GraphicSystemParameters gsp, GreasyScene callerScene) {
		//super(gsp);
		//^ this time we are not calling the superclass constructor
		//cause not doing so will allow us to configure this scene much differently (cause it's a child scene)
		
		graphicSystemParameters = gsp;
		this.callerScene = callerScene;
		
		//sets up the Greasy Touch Listener (processed touch listener)
		this.setOnSceneTouchListener(new GreasyTouchProcessor());
		GreasyTouchProcessor.setProcessedTouchListener(this);
		//TODO ^ this listener shit is dodgy with child scenes. it may rob the main scene of events! REWORK
		
		//disables background so we can still see the parent scene behind the pause screen
		this.setBackgroundEnabled(false);
				
		pauseWindowBox = new Rectangle(120, 152, 560, 176, gsp.vboManager);
		pauseWindowBox.setColor(Color.BLACK);
		pauseWindowBox.setAlpha(.75f);
		
		GreasyButton.Init(this, graphicSystemParameters.vboManager);
		//^^ if we have a scene within a scene re-initialising the same static members will cause problems!
		//consider getting rid of the static members...
		
		CreatePauseMenu();
		
	}
	
	
	private void CreatePauseMenu() {
		greasySubMenu = new GsSubMenuAnimated(this);

		String musicButtonText;
		String sfxButtonText;
		
		if (MasterSettings.music) {
			musicButtonText = "MUSIC ON ";
		} else {
			musicButtonText = "MUSIC OFF";
		}
		if (MasterSettings.sfx) {
			sfxButtonText = "EFFECTS ON ";
		} else {
			sfxButtonText = "EFFECTS OFF";
		}

		greasySubMenu.AddElement(pauseWindowBox);
		greasySubMenu.AddElement(new BigButton("sound",  sfxButtonText, 136, 168));
		greasySubMenu.AddElement(new BigButton("music",  musicButtonText , 408, 168));
		greasySubMenu.AddElement(new BigButton("resume", "RESUME",    136, 242));
		greasySubMenu.AddElement(new SmallButton("quit", "QUIT",      600, 242));
	}

	public void Present() {
		greasySubMenu.Present();
	}
	
	private void UnregisterAllTouchAreas() {
		this.unregisterTouchAreas(new ITouchAreaMatcher() {
			@Override
			public boolean matches(ITouchArea pObject) {
				// TODO Auto-generated method stub
				return true;
			}
		});
	}
	
	@Override
	public void UpdateLoop(float secondsElapsed) {
		//this scene doesn't have an update loop.		
	}
	
	@Override
	public void OnTapUp(int pixelX, int pixelY) {

	}

	@Override
	public void OnTapDown(int pixelX, int pixelY) {

	}
	
	@Override
	public void OnPressButton(GreasyButton buttonPressed) {
		//if we are not ready to take touches, ignore it.
		if (menuStatus != MenuStatus.Ready)
			return;

		//process the touch according to what screen we are in.
		if (buttonPressed.getID().equals("sound")) {
			MasterSettings.sfx = !(MasterSettings.sfx);
			if (MasterSettings.sfx) {
				buttonPressed.setText("EFFECTS ON ");
			} else {
				buttonPressed.setText("EFFECTS OFF");
			}
			MasterSettings.saveMusicSfxSettings(MasterSettings.music, MasterSettings.sfx);
		} else if (buttonPressed.getID().equals("music")) {
			MasterSettings.music = !(MasterSettings.music);
			if (MasterSettings.music) {
				buttonPressed.setText("MUSIC ON ");
			} else {
				buttonPressed.setText("MUSIC OFF");
			}
			MasterSettings.saveMusicSfxSettings(MasterSettings.music, MasterSettings.sfx);
		} else if (buttonPressed.getID().equals("resume")) {
			menuStatus = MenuStatus.Animating;
			greasySubMenu.HighlightElement(buttonPressed);
			greasySubMenu.Destroy();
		} else if (buttonPressed.getID().equals("quit")) {
			quit = true;
			menuStatus = MenuStatus.Animating;
			greasySubMenu.HighlightElement(buttonPressed);
			greasySubMenu.Destroy();
		}

	}

	@Override
	public void OnSubmenuCreated(GreasySubMenu subMenu) {
		menuStatus = MenuStatus.Ready;
	}
		
	@Override
	public void OnSubmenuDestroyed(GreasySubMenu subMenu) {
		UnregisterAllTouchAreas();
		((IGreasyCallback)callerScene).onSceneFinishedCallback(this, new CallbackParams());
	}

	//internal classes
	//
	
	class BigButton extends GreasyButton {
		public BigButton(String id, String text, int x, int y) {
			super(id, (TextureRegion)GreasyAssets.getTexture("button"),
					GreasyAssets.fMainFont.getFont(), text);
			this.setPosition(x,y);
		}
	}

	class SmallButton extends GreasyButton {
		public SmallButton(String id, String text, int x, int y) {
			super(id, (TextureRegion)GreasyAssets.getTexture("buttonsmall"),
					GreasyAssets.fMainFont.getFont(), text);
			this.setPosition(x,y);
		}
	}
	class InvisibleButton extends GreasyButton {
		public InvisibleButton(String id, int x, int y, int width, int height) {
			super(id, width, height);
			this.setPosition(x,y);
		}
	}
	
	class MenuHeaderText extends Text {
		public MenuHeaderText(String text, int y) {
			super(0, 0, GreasyAssets.fMainFont.getFont(), text, 48,
					graphicSystemParameters.vboManager);
			this.setColor(Color.YELLOW);
			this.setX((RASTER_WIDTH-this.getWidth())/2);
			this.setY(y);
		}
	}
	
}
