package com.greasybubbles.bringdown.scenes;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.util.color.Color;

import static com.greasybubbles.bringdown.Constants.*;

import com.greasybubbles.bringdown.graphics.GraphicSystemParameters;
import com.greasybubbles.greasytools.GreasyAssets;
import com.greasybubbles.greasytools.GreasyScene;

public class GsLoadingScene extends GreasyScene {

	private final String[] assetsRequired = {};
		
	Text loadingText;
	
	float bgOffsetX = 0f;
	float bgOffsetY = 0f;
	float bgSpeedX = 0.9f;
	float bgSpeedY = 0.31f;

	public GsLoadingScene(GraphicSystemParameters gsp) {
		super(gsp);
		this.StopLoop(); //this scene does not update at all... let's not waste time by having an update loop
		BlankScreen();

		GreasyAssets.LoadAssets(assetsRequired, gsp); //this is academic..no assets needed apart from the font
		
		//black background
		setBackground(new Background(0f, 0f, 0f));

		//adds a humble text. This is all that this scene contains!
		loadingText = new Text(0, 0, GreasyAssets.fMainFont.getFont(), "LOADING...", this.vboManager);
		loadingText.setColor(Color.WHITE);
		loadingText.setX((RASTER_WIDTH-loadingText.getWidth())/2);
		loadingText.setY((RASTER_HEIGHT-loadingText.getHeight())/2);
		UnblankScreen();
	}
}