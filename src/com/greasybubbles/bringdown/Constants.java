package com.greasybubbles.bringdown;

import org.andengine.util.color.Color;

public class Constants {
	
	//constants that define the size of the play grid.
	
	public static final int MIN_GRID_X = 0;
	public static final int MAX_GRID_X = 15;
	
	public static final int ACTIVE_MIN_X = 4;	//the first X cell coordinate of solid ground.
	public static final int ACTIVE_MAX_X = 11;	//the last X cell coordinate of solid ground
	public static final int ACTIVE_MIN_Y = 0;	//there is also a reserved side area to allow 
	public static final int ACTIVE_MAX_Y = 7;	//pieces to overhang but it is not in active play.

	public static final int TOTAL_X = 16; 	//total number of cells including 
	public static final int TOTAL_Y = 40;	//inactive margin and 'headroom' cells
	
	//constants that define the size in pixels of a grid cell
	//and of a whole piece (usually 2 cells wide)
	
	public static final int PIXELS_PER_CELL_ON_FILE = 48;
	public static final int PIXELS_PER_CELL_ON_SCREEN = 48;
	
	static final int PIXELS_PER_CELL_ON_STRIP_FILE = 48;
	
	// hardware and screen constants
	
	public static final int RASTER_WIDTH = 800;
	public static final int RASTER_HEIGHT = 480;
	
	static final int GRID_START_X_ON_SCREEN = 16;
	static final int GRID_START_Y_ON_SCREEN = 24;	
	
	public static final float TILE_SCALE_FACTOR =
			(float)PIXELS_PER_CELL_ON_SCREEN / PIXELS_PER_CELL_ON_FILE;

	// Physical constants
	
	private static final float GRAVITY_ACCELERATION = 9.81f;
	private static final float CELL_SIZE_METERS = 0.5f; //experimentally determined to make animations look cool
	public static final float CELL_GRAVITY_ACCELERATION =
			GRAVITY_ACCELERATION / CELL_SIZE_METERS;
	public static final float PIXEL_GRAVITY_ACCELERATION =
		GRAVITY_ACCELERATION * PIXELS_PER_CELL_ON_SCREEN / CELL_SIZE_METERS;

	public static final int NUM_LEVELS = 12;	

	
	public enum SplitMode {
		Horizontal,
		Vertical,
		DiagSlash,
		DiagBackslash
	}

	public enum ExplMode { Destroy, Split, Apart };
	
	//these are the possible types of lines that our game will process. Only the possible ones
	//are listed here, for example there is no "horizontal double" because that case doesn't occur in game.
	
	public enum LineType {
		NotValid,
		VertHalf,
		VertSingle,
		HorzSingle,
		VertDouble,
		DiagSlash,
		DiagBackslash
	}
	
	//misc constants
	
	public static final int OUTCOME_SUCCESS = 1;
	public static final int OUTCOME_FAILURE = 2;
	public static final int OUTCOME_ABORT = 3;

	//COLOR CONSTANTS
	
	public static final Color COLOR_POINTSMINUS = new Color(0.5f, 0.5f, 1.0f);
	public static final Color COLOR_POINTSPLUS = new Color(1.0f, 0.2f, 0.2f);

	//THEMES
	
	public static final int THEME_TUTORIAL = 0;
	public static final int THEME_FIERY = 1;
	public static final int THEME_SWAMP = 2;
	public static final int THEME_TRAFFIC = 3;
	public static final int THEME_CLIFFS = 4;
	public static final int THEME_URBANSNOW = 5;
	public static final int THEME_TITIAN = 6;

	
}
