package com.greasybubbles.bringdown;

import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import static com.greasybubbles.bringdown.Constants.*;

import android.view.KeyEvent;

import com.greasybubbles.bringdown.graphics.GraphicSystemParameters;
import com.greasybubbles.bringdown.scenes.GsGameplayScene;
import com.greasybubbles.bringdown.scenes.GsLoadingScene;
import com.greasybubbles.bringdown.scenes.GsMenuScene;
import com.greasybubbles.greasytools.CallbackParams;
import com.greasybubbles.greasytools.IGreasyCallback;
import com.greasybubbles.greasytools.GreasyScene;

public class Main extends BaseGameActivity implements IGreasyCallback {

	// Variables
	//
	Camera camera;
	Engine engine;

	GraphicSystemParameters graphicParams;
		
	GreasyScene activeScene;
	GreasyScene loadingScene;
		
	// Override methods
	//
	
	@Override
	public EngineOptions onCreateEngineOptions() {
		camera = new Camera(0, 0, RASTER_WIDTH, RASTER_HEIGHT);
		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED,
				new RatioResolutionPolicy(RASTER_WIDTH, RASTER_HEIGHT), camera);
	}

	@Override
	public Engine onCreateEngine(EngineOptions myEngineOptions) {
		engine = new FixedStepEngine(myEngineOptions, 60);
		graphicParams = new GraphicSystemParameters(this, engine);
		return engine;
	}
	
	@Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws Exception {
		pOnCreateResourcesCallback.onCreateResourcesFinished();		
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception {
		//this.mEngine.registerUpdateHandler(new FPSLogger());
		MasterSettings.setContext(getApplicationContext());
		MasterSettings.loadPreferences();
		
		activeScene = new GreasyScene(graphicParams); //creates an empty instance just so that any methods
		loadingScene = new GsLoadingScene(graphicParams);
		//called by the first switchtoscene function will not incur a null pointer exception
		SwitchToScene(new GsMenuScene(graphicParams));
		
		pOnCreateSceneCallback.onCreateSceneFinished(activeScene);
}

	@Override
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	
	// THIS WILL BE CALLED BY THE SCENES WHEN THEY ARE DONE.
	
	@Override
	public void onSceneFinishedCallback(GreasyScene callerScene, CallbackParams params) {
		// TODO Auto-generated method stub
		if (callerScene instanceof GsMenuScene) {

			int levelToLoad = params.getParamInt("level");
						
			SwitchToScene(new GsGameplayScene(graphicParams, levelToLoad));			

		} else if (callerScene instanceof GsGameplayScene) {

			int levelJustFinished = params.getParamInt("level");
			int outcome = params.getParamInt("outcome");
			int score = params.getParamInt("score");
			
			if (levelJustFinished < 0) { //if it was a tutorial level (denoted by negative level numbers)
				SwitchToScene(new GsMenuScene(graphicParams, GsMenuScene.MenuScreen.Title));
			} else { //if it was a 'real' level (positive level numbers)
				SwitchToScene(new GsMenuScene(graphicParams, GsMenuScene.MenuScreen.LevelResults,
					levelJustFinished, outcome, score));
			}
		}
	}

	//switchToScene
	private void SwitchToScene(GreasyScene newScene) {
		loadingScene.BlankScreen();
		engine.setScene(loadingScene);
		loadingScene.UnblankScreen();
		activeScene.StopLoop();
		activeScene.dispose(); //TODO ?
		activeScene = newScene;
		activeScene.StartLoop();
		activeScene.BlankScreen();
		engine.setScene(activeScene);
		activeScene.StartLoop();
		activeScene.FadeIn(1.0f);
	}
	
	
	//TODO integrate the below stuff into my touch listener?
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {                  
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			if (activeScene.OnBackButtonPressed()) //if our function returns true (ie deals with the event)
				return true; //true means: we've dealt with it.
			else
				return super.onKeyDown(keyCode, event);
		}                                                                    
		return super.onKeyDown(keyCode, event);                              
	}

}