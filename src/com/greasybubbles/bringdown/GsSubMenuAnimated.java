package com.greasybubbles.bringdown;

import org.andengine.entity.Entity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;

import com.greasybubbles.greasytools.GreasyScene;
import com.greasybubbles.greasytools.GreasySubMenu;

public class GsSubMenuAnimated extends GreasySubMenu {
		
	private Entity highlightedElement = null;

	public GsSubMenuAnimated(GreasyScene parentScene) {
		super(parentScene);
	}
	
	@Override
	public float PresentAnimation() {
		final float PRESENT_ANIMATION_LENGTH = 1.0f;
		//menuLayer.setAlpha(0f);
		for (Entity element : elementList) {
			if (!element.hasParent())
				menuLayer.attachChild(element);
		}
		for (Entity e : elementList)
			e.registerEntityModifier(new AlphaModifier(0.45f, 0f, e.getAlpha()));
		return PRESENT_ANIMATION_LENGTH;
	}

	@Override
	public float DestroyAnimation() {
		final float DESTROY_ANIMATION_LENGTH = 1.0f;
		for (Entity e : elementList) {
			if (e != highlightedElement) {
				e.registerEntityModifier(new AlphaModifier(0.75f, e.getAlpha(), 0f));
			}
			else {
				e.registerEntityModifier(new SequenceEntityModifier(
						new DelayModifier(0.5f),
						new MoveModifier(0.45f, e.getX(), e.getX(), e.getY(), e.getY()+480)));
			}
		}
		return DESTROY_ANIMATION_LENGTH;
	}


	
	public void HighlightElement(Entity element) {
		if (!elementList.contains(element))
			throw new RuntimeException("Trying to access nonexistent submenu element!");
		element.setColor(0f, 1f, 1f);
		highlightedElement = element;
	}
	
}
