package com.greasybubbles.greasytools;

import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;


import android.view.MotionEvent;


public class GreasyTouchProcessor implements IOnSceneTouchListener{
	
	// Constants
	//
	
	private static final long DOUBLE_TAP_TIMEOUT = 250;
	// ^ milliseconds: two taps further apart in time that the above value
	// will not be considered a double tap.
	private static final int DOUBLE_TAP_TOLERANCE = 20;
	// ^ pixels: two taps further apart on screen than the above value
	// will not be considered a double tap.	
	private static final int DRAG_THRESHOLD = 20;
	// ^ pixels: until the finger separates the above number of pixels from
	// its starting position, it will not be considered a drag action but simply a tap.
	
	// Variables
	//
	
	private static GreasyTouchListener listener;
	
	private static boolean gestureInProgress = false;

	private static int startX, startY;
	private static int midX, midY;
	private static int endX, endY;
	private static int lastX, lastY = 0;

	
	private static long timeOfLastTap = 0;
	
	
	private static boolean doubleTap;
	
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent event) {
		
		switch (event.getAction()) {
		
		case MotionEvent.ACTION_DOWN:

			startX = (int) event.getX();
			startY = (int) event.getY();
			
			//first, detect if this is a double tap by checking the time since last tap
			//and the location of the last tap (if close in time and in location, it's a double tap)

			long timeNow = System.currentTimeMillis();
			long timeSinceLastTap = timeNow - timeOfLastTap;
			timeOfLastTap = timeNow;
			
			if (timeSinceLastTap < DOUBLE_TAP_TIMEOUT) {
				if (Math.abs(startX-lastX) < DOUBLE_TAP_TOLERANCE &&
						Math.abs(startY-lastY) < DOUBLE_TAP_TOLERANCE) {
						doubleTap = true;
				} else {
					doubleTap = false;
				}
			} else {
				doubleTap = false;
			}

			lastX = startX;
			lastY = startY;
			
			if (doubleTap)
				listener.OnDoubleTapDown(startX, startY);
			else 
				listener.OnTapDown(startX, startY);
			
			break;
			
		case MotionEvent.ACTION_MOVE:
			midX = (int)event.getX();
			midY = (int)event.getY();
			if (Math.abs(startX-midX) > DRAG_THRESHOLD || Math.abs(startY-midY) > DRAG_THRESHOLD) {
				gestureInProgress = true;
			}
			if (gestureInProgress)
				listener.OnDrag(startX, startY, midX, midY);
			break;
		case MotionEvent.ACTION_UP:
			endX = (int)event.getX();
			endY = (int)event.getY();
			
			if (gestureInProgress) {
				gestureInProgress = false;
				listener.OnReleaseDrag(startX, startY, endX, endY);
			} else {
				if (doubleTap) {
					doubleTap = false;
					listener.OnDoubleTapUp(endX, endY);
				} else
					listener.OnTapUp(endX, endY);
			}

			break;
		}
		return true;
	}
	
	public static void setProcessedTouchListener(GreasyTouchListener pListener) {
		listener = pListener;
	}
	
}
