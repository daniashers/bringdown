package com.greasybubbles.greasytools;


public interface GreasyButtonListener {
	void OnPressButton(GreasyButton buttonPressed);
	void OnSubmenuDestroyed(GreasySubMenu subMenu);
	void OnSubmenuCreated(GreasySubMenu subMenu);
}
