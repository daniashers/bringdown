package com.greasybubbles.greasytools;

public interface GreasyTouchListener {
	void OnTapDown(int pixelX, int pixelY);
	void OnTapUp(int pixelX, int pixelY);
	void OnDoubleTapDown(int pixelX, int pixelY);
	void OnDoubleTapUp(int pixelX, int pixelY);
	void OnDrag(int startX, int startY, int currentX, int currentY);	
	void OnReleaseDrag(int startX, int startY, int endX, int endY);
}
