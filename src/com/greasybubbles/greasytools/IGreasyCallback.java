package com.greasybubbles.greasytools;


public interface IGreasyCallback {
	public void onSceneFinishedCallback(GreasyScene callerScene, CallbackParams params);
}
