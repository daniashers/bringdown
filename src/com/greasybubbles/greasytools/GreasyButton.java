package com.greasybubbles.greasytools;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.shape.RectangularShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;


public class GreasyButton extends Entity {

	public static VertexBufferObjectManager vbom; //TODO public? kluge!
	static private GreasyButtonListener touchListener;
	static private Scene scene;

	private boolean active = true;
	
	private RectangularShape buttonBody; //Remember that Sprite is derived from RectangularShape
	private Text buttonText = null;
	private String id;

	//this creates an "invisible" button (a touch area, in fact)
	public GreasyButton(String buttonID, int width, int height) {
		final GreasyButton referenceToSelf = this; //TODO THIS IS A MESS
		
		id = buttonID;
		buttonBody = new Rectangle(0,0,width,height,vbom) {
			@Override
			public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {

				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN)
					touchListener.OnPressButton(referenceToSelf);
				return true;
			}
		};
		buttonBody.setVisible(false);
		this.attachChild(buttonBody);

		scene.registerTouchArea(buttonBody);
	}

	//This creates a button with text and a background
	public GreasyButton(String buttonID, TextureRegion buttonTexture, Font font, String text) {
		final GreasyButton referenceToSelf = this; //TODO THIS IS A MESS
		
		id = buttonID;
		buttonBody = new Sprite(0,0,buttonTexture,vbom) {
			@Override
			public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN)
					if (referenceToSelf.isActive())
						touchListener.OnPressButton(referenceToSelf);
				return true;
			}
		};
		buttonText = new Text(0,0,font,text,36,vbom);
		buttonText.setX((buttonBody.getWidth()-buttonText.getWidth())/2);
		buttonText.setY((buttonBody.getHeight()-buttonText.getHeight())/2);
		this.attachChild(buttonBody);
		this.attachChild(buttonText);
		
		scene.registerTouchArea(buttonBody);
	}

	//This creates a button that is only an image, without text
	public GreasyButton(String buttonID, TextureRegion buttonTexture) {
		final GreasyButton referenceToSelf = this; //TODO THIS IS A MESS
		
		id = buttonID;
		buttonBody = new Sprite(0,0,buttonTexture,vbom) {
			@Override
			public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN)
					if (referenceToSelf.isActive())
						touchListener.OnPressButton(referenceToSelf);
				return true;
			}
		};
		this.attachChild(buttonBody);
		
		scene.registerTouchArea(buttonBody);
	}
	
	public String getID() {
		return id;
	}
	
	//TODO debug function
	public void ChangeColor() {
		buttonBody.setColor((float)Math.random(), (float)Math.random(), (float)Math.random());
	}

	@Override
	public void setColor(float r, float g, float b) {
		buttonBody.setColor(r,g,b);
	}
	
	public float getWidth() {
		return buttonBody.getWidth();
	}
	public float getHeight() {
		return buttonBody.getHeight();
	}

	@Override
	public void setAlpha(float mAlpha) {
		buttonBody.setAlpha(mAlpha);
		if (buttonText != null)
			buttonText.setAlpha(mAlpha);
	}
	
	public void UnregisterTouchArea() {
		scene.unregisterTouchArea(buttonBody);
	}
	
	public void setActive(boolean state) {
		active = state;
	}

	public void setText(String text) {
		buttonText.setText(text);
		buttonText.setX((buttonBody.getWidth()-buttonText.getWidth())/2);
		buttonText.setY((buttonBody.getHeight()-buttonText.getHeight())/2);		
	}
	
	public boolean isActive() {
		return active;
	}
	
	public static void Init(GreasyScene s, VertexBufferObjectManager v) {
		scene = s;
		touchListener = s;
		vbom = v;
	}
	
}
