package com.greasybubbles.greasytools;

import java.util.ArrayList;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.util.modifier.IModifier;

public class GreasySubMenu implements IEntityModifierListener {
	
	public ArrayList<Entity> elementList;
		
	public GreasyScene scene;
	
	private boolean creating = false;
	private boolean destroying = false;

	public Entity menuLayer; //TODO turn GreasySubMenu into a subclass of Entity & do away with the layer...
	
	public GreasySubMenu(GreasyScene parentScene) {
		scene = parentScene;
		menuLayer = new Entity();
		scene.attachChild(menuLayer);
		elementList = new ArrayList<Entity>();
		PrepareElements();
	}

	public void PrepareElements() {
		//this function will be overriden by derivative classes so that the contents of the menu
		//can be part of the class...
	}

	public void Present() {
		creating = true;
		PresentAnimation();
		float pAniLength = PresentAnimation();
		menuLayer.registerEntityModifier(new DelayModifier(pAniLength, this));
	}

	public void Destroy() {
		destroying = true;
		float dAniLength = DestroyAnimation();
		menuLayer.registerEntityModifier(new DelayModifier(dAniLength, this));
	}

	
	//the below method can be overridden by derivative classes to achieve the desired effect.
	//IT MUST RETURN THE TOTAL ANIMATION TIME THAT WILL BE TAKEN.
	public float PresentAnimation() {
		return 0f;
	}
	
	//the below method can be overridden by derivative classes to achieve the desired effect.	
	//IT MUST RETURN THE TOTAL ANIMATION TIME THAT WILL BE TAKEN.
	public float DestroyAnimation() {
		return 0f;
	}
	
	public void AddElement(Entity element) {
		elementList.add(element);
	}
		
	public GreasyButton getButton(String id) {
		for (Entity element : elementList) {
			if (element instanceof GreasyButton) {
				if (((GreasyButton)element).getID().equals(id)) {
					return (GreasyButton)element;
				}
			}
		}
		return null;
	}
	
	
	public void Reset() {
		menuLayer.detachChildren();
		elementList.clear();
	}

	@Override
	public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
		//nothing to do
	}

	@Override
	public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
		if (creating) {
			creating = false;
			scene.OnSubmenuCreated(this);
		} else if (destroying) {
			destroying = false;
			if (menuLayer.hasParent())
				menuLayer.detachSelf();
			scene.OnSubmenuDestroyed(this);
		} else
			throw new RuntimeException("Improper use of submenu modifier callbacks!");
	}
	
}
