package com.greasybubbles.greasytools;

import org.andengine.entity.Entity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;

public abstract class GreasyTheme extends Entity {

	GreasyScene scene;
	
	public GreasyTheme(GreasyScene mScene) {
		this.scene = mScene;
	}
	
	public Entity GetBackground() { return null; }
	
	public Entity GetForeground() { return null; }
	
	public abstract void Update(float timeElapsed);

	public static String[] getRequiredAssets() {
		return new String[0];
	}
	
	//the static method below simply returns a background that contains nothing, do nothing,
	//is just completely transparent and invisible but still contains valid methods.
	public static GreasyTheme NullBackground(GreasyScene mScene) {
		return new GreasyTheme(mScene) {
			@Override
			public void Update(float timeElapsed) {}
		};
	}
	
	//the static method below simply returns a background that contains just a simple
	//background image and nothing else.
	public static GreasyTheme ImageBackground(GreasyScene mScene, final TextureRegion image) {
		return new ImageBackground(mScene, image);
	}
	
	//internal class
	public static class ImageBackground extends GreasyTheme {
		public ImageBackground(GreasyScene mScene, TextureRegion image) {
			super(mScene);
			this.attachChild(new Sprite(0,0,image,mScene.vboManager));
		}
		@Override
		public void Update(float timeElapsed) {}
	}
}
