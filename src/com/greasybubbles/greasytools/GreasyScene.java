package com.greasybubbles.greasytools;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.font.FontManager;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import android.content.Context;

import com.greasybubbles.bringdown.Constants;
import com.greasybubbles.bringdown.graphics.GraphicSystemParameters;

public class GreasyScene extends Scene implements GreasyTouchListener, GreasyButtonListener {
	
	//fields that store frequently used object references for graphics purposes.
	public GraphicSystemParameters graphicSystemParameters;
	public VertexBufferObjectManager vboManager;
	public TextureManager textureManager;
	public FontManager fontManager;
	public Engine engine;
	public Context mainContext;
	
	boolean screenBlanked; //keeps track of whether the screen has been blanked or is visible.
	private boolean loopEnabled = false; //keeps track of whether the 'main loop' is running or paused.
	
	IUpdateHandler loopUpdateHandler; //this is the handler that will call our loop 60 times a second.
		
	Rectangle fadeRect; //this is the big screen-sized black rectangle that we will manipulate
						//to achieve fade in/fade out effects.

	//these are the various "layers" of the scene. By attaching children to one of these rather
	//than to the scene itself the developer can ensure which ones will be above which others.
	public Entity layerBG0;
	public Entity layerBG1;
	public Entity layerBG2;
	public Entity layerSPRITE;
	public Entity layerSFX;
	public Entity layerFRONT;
	public Entity layerOVERLAY0;
	public Entity layerOVERLAY1;
	private Entity layerFADE;
		
	//
	// Getter & Setter
	//
	
	
	//
	// Constructor
	//

	public GreasyScene() {
		//a no-arg constructor that does nothing.
		//It is necessary to have it for inheritance reasons...
	}
	
	public GreasyScene(GraphicSystemParameters gsp) {
		
		//copies the graphics parameters into internal fields,
		//as these will be used heavily.
		graphicSystemParameters = gsp;
		this.engine = gsp.engine;
		this.vboManager = gsp.vboManager;
		this.textureManager = gsp.textureManager;
		this.fontManager = gsp.fontManager;
		this.mainContext = gsp.context;
		
		ClearScene();
		
		this.attachChild(layerBG0 = new Entity());
		this.attachChild(layerBG1 = new Entity());
		this.attachChild(layerBG2 = new Entity());
		this.attachChild(layerSPRITE = new Entity());
		this.attachChild(layerSFX = new Entity());
		this.attachChild(layerFRONT = new Entity());
		this.attachChild(layerOVERLAY0 = new Entity());
		this.attachChild(layerOVERLAY1 = new Entity());
		this.attachChild(layerFADE = new Entity());

		//prepares the big occluding rectangle that will allow the fadeout effect and screen blank.
		fadeRect = new Rectangle(-10, -10,
				Constants.RASTER_WIDTH+20, Constants.RASTER_HEIGHT+20, gsp.vboManager);
		//TODO ^^ avoid using constants. Find out how to detect surface size.
		fadeRect.setColor(0.0f, 0.0f, 0.0f);
		fadeRect.setAlpha(0.0f);
		layerFADE.attachChild(fadeRect);
		
		//sets up the Greasy Touch Listener (processed touch listener)
		this.setOnSceneTouchListener(new GreasyTouchProcessor());
		GreasyTouchProcessor.setProcessedTouchListener(this);
		
		//registers the update handler that will trigger the main loop
		loopEnabled = false;
		loopUpdateHandler = new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				// CALLS THE MAIN LOOP
				if (loopEnabled) {
					UpdateLoop(pSecondsElapsed); //calls the main loop.
				}
			}
			
			@Override
			public void reset() {}
		};
		this.registerUpdateHandler(loopUpdateHandler);
		
	}
	
	public void ClearScene() {
		this.detachChildren();
		this.clearEntityModifiers();
		this.clearUpdateHandlers();
	}
	
	//
	// Blank screen and fade in/out methods.
	//
	
	public void BlankScreen() {
		fadeRect.setAlpha(1.0f);
		screenBlanked = true;
	}
	
	public void UnblankScreen() {
		fadeRect.setAlpha(0.0f);
		screenBlanked = false;
	}
	
	public void FadeOut(float fadeOutTime) {
		fadeRect.setAlpha(0.0f);
		fadeRect.registerEntityModifier(new AlphaModifier(fadeOutTime, 0.0f, 1.0f));
		screenBlanked = true;
	}

	public void FadeOut() {
		FadeOut(0.5f);
	}
	
	public void FadeIn(float fadeInTime) {
		fadeRect.setAlpha(1.0f);
		fadeRect.registerEntityModifier(new AlphaModifier(fadeInTime, 1.0f, 0.0f));
		screenBlanked = false;
	}

	
	public void FadeIn() {
		FadeIn(0.5f);
	}

	//TODO the existence of this method is a kluge to make the Special Effects class work.
	//find a better way.
	public Entity getFXLayer() {
		return layerSFX;
	}	

	//an utility function to perform a fadeout and then end the scene.
	public void OverAndOut(final float preDelay, final float fadeOutTime, final CallbackParams params) {
		final GreasyScene refToScene = this;
		this.registerEntityModifier(new SequenceEntityModifier(
				new DelayModifier(preDelay),
				new DelayModifier(fadeOutTime, new IEntityModifierListener() {
					@Override
					public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
						FadeOut(fadeOutTime);
					}
					public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
						((IGreasyCallback)graphicSystemParameters.context).onSceneFinishedCallback(refToScene, params);
					}	
				})));
	}
	
	//
	// GreasyTouchListener stubs
	// and GreayButtonListener stubs
	//

	@Override	
	public void OnTapDown(int pixelX, int pixelY) { }
	@Override	
	public void OnTapUp(int pixelX, int pixelY) { }
	@Override	
	public void OnDoubleTapDown(int pixelX, int pixelY) { }
	@Override	
	public void OnDoubleTapUp(int pixelX, int pixelY) { }
	@Override	
	public void OnDrag(int startX, int startY, int currentX, int currentY) { }	
	@Override	
	public void OnReleaseDrag(int startX, int startY, int endX, int endY) { }
	@Override	
	public void OnPressButton(GreasyButton buttonPressed) { }
	@Override
	public void OnSubmenuDestroyed(GreasySubMenu subMenu) {	}
	@Override
	public void OnSubmenuCreated(GreasySubMenu subMenu) {	}

	//The below function is not automatic, it must be called from the main. //TODO it's ugly
	public boolean OnBackButtonPressed() {
		return false; //if we are implementing back button functionality we override and return true.
	};
	
	//
	// UpdateLoop stub & loop start/stop functions
	//
	
	
	public void UpdateLoop(float secondsElapsed) {
		
	}
	
	public void StartLoop() {
		loopEnabled = true;
	}
	
	public void StopLoop() {
		loopEnabled = false;
	}

}
