package com.greasybubbles.greasytools;

import java.util.HashMap;

import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.greasybubbles.bringdown.AssetList;
import com.greasybubbles.bringdown.graphics.FontAsset;
import com.greasybubbles.bringdown.graphics.GraphicSystemParameters;

public class GreasyAssets {

	private static GraphicSystemParameters params;
		
	private static HashMap<String, BitmapTextureAtlas> loadedAtlases = new HashMap<String, BitmapTextureAtlas>();
	private static HashMap<String, ITextureRegion> loadedAssets = new HashMap<String, ITextureRegion>();
		
	private static String variant;
	
	public static FontAsset fMainFont = null;
	public static FontAsset fBigFont = null;
	public static FontAsset fSimpleFont = null;
	
	public static void LoadAssets(String[] assetsToBeLoaded, GraphicSystemParameters gParams) {
		params = gParams;

		variant = "";
		
		for (String assetId : assetsToBeLoaded) {
			LoadAsset(assetId);
		}

		//TODO eventually the font too will be passed as an asset name rather than having a special case.
		if (fMainFont == null)
			fMainFont = new FontAsset("akashi.ttf", 28f, 1024, 1024, params);
		if (fBigFont == null)
			fBigFont = new FontAsset("akashi.ttf", 42f, 1024, 1024, params);
		if (fSimpleFont == null)
			fSimpleFont = new FontAsset(24f, 256, 256, params);
	}
	
	private static void LoadAsset(String assetId) {
		
		//TODO: this function is a monster. There must be a better way of breaking it down.		
		
		//TODO: currently whenever there is a series of regions that have to be combined at runtime
		//into the same atlas, they are all loaded at once. Can this cause problems??
				
		if (isAlreadyLoaded(assetId))
			return;
		
		// Case of simple texture (in its own texture atlas, not packed)
		if (AssetList.isNotPacked(assetId)) {
			//first create a new atlas
			BitmapTextureAtlas tempTextureAtlas = new BitmapTextureAtlas(params.textureManager,
					AssetList.getWidthOf(assetId), AssetList.getHeightOf(assetId));
			//then load the asset onto it
			TextureRegion tempTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset
					(tempTextureAtlas, params.context, assetId + variant + ".png", 0, 0);
			//finalise and store reference to the newly created objects into the hashmap for later retrieval
			tempTextureAtlas.load();
			loadedAtlases.put(assetId, tempTextureAtlas);
			loadedAssets.put(assetId, tempTextureRegion);
			
		} else if (AssetList.isPostPacked(assetId)) {
		
			//check if the atlas on which this texture should reside is already loaded
			if (loadedAtlases.containsKey(AssetList.getParentAtlasOf(assetId))) {
				//if the atlas is already present we assume that all the textures within are
				//already loaded; we may want to change that later so that we can load each texture region
				//individually, but for the moment let's leave it like this.
				return;
			}

			//get the list of all the assets to load into this atlas.
			String parentAtlas = AssetList.getParentAtlasOf(assetId);
			String[] assetsInThisAtlas = AssetList.getAllRegionsInAtlas(parentAtlas);
			
			//determine maximum size of textures so we know how big an atlas we have to make
			int maxX = 0;
			int maxY = 0;
			for (String id : assetsInThisAtlas) {
				int thisX = (AssetList.getWidthOf(id)+AssetList.getOffsetXOf(id));
				int thisY = (AssetList.getHeightOf(id)+AssetList.getOffsetYOf(id));
				if (thisX > maxX) maxX = thisX;
				if (thisY > maxY) maxY = thisY;
			}

			//now create a new atlas with the newly established dimensions.
			BitmapTextureAtlas tempTextureAtlas = new BitmapTextureAtlas(params.textureManager,
					maxX, maxY);
			//then load all the assets onto it with the proper offsets as defined in the asset list.
			for (String id : assetsInThisAtlas) {
				TextureRegion tempTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset
						(tempTextureAtlas, params.context, id + variant + ".png",
								AssetList.getOffsetXOf(id), AssetList.getOffsetYOf(id));
				loadedAssets.put(id, tempTextureRegion);
			}
			//finalise and store reference to the newly created objects into the hashmap for later retrieval
			tempTextureAtlas.load();
			loadedAtlases.put(parentAtlas, tempTextureAtlas);
		
		} else if (AssetList.isTiled(assetId)) {
			
			//first create a new atlas
			BitmapTextureAtlas tempTextureAtlas = new BitmapTextureAtlas(params.textureManager,
					AssetList.getWidthOf(assetId), AssetList.getHeightOf(assetId));
			//then load the asset onto it
			TiledTextureRegion tempTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset
					(tempTextureAtlas, params.context, assetId + variant + ".png", 0, 0,
							AssetList.getTileColumnsOf(assetId), AssetList.getTileRowsOf(assetId));
			//finalise and store reference to the newly created objects into the hashmap for later retrieval
			tempTextureAtlas.load();
			loadedAtlases.put(assetId, tempTextureAtlas);
			loadedAssets.put(assetId, tempTextureRegion);
	
		} else if (AssetList.isPrePacked(assetId)) {
			
			String parentAtlas = AssetList.getParentAtlasOf(assetId);
			BitmapTextureAtlas tempTextureAtlas;

			//check if the atlas on which this texture should reside is already loaded and retrieve it
			//if it is, otherwise create it.
			if (loadedAtlases.containsKey(parentAtlas)) {
				tempTextureAtlas = loadedAtlases.get(parentAtlas);
			} else {
				//if the parent atlas has not been loaded yet then we load it.
				 tempTextureAtlas = new BitmapTextureAtlas(params.textureManager,
							AssetList.getWidthOf(parentAtlas), AssetList.getHeightOf(parentAtlas));
				 BitmapTextureAtlasTextureRegionFactory.createFromAsset
							(tempTextureAtlas, params.context, parentAtlas + variant + ".png", 0, 0);
				 tempTextureAtlas.load();
				 loadedAtlases.put(parentAtlas, tempTextureAtlas);
			}

			//get the list of all the assets to load into this atlas.
			String[] assetsInThisAtlas = AssetList.getAllRegionsInAtlas(parentAtlas);
			
			//now create all the Texture Region objects that share this pre-packed atlas.
			for (String id : assetsInThisAtlas) {
				assert (AssetList.isPrePacked(id));
				TextureRegion tempTextureRegion = new TextureRegion( tempTextureAtlas,
						AssetList.getOffsetXOf(id), AssetList.getOffsetYOf(id),
						AssetList.getWidthOf(id), AssetList.getHeightOf(id));
				loadedAssets.put(id, tempTextureRegion);
			}
		} else if (AssetList.isPrePackedMap(assetId)) {
			
			//load all Texture Regions in the atlas.
			for (String rId : AssetList.getAllRegionsInAtlas(assetId)) {
				LoadAsset(rId);
			}
		}
	}
	
	private static boolean isAlreadyLoaded(String assetId) {
		if (loadedAssets.containsKey(assetId))
			return true;
		else
			return false;
	}
	
	public static TextureRegion getTexture(String id) {
		if (!loadedAssets.containsKey(id))
			throw new RuntimeException("Requested texture <"+id+"> not loaded!");
		return (TextureRegion)(loadedAssets.get(id));
	}
	
	public static TiledTextureRegion getTextureTiled(String id) {
		if (!loadedAssets.containsKey(id))
			throw new RuntimeException("Requested texture <"+id+"> not loaded!");
		return (TiledTextureRegion)(loadedAssets.get(id));
	}
	
	//TODO add a 'destructor' that releases the textures
}
