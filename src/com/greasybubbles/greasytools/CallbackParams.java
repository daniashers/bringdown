package com.greasybubbles.greasytools;

public class CallbackParams {
	private String[] params;
	
	public CallbackParams(String... params) {
		this.params = new String[params.length];
		for (int i = 0; i < params.length; i++) {
			this.params[i] = params[i];
		}
	}
	 
	public String getParam(String key) {
		//the arguments are stored as "key=value".
		// if id == "key", return "value".
		for (String s : params) {
			if ((s.substring(0, key.length()+1)).equals(key+"=")) {
				return s.substring(key.length()+1, s.length());
			}
		}
		//if not found return a null string
		return null;
	}
	
	public int getParamInt(String key) {
		String result = this.getParam(key);
		if (result != null) {
			return Integer.parseInt(result);
		} else {
			throw new RuntimeException("Parameter \""+key+"\" is not an integer!");
		}
	}
}
